<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
Route::post('auth/login', [Api\AuthController::class, 'loginUser']);
Route::post('auth/register', [Api\UserController::class, 'store']);
Route::post('auth/password/forgot', [Api\AuthController::class, 'forgotPassword']);
Route::get('splash', [Api\AuthController::class, 'splash']);
Route::get('params', [Api\AuthController::class, 'params']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('auth/otp/send', [Api\AuthController::class, 'sendOTP']);
    Route::post('auth/otp/verify', [Api\AuthController::class, 'verifyOTP']);
    Route::post('auth/password/set', [Api\AuthController::class, 'setPassword']);
    Route::post('auth/password/change', [Api\AuthController::class, 'changePassword']);
    Route::post('auth/profile/edit', [Api\AuthController::class, 'editProfile']);
    Route::post('upload-document', [Api\AuthController::class, 'uploadDocument']);


    //job
    Route::get('job-listing', [Api\JobController::class, 'jobListing']);
    Route::get('job-detail/{id}', [Api\JobController::class, 'jobDetail']);
    Route::post('job-store', [Api\JobController::class, 'jobStore']);

    Route::post('job-start', [Api\JobController::class, 'jobStart']);
    Route::post('job-end', [Api\JobController::class, 'jobEnd']);
    Route::post('complete-job', [Api\JobController::class, 'completeJob']);

    //earn
    Route::get('earn-listing', [Api\JobController::class, 'earnListing']);
    Route::get('projects', [Api\JobController::class, 'projects']);



});
