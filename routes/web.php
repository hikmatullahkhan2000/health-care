<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\{
    ProfileController,
    CompanyController,
    ParamController,
    UserController,
    JobController
};
use App\Models\Job;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function ()
{
    Route::get('/dashboard',function (){
        return view('dashboard');
    })->middleware(['verified']);

    Route::resources([
        'users' => UserController::class,
        'drugs' => \App\Http\Controllers\DrugController::class,
        'diseases' => \App\Http\Controllers\DiseasesController::class,
        'procedures' => \App\Http\Controllers\ProcedureController::class,
        'blogs' => \App\Http\Controllers\BlogController::class,
        'specialties' => \App\Http\Controllers\SpecialtyController::class,
        'services' => \App\Http\Controllers\ServiceController::class,
    ]);

//    Route::middleware(['admin'])->group(function ()
//    {
//        Route::resources([
//            'params' => ParamController::class,
//        ]);
//    });

//    Route::get('params/status/{id}', [ParamController::class, "toggleStatus"])->name('params.toggle-status');
//
//    Route::get('job-calender', [JobController::class, 'jobCalender'])->name('jobCalender');
//    Route::get('job-logs', [JobController::class, 'jobLogs'])->name('jobLogs');
//    Route::get('job-assign-member/{id}', [JobController::class, 'jobAssignMember'])->name('jobAssignMember');
//    Route::post('job-assignStore/{id}', [JobController::class, 'jobAssignStore'])->name('jobAssignStore');
//    Route::delete('jobs/images/{id}', [JobController::class, 'imageDelete'])->name('jobs.images');
//    Route::post('airtable-store', [JobController::class, "airtableStore"])->name('airtableStore');
//    Route::get('job-complete', [JobController::class, 'jobComplete'])->name('jobComplete');
//
//
    //Appointments
    Route::get('appointments', [\App\Http\Controllers\UserController::class, 'Appointments'])->name('appointments.index');

    Route::controller(ProfileController::class)->prefix('profile')->name('profile.')->group(function () {
        Route::get('/', 'edit')->name('edit');
        Route::patch('/', 'update')->name('update');
        Route::delete('/', 'destroy')->name('destroy');

        Route::view('/change-password', 'profile.change-password')->name('password');
        Route::post('/change-password', 'changePassword')->name('change-password');
    });
});


///front

Route::get('/', [\App\Http\Controllers\Front\HomeController::class, 'index'])->name('home');
Route::get('/page/diseases-condition', [\App\Http\Controllers\Front\HomeController::class, 'diseasesCondition'])->name('diseases_condition');
Route::get('/page/diseases-condition-detail/{slug}', [\App\Http\Controllers\Front\HomeController::class, 'diseasesConditionDetail'])->name('diseasesConditionDetail');

Route::get('/page/drugs', [\App\Http\Controllers\Front\HomeController::class, 'drug'])->name('drug');
Route::get('/page/drug-detail/{slug}', [\App\Http\Controllers\Front\HomeController::class, 'drugDetail'])->name('drugDetail');

Route::get('/page/procedures', [\App\Http\Controllers\Front\HomeController::class, 'procedure'])->name('procedure');
Route::get('/page/procedure-detail/{slug}', [\App\Http\Controllers\Front\HomeController::class, 'procedureDetail'])->name('procedureDetail');

Route::get('/page/doctors', [\App\Http\Controllers\Front\HomeController::class, 'doctor'])->name('doctor');
Route::get('/page/news-feeds', [\App\Http\Controllers\Front\HomeController::class, 'newsfeeds'])->name('newsfeeds');
Route::get('/page/about-us', [\App\Http\Controllers\Front\HomeController::class, 'aboutus'])->name('aboutUs');
Route::get('/page/contact-us', [\App\Http\Controllers\Front\HomeController::class, 'contactus'])->name('contactUs');
Route::post('/page/contactus-store', [\App\Http\Controllers\Front\HomeController::class, 'contactStore'])->name('contactStore');

//news feed detail route
Route::get('/page/news-feed-detail/{slug}', [\App\Http\Controllers\Front\HomeController::class, 'newsFeedDetail'])->name('newsFeedDetail');


Route::get('/request-appointment/{id}', [\App\Http\Controllers\Front\HomeController::class, 'requestAppointment'])->name('requestAppointment');
Route::post('/appointmentStore', [\App\Http\Controllers\Front\HomeController::class, 'appointmentStore'])->name('appointmentStore');

require __DIR__.'/auth.php';
