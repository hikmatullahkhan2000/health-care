<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('short_title')->nullable();
            $table->longText('short_description')->nullable();
            $table->string('slug')->nullable();
            $table->longText('long_description')->nullable();
            $table->string('banner')->nullable();
            $table->integer('specialty')->nullable();
            $table->longText('description')->nullable();
            $table->string('video_link')->nullable();
            $table->integer('type')->default(0)->comment('0:by_drug_name,1:by_therapeutic_classification');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
