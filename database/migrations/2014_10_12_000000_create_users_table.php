<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string("last_name")->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->integer('type')->comment('1:admin,2:doctor');
            $table->timestamp('phone_verified_at')->nullable();
            $table->string("job_category_id")->nullable();
            $table->integer("gender")->nullable()->comment("1:male,2:female");
            $table->string('otp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('specialty')->nullable();
            $table->string('address')->nullable();
            $table->string('description')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->string('phone')->nullable();
            $table->string('otp_verified_at')->nullable();
            $table->string('fcm_token')->nullable();
            $table->string('document')->nullable();
            $table->tinyInteger('doc_approved')->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
