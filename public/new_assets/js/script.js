//Header dely
	document.addEventListener("DOMContentLoaded", function(){
		window.addEventListener('scroll', function() {
			if (window.scrollY > 200) {
				document.getElementById('navbar_top').classList.add('fixed-top');
				// add padding top to show content behind navbar
				navbar_height = document.querySelector('.navbar').offsetHeight;
				document.body.style.paddingTop = navbar_height + 'px';
			} else {
			 	document.getElementById('navbar_top').classList.remove('fixed-top');
				 // remove padding top from body
				document.body.style.paddingTop = '0';
			} 
		});
	}); 
	// DOMContentLoaded  end
  // Header sec end //


  //Aos Animation
  jQuery(document).ready(function($){$(function() {      AOS.init(); });  });
  //mobile off
  AOS.init({disable: 'mobile'});
  //Aos end


  //ul active
  $(document).ready(function() {
    $('.search-cont ul a').click(function() {
      $('.search-cont ul a.active').not(this).removeClass('active');
      $(this).toggleClass('active');
    });
  });
//ul active end  



