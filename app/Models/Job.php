<?php

namespace App\Models;

use App\TraitLibraries\ModelDependentUpload;
use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Job extends Model
{
    use HasFactory, ModelHelper, ModelDependentUpload, ModelSearch;
    public static $filepath = "jobs";

    const statuses = [
        self::STATUS_PENDING   => 'pending',
        self::STATUS_ACTIVE    => 'active',
        self::STATUS_CANCEL    => 'cancel',
        self::STATUS_COMPLETE  => 'complete',
        self::STATUS_ASSIGN    => 'assign'
    ];

    const STATUS_PENDING    = 0;
    const STATUS_ACTIVE     = 1;
    const STATUS_CANCEL     = 2;
    const STATUS_COMPLETE   = 3;
    const STATUS_ASSIGN     = 4;


    protected $fillable = [
        'title',
        'date',
        'description',
        'price_doc',
        'job_doc',
        'job_requirement_doc',
        'pickup_doc',
        'estimated_day',
        'price',
        'address',
        'square',
        'longitude',
        'latitude',
        'status',

    ];

    public function jobLog()
    {
        return $this->hasOne(JobLog::class, 'job_id', 'id');
    }

    public function scopeStatusPending($query)
    {
        return $query->where('status', 0);
    }

    public function jobDoc()
    {
        return $this->hasMany(File::class, 'objectId', 'id')
            ->where('object','JOBS')->where('type','job_doc');
    }
    public function bannerImages()
    {
        return $this->hasMany(File::class, 'objectId', 'id')
            ->where('object','JOBS')->where('type','banner_image');
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'member_id');
    }


}
