<?php

namespace App\Models;
use App\TraitLibraries\ModelDependentUpload;
use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Table;


class RequestAppointment extends Model
{
    protected $guarded  = [];
    use ModelHelper,ModelDependentUpload,ModelSearch;
    protected $table = 'request_appointments';
    protected $fillable = [
        'full_name',
        'email',
        'phone',
        'user_id',
        'concerns'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
