<?php

namespace App\Models;
use App\TraitLibraries\ModelDependentUpload;
use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Table;


class Contact extends Model
{
    protected $guarded  = [];
    use ModelHelper,ModelDependentUpload,ModelSearch;
    protected $table = 'contact_us';
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'area_code',
        'phone',
        'state',
        'address',
        'city',
        'concerns',
        'zip_code',
    ];


}
