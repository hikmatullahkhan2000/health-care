<?php

namespace App\Models;

use App\TraitLibraries\ModelDependentUpload;
use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Disease extends Model
{
    use HasFactory, ModelHelper, ModelDependentUpload, ModelSearch;
    public static $filepath = "diseases_condition";
    protected $table = 'diseases_condition';

    const types = [
        self::by_drug_name   => 'By Drug Name',
        self::by_therapeutic_classification    => 'By Therapeutic Classification',
    ];

    const by_drug_name    = 0;
    const by_therapeutic_classification     = 1;

//    const specialties = [
//        "cardiology" => 'cardiology',
//        "dermatology" => 'dermatology',
//        "emergency_medicine" => 'emergency medicine',
//        "endocrinology" => 'endocrinology',
//        "gastroenterology" => 'gastroenterology',
//        "general_family_practice" => 'General Family Practice',
//        "geriatrics" => 'Geriatrics',
//        "hematology_oncology" => 'Hematology/Oncology',
//        "infectious_diseases" => 'Infectious Diseases',
//        "nephrology" => 'Nephrology',
//        "neurology" => 'Neurology',
//        "obstetrics_gynecology" => 'Obstetrics/Gynecology',
//        "ophthalmology" => "Ophthalmology",
//        "otolaryngology" => "Otolaryngology",
//        "pediatrics" => "Pediatrics",
//        "psychiatry" => "Psychiatry",
//        "public_health" => "Public Health",
//        "pulmonology" => "Pulmonology",
//        "rheumatology" => "Rheumatology",
//        "sports_medicine" => "Sports Medicine",
//        "urology" => "Urology",
//    ];



    protected $fillable = [
        'title',
        'short_title',
        'short_description',
        'slug',
        'long_description',
        'banner',
        'specialty',
        'description',
        'type',
        'video_link',
    ];

    public function bannerImages()
    {
        return $this->hasMany(File::class, 'objectId', 'id')
            ->where('object','JOBS')->where('type','banner_image');
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'member_id');
    }

    public function specialtie(){
        return $this->hasOne(Specialty::class, 'id', 'specialty');
    }

}
