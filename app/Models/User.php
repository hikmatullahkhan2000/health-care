<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\TraitLibraries\FcmManager;
use App\TraitLibraries\ModelDependentUpload;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, ModelDependentUpload,SoftDeletes;


    const admin_user = 1;
    const member = 2;

    const type = [
        1 => 'admin',
        2 => 'member',
//        3 => 'nurse',
    ];

    const genders = [
        1 => 'male',
        2 => 'female',
    ];


//    const nurseType = [
//        1 => 'LVN',
//        2 => 'CNA',
//    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'job_category_id',
        'gender',
        'email',
        'phone',
        'password',
        'specialty',
        'type',
        'otp',
        'photo',
        'address',
        'lat',
        'long',
        'document',
        'doc_approved',
        'service_id',
        'state'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function fetch()
    {
        return self::all();
    }

    public function send_message($title, $message, $data = [])
    {
        if ($this->fcm_token)
            FcmManager::send($this, $title, $message, $data);
    }
    public function specialtie(){
        return $this->hasOne(Specialty::class, 'id', 'specialty');
    }
}

