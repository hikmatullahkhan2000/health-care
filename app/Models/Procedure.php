<?php

namespace App\Models;

use App\TraitLibraries\ModelDependentUpload;
use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Procedure extends Model
{
    use HasFactory, ModelHelper, ModelDependentUpload, ModelSearch;
    public static $filepath = "procedures";
    protected $table = 'procedures';

    const types = [
        self::by_drug_name   => 'By Drug Name',
        self::by_therapeutic_classification    => 'By Therapeutic Classification',
    ];

    const by_drug_name    = 0;
    const by_therapeutic_classification     = 1;

    const specialties = [
        "aesthetic_procedures" => 'Aesthetic Procedures',
        "eye_ear_nose_and_throat" => 'Eye, Ear, Nose, and Throat',
        "gastroenterology_procedures" => 'Gastroenterology Procedures',
        "gynecology_and_urology" => 'Gynecology and Urology',
        "injections_soft_tissue_and_joint" => 'Injections - Soft Tissue and Joint',
        "nail_procedures" => 'Nail Procedures',
        "pediatrics" => 'Pediatrics',
        "physical_examination" => 'Physical Examination',
        "skin_surgery" => 'Skin Surgery',
        "splints_and_casts" => 'Splints and Casts',
        "urgent_and_emergency_care" => 'Urgent and Emergency Care',

    ];



    protected $fillable = [
        'title',
        'short_title',
        'short_description',
        'slug',
        'long_description',
        'banner',
        'specialty',
        'description',
        'type',
        'video_link',
    ];

    public function bannerImages()
    {
        return $this->hasMany(File::class, 'objectId', 'id')
            ->where('object','JOBS')->where('type','banner_image');
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'member_id');
    }

    public function specialtie(){
        return $this->hasOne(Specialty::class, 'id', 'specialty');
    }


}
