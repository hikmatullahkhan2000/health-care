<?php

namespace App\Models;

use App\TraitLibraries\ModelDependentUpload;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\FileHelpers;

class JobLog extends Model
{
    use HasFactory,ModelDependentUpload;
    protected $table        = 'job_log';
    public static $filepath = "job_log";

    const statuses = [
        self::STATUS_PENDING    => 'pending',
        self::STATUS_COMPLETE   => 'complete',
        self::STATUS_CANCEL     => 'cancel',
        self::STATUS_ASSIGN     => 'assign',
        self::STATUS_START      => 'start',
        self::STATUS_END        => 'end',
    ];

    const STATUS_PENDING    = 0;
    const STATUS_COMPLETE   = 1;
    const STATUS_CANCEL     = 2;
    const STATUS_ASSIGN     = 3;
    const STATUS_START      = 4;
    const STATUS_END        = 5;

    protected $fillable = [
        'job_id',
        'member_id',
        'clock_in',
        'clock_out',
        'longitude',
        'latitude',
        'file',
        'status',
    ];

    public function job(){
        return $this->hasOne(Job::class,'id','job_id');
    }

    public function member(){
        return $this->hasOne(User::class,'id','member_id');
    }

}
