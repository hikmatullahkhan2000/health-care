<?php

namespace App\Models;

use App\TraitLibraries\ModelDependentUpload;
use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Drug extends Model
{
    use HasFactory, ModelHelper, ModelDependentUpload, ModelSearch;
    public static $filepath = "drugs";

    const types = [
        self::by_drug_name   => 'By Drug Name',
        self::by_therapeutic_classification    => 'By Therapeutic Classification',
    ];

    const by_drug_name    = 0;
    const by_therapeutic_classification     = 1;


    protected $fillable = [
        'title',
        'short_title',
        'short_description',
        'slug',
        'long_description',
        'banner',
        'specialty',
        'description',
        'type',
        'video_link',
    ];

    public function bannerImages()
    {
        return $this->hasMany(File::class, 'objectId', 'id')
            ->where('object','JOBS')->where('type','banner_image');
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'member_id');
    }


}
