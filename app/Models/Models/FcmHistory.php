<?php

namespace App\Models\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FcmHistory extends Model
{
    use HasFactory;
    protected $table = 'fcm_history';

    protected $fillable = [
        "user_id",
        "user_token",
        "title",
        "message",
        "multicast_id",
        "status",
        "fcm_response",
        "is_read",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
