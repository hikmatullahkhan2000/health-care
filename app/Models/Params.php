<?php

namespace App\Models;
use App\TraitLibraries\ModelDependentUpload;
use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Model;


class Params extends Model
{
    public const role = [
        0  =>'all',
        1  =>'admin',
        2  =>'company',
        3  =>'nurse',
    ];

    protected $fillable = [
        'key',
        'type',
        'value',
        'active',
//        'role'
    ];
    protected $guarded  = [];
    use ModelHelper,ModelDependentUpload,ModelSearch;
    //
}
