<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CompanyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     *
     */

    public function handle($request, Closure $next)
    {
        $bearer = $request->bearerToken();
        if(empty($bearer)){
            return response()->json([
                'success' => false,
                'error' => 'token required.',
            ]);
        }

        $currentTokens = DB::table('personal_access_tokens')
            ->orderBy("personal_access_tokens.id",'desc')
            ->first();
        $personal_access_tokens = DB::table('personal_access_tokens')->where('token',hash('sha256',$bearer))->first();

        if($currentTokens->token != $personal_access_tokens ->token){
            return response()->json([
                'success' => false,
                'error' => 'invalid token.',
            ]);
        }

        if ($token = DB::table('personal_access_tokens')->where('token',hash('sha256',$bearer))->first())
        {
            if ($user = User::find($token->tokenable_id))
            {
                Auth::login($user);
                return $next($request);
            }
        }

        return response()->json([
            'success' => false,
            'error' => 'Access denied.',
        ]);
    }
}
