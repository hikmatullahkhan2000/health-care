<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\TwilioController;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\JobResource;
use App\Http\Resources\RatingResource;
use App\Models\Company;
use App\Models\FcmHistory;
use App\Models\Job;
use App\Models\Member;
use App\models\MemberFcmToken;
use App\Models\Params;
use App\Models\Ratings;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class UserController extends BaseController
{


    protected $mainViewFolder = 'users.';


    /**
     * The user repository instance.
     */
    protected $_user;

    public function __construct(User $user)
    {
        $this->_user = $user;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::paginate(20);

        return view($this->mainViewFolder . 'index', ['users' => $users]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $model = $this->_user;

        return view($this->mainViewFolder . 'form', compact('model'));
    }


    public function edit($id)
    {
        $model = User::findOrFail($id);
        $model->password = '';

        return view($this->mainViewFolder . 'form', compact('model'));
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users',
                'phone' => 'required|unique:users',
//                'uid' => 'required|unique:users',
                 'password' => 'required|min:6|confirmed',
                // 'type' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendResult('Validation Error.', config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors()->first(), $request);
            }

            $user = new User($request->all());
            $hashed = Hash::make($request->password);
            $user->password = $hashed;
            $user->type = User::member;

            if (!$user->save()) {
                throw new \Exception('Error saving user');
            }
            $token = $user->createToken("API TOKEN")->plainTextToken;
            return $this->sendResult('User Created Successfully', config('codes.SUCCESS'), ['user' => $user, 'token' => $token], $request);
        } catch (Exception $e) {
            return $this->sendResult('User Error.', config('codes.REQUEST_VALIDATAION_ERROR'), ['data' => $e->getMessage()], $request);
        }
        return back()->withInput($request->all());
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError("Validation Error.", $validator->errors());
        }


        try {
            $this->_user = $this->_user->findOrFail($id);

            $this->_user->fill($request->except('password'));

            if ($request->password != null) {
                $this->_user->password = bcrypt($request->password);
            }
            // if the value exist in request filled
            if (!$this->_user->save())
                throw new \Exception('User not updated');

            $request->session()->flash('success', 'User Update successfully');

            return redirect(route('users.index'));

        } catch (\Exception $exception) {
            $request->session()->flash('error', $exception->getMessage());
        }

        return back()->withInput($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {

            $this->_user = $this->_user->findOrFail($id);
            if ($this->_user->delete())
                $request->session()->flash('success', 'User deleted successfully');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return back();

    }


    public function deleteAcount(Request $request){
        $member = User::where('id',$request->user()->id)->first();
        $member->delete();
        return $this->sendResult('Account Successfully delete.', config('codes.SUCCESS'), 'Account Successfully delete', $request);
    }




}
