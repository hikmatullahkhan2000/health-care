<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\JobResource;
use App\Models\Company;
use App\Models\CompleteJob;
use App\Models\Job;
use App\Models\JobLog;
use App\Models\User;
use Carbon\Carbon;
use Dflydev\DotAccessData\Data;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Mockery\Exception;
use function GuzzleHttp\Promise\all;

class JobController extends BaseController
{
    public function jobListing(Request $request){
        $jobs = Job::with(['jobDoc','bannerImages'])
            ->where('status', JobLog::STATUS_PENDING)->get();
        return $this->sendResult('Job Successfully Fetch.', config('codes.SUCCESS'), new JobResource($jobs), $request);
    }

    public function jobStore(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'status' => ['required'],
            ]);

            if ($validator->fails()) {
                return $this->sendResult('Validation Error', config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors()->first(), $request);
            }

            if($request->status == JobLog::STATUS_COMPLETE){
                $job = Job::where('id',$request->job_id)->first();
                $job->status = Job::STATUS_COMPLETE;
                if (!$job->update()) {
                    throw new Exception('Error while saving Job!');
                }
            }

            $jobLog = new JobLog();
            $jobLog->member_id = $request->user()->id;
            $jobLog->job_id = $request->job_id;
            $jobLog->status = JobLog::STATUS_COMPLETE;
            if (!$jobLog->save())
                throw new \Exception('Error saving job');


//            $jobLog = JobLog::where('member_id',$request->user()->id)
//                    ->where('job_id',$request->job_id)->first();
//
//
//            $jobLog->status = $request->status;
//            if (!$jobLog->update()) {
//                throw new Exception('Error while saving JobLog!');
//            }

            return $this->sendResult('Job Successfully Update.', config('codes.SUCCESS'), $job, $request);
        } catch (Exception $e) {
            return $this->sendResult('Job Error.', config('codes.REQUEST_VALIDATAION_ERROR'), ['data' => $e->getMessage()], $request);
        }
    }

//    public function jobStart(Request $request){
//
//        try {
//            $validator = Validator::make($request->all(), [
//                'job_id' => ['required'],
//            ]);
//
//            if ($validator->fails()) {
//                return $this->sendResult('Validation Error', config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors()->first(), $request);
//            }
//
//            $jobLog = new JobLog();
//            $jobLog->member_id  = $request->user()->id;
//            $jobLog->job_id     = $request->job_id;
//            $jobLog->status     = JobLog::STATUS_START;
//
//            if (!$jobLog->save())
//                throw new \Exception('Error saving job log');
//
//            $job = Job::where('id',$request->job_id)->first();
//            $job->status  = Job::STATUS_ASSIGN;
//            if (!$job->update())
//                throw new \Exception('Error saving job');
//
//            return $this->sendResult('Job Successfully Update.', config('codes.SUCCESS'), $jobLog, $request);
//        } catch (Exception $e) {
//            return $this->sendResult('Job Error.', config('codes.REQUEST_VALIDATAION_ERROR'), ['data' => $e->getMessage()], $request);
//        }
//    }

    public function jobEnd(Request $request){

        try {
            $validator = Validator::make($request->all(), [
                'job_id' => ['required'],
            ]);

            if ($validator->fails()) {
                return $this->sendResult('Validation Error', config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors()->first(), $request);
            }

            $jobLog = new JobLog();
            $jobLog->member_id  = $request->user()->id;
            $jobLog->job_id     = $request->job_id;
            $jobLog->status     = JobLog::STATUS_ASSIGN;
            if($request->file('e_signature')) $jobLog->e_signature = $jobLog->modelFileUpload($request->file("e_signature"));

            if (!$jobLog->save())
                throw new \Exception('Error saving job');

            $job = Job::where('id',$request->job_id)->first();
            $job->status  = Job::STATUS_ASSIGN;
            if (!$job->update())
                throw new \Exception('Error saving job');

            return $this->sendResult('Job Successfully Update.', config('codes.SUCCESS'), $jobLog, $request);
        } catch (Exception $e) {
            return $this->sendResult('Job Error.', config('codes.REQUEST_VALIDATAION_ERROR'), ['data' => $e->getMessage()], $request);
        }
    }

    public function jobDetail(Request $request,$id){
        $job  = Job::with(['jobDoc','bannerImages'])->where('id',$id)->first();
        return $this->sendResult('Job Successfully View.', config('codes.SUCCESS'), $job, $request);
    }

    public function earnListing(Request $request){
        $jobLogs = JobLog::with(['job.jobDoc','job.bannerImages'])
            ->where('status',JobLog::STATUS_COMPLETE)
            ->where('member_id',$request->user()->id)
            ->get();
        $total = 0;
        foreach ($jobLogs as $jobLog){
            $total += $jobLog->job->price;
        }
        return $this->sendResult('Job Successfully View.', config('codes.SUCCESS'), ['total'=>$total,'jobs'=>$jobLogs], $request);
    }

    public function projects(Request $request){
        $currentDate = Carbon::now()->format('Y-m-d'); // Get the current date in 'YYYY-MM-DD' format
        $currentDateJobs = JobLog::with(['job.jobDoc','job.bannerImages'])
            ->join('jobs', 'jobs.id', '=', 'job_log.job_id')
            ->whereDate('jobs.date', '=', $currentDate)
            ->where('jobs.status', Job::STATUS_ASSIGN)
            ->where('job_log.member_id', $request->user()->id)
            ->get();

        $nextJobs = JobLog::with(['job.jobDoc','job.bannerImages'])
            ->join('jobs', 'jobs.id', '=', 'job_log.job_id')
            ->whereDate('jobs.date', '>', $currentDate)
            ->where('jobs.status', Job::STATUS_ASSIGN)
            ->where('job_log.member_id', $request->user()->id)
            ->get();

        return $this->sendResult('Project Successfully Fetch.', config('codes.SUCCESS'), ['onGoing'=>$currentDateJobs,'upNext'=>$nextJobs], $request);
    }

    public function completeJob(Request $request){

        try {
            $validator = Validator::make($request->all(), [
                'job_log_id'=> ['required'],
                'nails'     => ['required'],
                'dg'        => ['required'],
                'staples'   => ['required'],
            ]);

            if ($validator->fails()) {
                return $this->sendResult('Validation Error', config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors()->first(), $request);
            }

            $completeJob = new CompleteJob();
            $completeJob->member_id     = $request->user()->id;
            $completeJob->job_log_id    = $request->job_log_id;
            $completeJob->nails         = $request->nails;
            $completeJob->dg            = $request->dg;
            $completeJob->staples       = $request->staples;
            $completeJob->weed_fabric  = $request->weed_fabric;
            $completeJob->glue          = $request->glue;
            $completeJob->seam_type     = $request->seam_type;
            $completeJob->option1       = $request->option1;
            $completeJob->option2       = $request->option2;
            $completeJob->comments      = $request->comments;

            if (!empty($request->photo_seam)) {
                $photo_seam= $completeJob->modelFileUpload($request->file('photo_seam'));
                $completeJob->photo_seam = Job::$filepath.'/'.$photo_seam;
            }

            if (!empty($request->photo_turf)) {
                $photo_turf= $completeJob->modelFileUpload($request->file('photo_turf'));
                $completeJob->photo_turf = Job::$filepath.'/'.$photo_turf;
            }

            if (!$completeJob->save())
                throw new \Exception('Error complete job ');

            $job = Job::where('id',$request->job_id)->first();
            $job->status  = Job::STATUS_COMPLETE;
            if (!$job->update())
                throw new \Exception('Error saving job');

            $job = JobLog::where('id',$request->job_log_id)->first();
            $job->status  = JobLog::STATUS_COMPLETE;
            if (!$job->update())
                throw new \Exception('Error saving job');

            return $this->sendResult('Complete Job Successfully Update.', config('codes.SUCCESS'), $completeJob, $request);
        } catch (Exception $e) {
            return $this->sendResult('Complete Job Error.', config('codes.REQUEST_VALIDATAION_ERROR'), ['data' => $e->getMessage()], $request);
        }
    }
}
