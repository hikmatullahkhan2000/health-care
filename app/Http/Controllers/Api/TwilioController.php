<?php

namespace App\Http\Controllers\Api;


//use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Twilio\Rest\Client;

class TwilioController extends BaseController
{
    public function generate_token(Request $request)
    {
        $this->validate($request,[
            'user_id' => 'required',
        ]);
//        $from_user_id=$request->auth->customer_id;
//        $from_user_name=$request->auth->name;
        $to_user_id=$request->user_id;
        $current_timestamp = Carbon::now(env('APP_TIMEZONE'))->timestamp;
        $room=$to_user_id;
//        $identity = $from_user_id;

        $token=$this->getTwillioToken();
        $grant = new VideoGrant();
        $grant->setRoom($room);

        $token->addGrant($grant);
  return $this->sendResult('Twilio token.',config('codes.SUCCESS'),[['token' => $token->toJWT(),'room'=>$room]],$request);

    }

    public static function getTwillioToken($identity=null,$time=3600){
        if (empty($identity)){
            $identity=uniqid();
        }
        $token = new AccessToken(
            env('TWILIO_ACCOUNT_SID'),
            env('TWILIO_API_KEY_SID'),
            env('TWILIO_API_KEY_SECRET'),
            $time,
            $identity
        );
        return $token;
    }

    public static function sendSms($phone,$message){
        try {
        $auth_token = env('TWILIO_API_AUTH_TOKEN');
        $account_sid = env('TWILIO_ACCOUNT_SID');

        //// A Twilio number you own with SMS capabilities
        $twilio_number = "+19592144999";
        //

        $client = new Client($account_sid, $auth_token);
        $client->messages->create(
        // Where to send a text message (your cell phone?)
            "+".$phone,
            array(
                'from' => $twilio_number,
                'body' => $message
            )
        );

        }catch (\Exception $exception){
            Log::error('Twilio Sms Error '.$exception->getMessage());
        }
    }
    public function generate_token_accept(Request $request)
    {

       $this->validate($request,[
           'room'=>'required'
       ]);
        $identity = $request->auth->member_id;
        $token = new AccessToken(
            env('TWILIO_ACCOUNT_SID'),
            env('TWILIO_API_KEY_SID'),
            env('TWILIO_API_KEY_SECRET'),
            3600,
            $identity
        );
        $video=VideoCall::where('to_user_id','=',$request->auth->member_id)
        ->whereNull('callStartedAt')
        ->where('roomCode','=',$request->room)
        ->where('active','=',1)
        ->first();
        if($video != null)
        {
        $video->callStartedAt= date('Y-m-d H:i:s');
        $video->save();


        $grant = new VideoGrant();
        $grant->setRoom($request->room);
        $token->addGrant($grant);
        return $this->sendResult('TWILIO TOKEN.',config('codes.SUCCESS'), [[
                    'identity' => $identity,
                    'token' => $token->toJWT(),
                    'room'=>$request->room
                ]],$request);

        }else{
            return $this->sendResult('No Room Exist.',config('codes.REQUEST_VALIDATAION_ERROR'), ['errors' => ['room'=>['No room exists.']]],$request);
        }
    }

    public function TestingNotifications(Request $request){
        $fcmtoken=[''];
        $title='Title';
        $body='Body';
        $type='Call';
        $data=['roomCode'=>'1111','caller_name'=>'Test User'];
        $FcmManager=new FcmManager();
        $resp=$FcmManager->sendFCMNotification($fcmtoken,$title,$body,$type,$data);
        print_r($resp);



    }


}
