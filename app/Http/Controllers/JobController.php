<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\CompleteJob;
use App\Models\File;
use App\Models\Job;
use App\Models\JobLog;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;


class JobController extends Controller
{


    protected $mainViewFolder = 'jobs.';


    /**
     * The user repository instance.
     */
    protected $_job;

    public function __construct(Job $job)
    {
        $this->_job = $job;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobs = Job::orderBy('id', 'DESC')->get();
        return view($this->mainViewFolder . 'index', ['jobs' => $jobs]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->_job;
        return view($this->mainViewFolder . 'form', compact('model'));
    }


    public function edit($id)
    {
        $model = Job::findOrFail($id);
        return view($this->mainViewFolder . 'form', compact('model'));
    }

    public function store(Request $request)
    {
//        $this->validate($request, [
//            'date'=>"required",
//            'price'=>"required",
//            'description'=>"required",
//            'address'=>"required",
//            'estimated_day'=>"required",
//            'images'=>"required",
//            'square'=>"required",
//        ]);

        try {
            // Start Transaction
            $data = $request->all();
            DB::beginTransaction();
            $this->_job = new Job($data);
            $this->_job->job_doc = '';
            if ($request->hasFile('price_doc')) {
                $price_doc = (new Job())->modelFileUpload($request->file('price_doc'));
                $this->_job->price_doc = Job::$filepath.'/'.$price_doc;
            }

//            if ($request->hasFile('job_doc')) {
//                $job_doc = (new Job())->modelFileUpload($request->file('job_doc'));
//                $this->_job->job_doc = Job::$filepath.'/'.$job_doc;
//            }
//            if ($request->hasFile('pickup_doc')) {
//                $pickup_doc = (new Job())->modelFileUpload($request->file('pickup_doc'));
//                $this->_job->pickup_doc = Job::$filepath.'/'.$pickup_doc;
//            }
            if ($request->hasFile('job_requirement_doc')) {
                $job_requirement_doc= (new Job())->modelFileUpload($request->file('job_requirement_doc'));
                $this->_job->job_requirement_doc = Job::$filepath.'/'.$job_requirement_doc;
            }


            if (!$this->_job->save())
                throw new \Exception('Error saving job');


            if(!empty($request->images)){
                foreach ($request->images as $image){
                    if (!empty($image)) {
                        $this->_job->modelFileUpload($image,true,'banner_image');
                    }
                }

            }

            if(!empty($request->job_doc)){
                foreach ($request->job_doc as $file){
                    if (!empty($file)) {
                        $this->_job->modelFileUpload($file,true,'job_doc');
                    }
                }
            }


            DB::commit();
            $request->session()->flash('success', 'Job store successfully');

            return redirect(route('jobs.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            dd($exception->getMessage());
            $request->session()->flash('Jobs', $exception->getMessage());
        }
        return back()->withInput($request->all());
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date'=>"required",
            'price'=>"required",
            'description'=>"required",
            'address'=>"required",
            'estimated_day'=>"required",
            'square'=>"required",
        ]);
        try {

            // Start Transaction
            DB::beginTransaction();

            $this->_job = $this->_job->findOrFail($id);
            $this->_job->date = $request->date;
            $this->_job->price = $request->price;
            $this->_job->description = $request->description;
            $this->_job->address = $request->address;
            $this->_job->estimated_day = $request->estimated_day;
            $this->_job->square = $request->square;
            $this->_job->status = $request->status;
            $this->_job->pickup_doc = $request->pickup_doc;


            if ($request->hasFile('price_doc')) {
                $price_doc = $this->_job->modelFileUpload($request->file('price_doc'));
                $this->_job->price_doc = Job::$filepath.'/'.$price_doc;
            }

//            if ($request->hasFile('job_doc')) {
//                $job_doc = $this->_job->modelFileUpload($request->file('job_doc'));
//                $this->_job->job_doc = Job::$filepath.'/'.$job_doc;
//            }
//            if ($request->hasFile('pickup_doc')) {
//                $pickup_doc = (new Job())->modelFileUpload($request->file('pickup_doc'));
//                $this->_job->pickup_doc = Job::$filepath.'/'.$pickup_doc;
//            }
            if ($request->hasFile('job_requirement_doc')) {
                $job_requirement_doc= $this->_job->modelFileUpload($request->file('job_requirement_doc'));
                $this->_job->job_requirement_doc = Job::$filepath.'/'.$job_requirement_doc;
            }

            if (!$this->_job->save())
                throw new \Exception('Error saving job');


            if(!empty($request->images)){
                foreach ($request->images as $image){
                    if (!empty($image)) {
                        $this->_job->modelFileUpload($image,true,'banner_image');
                    }
                }
            }
            if(!empty($request->job_doc)){
                foreach ($request->job_doc as $file){
                    if (!empty($file)) {
                        $this->_job->modelFileUpload($file,true,'job_doc');
                    }
                }
            }

            DB::commit();
            $request->session()->flash('success', 'Job update successfully');

            return redirect(route('jobs.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('Jobs', $exception->getMessage());
        }

        return back()->withInput($request->all());
    }

    public function show(Request $request,$id){
        $files = File::where(['object'=>'JOBS','objectId'=>$id])->get();
        return view($this->mainViewFolder . '.images',['files' => $files]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {

            $this->_job = $this->_job->findOrFail($id);
            if ($this->_job->delete())
                $request->session()->flash('success', 'Job deleted successfully');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return back();

    }

    public function jobLogs(Request $request)
    {
        $jobLogs = JobLog::all();
        return view($this->mainViewFolder . 'jobLogs', ['jobLogs' => $jobLogs]);
    }

    public function jobComplete(Request $request)
    {
        $completeJobs = CompleteJob::all();
        return view($this->mainViewFolder . 'completeJobs', ['completeJobs' => $completeJobs]);
    }

    public function imageDelete(Request $request, $id)
    {
        try {
            $file = File::findOrFail($id);
            if ($file->delete())
                $request->session()->flash('success', 'File deleted successfully');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return back();
    }

    public function jobAssignMember(Request $request,$id){
        $model   = Job::where('id',$id)->first();
        $members = User::where('type',User::member)->get();
        $jobs    = Job::where('status',Job::STATUS_PENDING)->get();
        $jobLogs = JobLog::where('status',JobLog::STATUS_ASSIGN)->where('job_id',$id)->get()->toArray();
        return view($this->mainViewFolder . 'assignJob', ['model' => $model,'jobs'=>$jobs,'members'=>$members,'jobLogs'=>$jobLogs]);
    }

    public function jobAssignStore(Request $request,$job_id){
        $this->validate($request, [
            'users'=>"required",
        ]);
        try {
            // Start Transaction
            DB::beginTransaction();

            if(!empty($request->users)){
                foreach ($request->users as $memberId){
                    $jobLogExists = JobLog::where('member_id',$memberId)
                        ->where('job_id',$job_id)->first();
                    if(empty($jobLogExists)){
                        $jobLog = new JobLog();
                        $jobLog->member_id = $memberId;
                        $jobLog->job_id = $job_id;
                        $jobLog->status = JobLog::STATUS_ASSIGN;
                        if (!$jobLog->save())
                            throw new \Exception('Error saving job');
                    }
                }
            }

            DB::commit();
            $request->session()->flash('success', 'Job Assign successfully');

            return redirect(route('jobs.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('Jobs', $exception->getMessage());
        }

        return back()->withInput($request->all());
    }

    public function jobCalender(Request $request){
        $jobs = Job::select('date','title') // Adjust columns as needed
        ->where('status', Job::STATUS_PENDING)
            ->get();

        // Assuming you have already fetched the $jobs collection from your query
        $events = [];

        foreach ($jobs as $job) {
            $event = [
                'title' => $job->date,
                'start' => $job->date, // Assuming the date format is compatible with the calendar
            ];

            // You can add more properties to the $event array as needed

            $events[] = $event;
        }

        // Now, convert the $events array to JSON format
        $eventsJson = json_encode($events);


        return view($this->mainViewFolder . 'calender', ['jobs' => $eventsJson]);

    }

    public function airtableStore(Request $request){

        $this->validate($request, [
            'title'=>"required",
        ]);

        $curl = curl_init();

        $siteAddress = $request->title; // Set your desired site address here

        $apiEndpoint = 'https://api.airtable.com/v0/apppHdrp2Nf2Xmh4C/tblFrAEAVcqIWitdF';
        $queryParams = [
            'maxRecords' => 100, // Change the limit to 100 or any other desired value
            'filterByFormula' => "SEARCH('{$siteAddress}', {Site Address})", // Replace 'Site Address' with the actual column name
        ];

        $url = $apiEndpoint . '?' . http_build_query($queryParams);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer keyoVX37Y4yAgGDKJ',
                'Cookie: brw=brwpY3qS6OAZxUzyR; AWSALB=Qg7HfFbkN0p1PCMLaqv67XrED29i5kd0f1NvzgXSu5NUFBzMgOC/sJRS6soiap5iLj8yrdUlS//pYZ13i9yA6h5/v8RdffireNZ9JE/zThgzoFP1aoZOQSnw4PJy; AWSALBCORS=Qg7HfFbkN0p1PCMLaqv67XrED29i5kd0f1NvzgXSu5NUFBzMgOC/sJRS6soiap5iLj8yrdUlS//pYZ13i9yA6h5/v8RdffireNZ9JE/zThgzoFP1aoZOQSnw4PJy'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $responseDecode = json_decode($response);


//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://api.airtable.com/v0/apppHdrp2Nf2Xmh4C/tblFrAEAVcqIWitdF?maxRecords=10',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'GET',
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: Bearer keyoVX37Y4yAgGDKJ',
//                'Cookie: brw=brwpY3qS6OAZxUzyR; AWSALB=Qg7HfFbkN0p1PCMLaqv67XrED29i5kd0f1NvzgXSu5NUFBzMgOC/sJRS6soiap5iLj8yrdUlS//pYZ13i9yA6h5/v8RdffireNZ9JE/zThgzoFP1aoZOQSnw4PJy; AWSALBCORS=Qg7HfFbkN0p1PCMLaqv67XrED29i5kd0f1NvzgXSu5NUFBzMgOC/sJRS6soiap5iLj8yrdUlS//pYZ13i9yA6h5/v8RdffireNZ9JE/zThgzoFP1aoZOQSnw4PJy'
//            ),
//        ));
//
//        $response = curl_exec($curl);
//
//        curl_close($curl);
//        $responseDecode = json_decode($response);

        $result = '';
        foreach ($responseDecode->records as $key => $value){
            if($value->fields->{"Site Address"} == $request->title){
                $result = [
                    "address"       => @$value->fields->{"Site Address"},
                    "job_doc"       => @$value->fields->{"Plans"},
                    "banner_images" => @$value->fields->{"Renders"},
                    "square"        => @$value->fields->{"Sqft Installed"},
                ];
                break;
            }
        }


        try {
            if(empty($result)){
                throw new \Exception('data not found!');
            }

            // Start Transaction
            $data = $request->all();
            DB::beginTransaction();
            $this->_job = new Job($data);
            $this->_job->address = $result['address'];
            $this->_job->square  = $result['square'];
            if (!$this->_job->save())
                throw new \Exception('Error saving job');


            if(!empty($result['job_doc'])){
                foreach ($result['job_doc'] as $image){
                    // Download the image using Laravel's HTTP client
                    $response = Http::get($image->url);
                    if ($response->successful()) {

                        // Get the image data from the response body
                        $imageData = $response->body();

                        // Generate a unique filename for the image (you may customize this logic)
                        $filename = "image_" . now()->format('Y_m_d\TH_i_s\Z');
                        // Extract the file extension from the URL
                        $extension = pathinfo(parse_url($image->url, PHP_URL_PATH), PATHINFO_EXTENSION);
                        // Ensure the extension has a dot prefix (e.g., ".png")
                        if (!empty($extension) && !Str::startsWith($extension, '.')) {
                            $extension = '.' . $extension;
                        }

                        if($image->type == "application/pdf"){
                            // Append the extension to the filename or use a default extension if not provided
                            $filename .= $extension ?: '.pdf'; // Default to '.png' if extension is empty
                        }else{
                            // Append the extension to the filename or use a default extension if not provided
                            $filename .= $extension ?: '.png'; // Default to '.png' if extension is empty
                        }

                        // Create a temporary file and write the image data to it
                        $tempFilePath = tempnam(sys_get_temp_dir(), 'temp_image_');

                        file_put_contents($tempFilePath, $imageData);

                        // Create an UploadedFile instance from the temporary file
                        $uploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                            $tempFilePath, // Temporary file path
                            $filename, // Original name
                            'image/png', // Default MIME type (can be overridden)
                            null, // Error code
                            true // Test mode (true to prevent moving the file)
                        );

                        $this->_job->modelFileUpload($uploadedFile,true,'job_doc');

                    } else {
                        throw new \Exception('File saving error.');
                    }
                }
            }
            if(!empty($result['banner_images'])){
                foreach ($result['banner_images'] as $image){
                    // Download the image using Laravel's HTTP client
                    $response = Http::get($image->url);
                    if ($response->successful()) {
                        // Get the image data from the response body
                        $imageData = $response->body();

                        // Generate a unique filename for the image (you may customize this logic)
                        $filename = "image_" . now()->format('Y_m_d\TH_i_s\Z');

                        // Extract the file extension from the URL
                        $extension = pathinfo(parse_url($image->url, PHP_URL_PATH), PATHINFO_EXTENSION);

                        // Ensure the extension has a dot prefix (e.g., ".png")
                        if (!empty($extension) && !Str::startsWith($extension, '.')) {
                            $extension = '.' . $extension;
                        }

                        // Append the extension to the filename or use a default extension if not provided
                        $filename .= $extension ?: '.png'; // Default to '.png' if extension is empty

                        // Create a temporary file and write the image data to it
                        $tempFilePath = tempnam(sys_get_temp_dir(), 'temp_image_');
                        file_put_contents($tempFilePath, $imageData);

                        // Create an UploadedFile instance from the temporary file
                        $uploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                            $tempFilePath, // Temporary file path
                            $filename, // Original name
                            'image/png', // Default MIME type (can be overridden)
                            null, // Error code
                            true // Test mode (true to prevent moving the file)
                        );

                        $this->_job->modelFileUpload($uploadedFile,true,'banner_image');

                    } else {
                        throw new \Exception('File saving error.');
                    }
                }
            }

            DB::commit();
            $request->session()->flash('success', 'Job store successfully');

            return redirect(route('jobs.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('error', $exception->getMessage());
        }
        return back()->withInput($request->all());

    }


}
