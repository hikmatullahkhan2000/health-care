<?php

namespace App\Http\Controllers;

use App\Models\Params;
use App\Models\Services;
use App\Models\Specialty;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use PhpParser\Builder\Param;

class ServiceController extends Controller
{

    protected $_service;

    public function __construct(Services $service)
    {
        $this->_service = $service;
    }

    public function index()
    {
        $services = Services::all();
        return view('services.index',compact('services'));
    }

    public function create()
    {
        $model = $this->_service;
        return view('services.form',compact('model'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        Services::create($request->all());
        return redirect()->route("services.index")->with("success", "Service Created Successfully");
    }
    public function edit($id)
    {
        $model = Services::find($id);
        return view('services.form',compact('model'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        Services::find($id)->update($request->all());
        return redirect()->route("services.index")->with("success", "Service Updated Successfully");
    }

    public function destroy($id)
    {
        Services::find($id)->delete();
        return redirect()->route("services.index")->with("success", "Service Deleted Successfully");
    }

    public function show($id)
    {
        $model = Services::find($id);
        return view('services.show',compact('model'));
    }
}
