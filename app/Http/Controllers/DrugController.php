<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\CompleteJob;
use App\Models\Drug;
use App\Models\File;
use App\Models\Job;
use App\Models\JobLog;
use App\Models\Specialty;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;


class DrugController extends Controller
{


    protected $mainViewFolder = 'drugs.';


    /**
     * The user repository instance.
     */
    protected $_drug;

    public function __construct(Drug $job)
    {
        $this->_drug = $job;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $drug = Drug::orderBy('id', 'DESC')->get();
        return view($this->mainViewFolder . 'index', ['drugs' => $drug]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->_drug;
        $specialties = Specialty::get();
        return view($this->mainViewFolder . 'form', compact('model','specialties'));
    }


    public function edit($id)
    {
        $model = Drug::findOrFail($id);
        $specialties = Specialty::get();
        return view($this->mainViewFolder . 'form', compact('model','specialties'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>"required",
            'long_description'=>"required",
            'type'=>"required",
        ]);

        try {
            // Start Transaction
            $data = $request->all();
            DB::beginTransaction();
            $this->_drug = new Drug($data);
            $firstLetter = substr($request->title, 0, 1);
            $this->_drug->short_title = $firstLetter;
            $this->_drug->slug = Str::slug($request->title);
            if ($request->hasFile('banner')) {
                $banner = (new Drug())->modelFileUpload($request->file('banner'));
                $this->_drug->banner = Drug::$filepath.'/'.$banner;
            }
            if(!empty($request->file('video_link'))){
                $file = (new Drug())->modelFileUpload($request->file('video_link'));
                $this->_drug->video_link = Drug::$filepath.'/'.$file;
            }

            if (!$this->_drug->save())
                throw new \Exception('Error saving job');


            DB::commit();
            $request->session()->flash('success', 'Drugs store successfully');

            return redirect(route('drugs.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('Drugs', $exception->getMessage());
        }
        return back()->withInput($request->all());
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>"required",
            'long_description'=>"required",
            'type'=>"required",
        ]);

        try {

            // Start Transaction
            DB::beginTransaction();

            $this->_drug = $this->_drug->findOrFail($id);
            $this->_drug->title = $request->title;
            $this->_drug->short_title = $request->short_title;
            $this->_drug->short_description = $request->short_description;
            $firstLetter = substr($request->title, 0, 1);
            $this->_drug->short_title = $firstLetter;
            $this->_drug->slug = Str::slug($request->title);

            if ($request->hasFile('banner')) {
                $banner = $this->_drug->modelFileUpload($request->file('banner'));
                $this->_drug->banner = Drug::$filepath.'/'.$banner;
            }

            if(!empty($request->file('video_link'))){
                $file = (new Drug())->modelFileUpload($request->file('video_link'));
                $this->_drug->video_link = Drug::$filepath.'/'.$file;
            }


            if (!$this->_drug->save())
                throw new \Exception('Error saving Drugs');

            DB::commit();
            $request->session()->flash('success', 'Drugs update successfully');

            return redirect(route('drugs.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('drugs', $exception->getMessage());
        }

        return back()->withInput($request->all());
    }

    public function show(Request $request,$id){
    }

    public function destroy(Request $request, $id)
    {
        try {

            $this->_drug = $this->_drug->findOrFail($id);
            if ($this->_drug->delete())
                $request->session()->flash('success', 'Drug deleted successfully');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return back();

    }

}
