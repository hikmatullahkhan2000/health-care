<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Contact;
use App\Models\Disease;
use App\Models\Drug;
use App\Models\Procedure;
use App\Models\RequestAppointment;
use App\Models\User;
use Doctrine\DBAL\Result;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        $doctors = User::where('type',User::member)->get();
        return view('front.index',compact("doctors"));
    }

    public function diseasesCondition(Request $request){

        $shortTitle = $request->short_title;
        $diseases = Disease::query();
        if (!empty($request->title)) {
            $diseases->where('title', 'like', '%' . $request->title . '%');
        }elseif (!empty($request->short_title)){
            $diseases->where('short_title', '=',  $request->short_title);
        }elseif (!empty($request->specialty)){
            $diseases->where('specialty', '=',  $request->specialty);
        }
        // Order the diseases by title in alphabetical order (ascending)
        $diseases->orderBy('title', 'asc');
        $diseases = $diseases->get();
        return view('front.diseasesCondition',compact('diseases','shortTitle'));
    }
    public function diseasesConditionDetail(Request $request,$slug){
        $diseases = Disease::where('slug',$slug)->first();
        if(empty($diseases)){
            return redirect()->back();
        }
        return view('front.diseasesDetail',compact('diseases'));
    }
    public function drug(Request $request){
        $shortTitle = $request->short_title;
        $type = $request->type;
        $drugs = Drug::query();
        if (!empty($request->title)) {
            $drugs->where('title', 'like', '%' . $request->title . '%');
        }elseif (!empty($request->short_title)){
            $drugs->where('short_title', '=',  $request->short_title);
        }elseif (!empty($request->type)){
            $drugs->where('type', '=',  $request->type);
        }
        $drugs->orderBy('title', 'asc');
        $drugs = $drugs->get();

        return view('front.drug',compact('drugs','shortTitle','type'));
    }
    public function drugDetail(Request $request,$slug){
        $drug = Drug::where('slug',$slug)->first();
        if(empty($drug)){
            return redirect()->back();
        }
        return view('front.drugDetail',compact('drug'));
    }
    public function procedure(Request $request){
        $shortTitle = $request->short_title;
        $procedures = Procedure::query();
        if (!empty($request->title)) {
            $procedures->where('title', 'like', '%' . $request->title . '%');
        }elseif (!empty($request->short_title)){
            $procedures->where('short_title', '=',  $request->short_title);
        }elseif (!empty($request->specialty)){
            $procedures->where('specialty', '=',  $request->specialty);
        }
        $procedures->orderBy('title', 'asc');
        $procedures = $procedures->get();
        return view('front.procedure',compact('procedures','shortTitle'));
    }
    public function procedureDetail(Request $request,$slug){
        $procedure = Procedure::where('slug',$slug)->first();
        if(empty($procedure)){
            return redirect()->back();
        }
        return view('front.procedureDetail',compact('procedure'));
    }
    public function doctor(Request $request){
        $doctors = User::where('type',User::member);
        //filter state equal to state
        if (!empty($request->state)) {
            $doctors->where('state', '=', $request->state);
        }

        //filter specialty is id equal to specialty
        if (!empty($request->specialty)) {
            $doctors->where('specialty', '=', $request->specialty);
        }


        $doctors =  $doctors->get();
        return view('front.doctor',compact("doctors"));
    }
    public function newsfeeds(Request $request){
        $blogs = Blog::query();

        if (!empty($request->title)) {
            // Like query for title
            $blogs->where('title', 'like', '%' . $request->title . '%');
        }

        $blogs = $blogs->get();

        return view('front.newsFeeds',compact('blogs'));
    }

    public function newsFeedDetail(Request $request,$slug){
        $blog = Blog::where('slug',$slug)->first();
        if(empty($blog)){
            return redirect()->back();
        }
        return view('front.newsFeedDetail',compact('blog'));
    }

    public function aboutus(Request $request){
        return view('front.about-us');
    }
    public function contactus(Request $request){
        return view('front.contactus');
    }

    public function contactStore(Request $request){
        $this->validate($request, [
            'first_name'=> 'required',
            'last_name' => 'required',
            'phone'     => 'required',
            'email'     => 'required',
        ]);
        try {
            $contact = new Contact($request->all());
            if (!$contact->save()) {
                throw new \Exception('Failed to save data', 422);
            }
            $request->session()->flash('success', 'Contactus stored successfully');
            return redirect(route('home'));
        } catch (\Exception $exception) {
            $request->session()->flash('error', $exception->getMessage());
        }
        return back()->withInput($request->all());
    }

    public function requestAppointment(Request $request,$id){
        return view('front.appointment',compact("id"));
    }

    public function appointmentStore(Request $request){
        $this->validate($request, [
            'full_name'=> 'required',
            'phone'     => 'required',
            'email'     => 'required',
            'user_id'     => 'required',
        ]);
        try {
            $requestAppointment = new RequestAppointment($request->all());
            if (!$requestAppointment->save()) {
                throw new \Exception('Failed to save data', 422);
            }
            $request->session()->flash('success', 'Appointment stored successfully');
            return redirect(route('home'));
        } catch (\Exception $exception) {
            $request->session()->flash('error', $exception->getMessage());
        }
        return back()->withInput($request->all());
    }
}
