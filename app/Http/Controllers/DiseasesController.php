<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\CompleteJob;
use App\Models\Disease;
use App\Models\Drug;
use App\Models\File;
use App\Models\Job;
use App\Models\JobLog;
use App\Models\Specialty;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;


class DiseasesController extends Controller
{


    protected $mainViewFolder = 'diseases.';


    /**
     * The user repository instance.
     */
    protected $_diseases;

    public function __construct(Disease $_diseases)
    {
        $this->_diseases = $_diseases;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $diseases = Disease::orderBy('id', 'DESC')->get();
        return view($this->mainViewFolder . 'index', ['diseases' => $diseases]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->_diseases;
        $specialties = Specialty::get();
        return view($this->mainViewFolder . 'form', compact('model','specialties'));
    }


    public function edit($id)
    {
        $model = Disease::findOrFail($id);
        $specialties = Specialty::get();
        return view($this->mainViewFolder . 'form', compact('model','specialties'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>"required",
            'long_description'=>"required",
            'specialty'=>"required",
        ]);

        try {
            // Start Transaction
            $data = $request->all();
            DB::beginTransaction();
            $this->_diseases = new Disease($data);
            $firstLetter = substr($request->title, 0, 1);
            $this->_diseases->short_title = $firstLetter;
            $this->_diseases->slug = Str::slug($request->title);

            if ($request->hasFile('banner')) {
                $banner = (new Disease())->modelFileUpload($request->file('banner'));
                $this->_diseases->banner = Disease::$filepath.'/'.$banner;
            }

            if(!empty($request->file('video_link'))){
                $file = (new Disease())->modelFileUpload($request->file('video_link'));
                $this->_diseases->video_link = Disease::$filepath.'/'.$file;
            }

            if (!$this->_diseases->save())
                throw new \Exception('Error saving Diseases');


            DB::commit();
            $request->session()->flash('success', 'Diseases store successfully');

            return redirect(route('diseases.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('Drugs', $exception->getMessage());
        }
        return back()->withInput($request->all());
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>"required",
            'long_description'=>"required",
            'specialty'=>"required",
        ]);

        try {

            // Start Transaction
            DB::beginTransaction();

            $this->_diseases = $this->_diseases->findOrFail($id);
            $this->_diseases->title = $request->title;
            $this->_diseases->short_title = $request->short_title;
            $this->_diseases->specialties = $request->specialties;
            $this->_diseases->short_description = $request->short_description;
            $this->_diseases->long_description = $request->long_description;
            $firstLetter = substr($request->title, 0, 1);
            $this->_diseases->short_title = $firstLetter;
            $this->_diseases->slug = Str::slug($request->title);

            if ($request->hasFile('banner')) {
                $banner = $this->_diseases->modelFileUpload($request->file('banner'));
                $this->_diseases->banner = Drug::$filepath.'/'.$banner;
            }

            if(!empty($request->file('video_link'))){
                $file = (new Disease())->modelFileUpload($request->file('video_link'));
                $this->_diseases->video_link = Disease::$filepath.'/'.$file;
            }


            if (!$this->_diseases->save())
                throw new \Exception('Error saving Diseases');

            DB::commit();
            $request->session()->flash('success', 'Diseases update successfully');

            return redirect(route('diseases.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('diseases', $exception->getMessage());
        }

        return back()->withInput($request->all());
    }

    public function show(Request $request,$id){
    }

    public function destroy(Request $request, $id)
    {
        try {

            $this->_diseases = $this->_diseases->findOrFail($id);
            if ($this->_diseases->delete())
                $request->session()->flash('success', 'Diseases deleted successfully');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return back();

    }

}
