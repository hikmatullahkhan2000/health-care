<?php

namespace App\Http\Controllers;

use App\Models\Params;
use App\Models\Specialty;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use PhpParser\Builder\Param;

class SpecialtyController extends Controller
{
    //create store and index and destory edit update create please
    protected $_specialty;

    public function __construct(Specialty $specialty)
    {
        $this->_specialty = $specialty;
    }

    public function index()
    {
        $specialties = Specialty::all();
        return view('specialties.index',compact('specialties'));
    }

    public function create()
    {
        $model = $this->_specialty;
        return view('specialties.form',compact('model'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        Specialty::create($request->all());
        return redirect()->route("specialties.index")->with("success", "Specialty Created Successfully");
    }

    public function edit($id)
    {
        $model = Specialty::find($id);
        return view('specialties.form',compact('model'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        Specialty::find($id)->update($request->all());
        return redirect()->route("specialties.index")->with("success", "Specialty Updated Successfully");
    }
    public function destroy($id)
    {
        Specialty::find($id)->delete();
        return redirect()->route("specialties.index")->with("success", "Specialty Deleted Successfully");
    }

}
