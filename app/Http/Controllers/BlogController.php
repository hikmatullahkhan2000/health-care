<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\Blog;
use App\Models\CompleteJob;
use App\Models\Disease;
use App\Models\Drug;
use App\Models\File;
use App\Models\Job;
use App\Models\JobLog;
use App\Models\Procedure;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;


class BlogController extends Controller
{


    protected $mainViewFolder = 'blogs.';


    /**
     * The user repository instance.
     */
    protected $_blog;

    public function __construct(Blog $_blog)
    {
        $this->_blog = $_blog;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $blogs = Blog::orderBy('id', 'DESC')->get();
        return view($this->mainViewFolder . 'index', ['blogs' => $blogs]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->_blog;
        return view($this->mainViewFolder . 'form', compact('model'));
    }


    public function edit($id)
    {
        $model = Blog::findOrFail($id);
        return view($this->mainViewFolder . 'form', compact('model'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>"required",
            'long_description'=>"required",
        ]);

        try {
            // Start Transaction
            $data = $request->all();
            DB::beginTransaction();
            $this->_blog = new Blog($data);
            $this->_blog->slug = Str::slug($request->title);

            if ($request->hasFile('banner')) {
                $banner = (new Blog())->modelFileUpload($request->file('banner'));
                $this->_blog->banner = Blog::$filepath.'/'.$banner;
            }

            if (!$this->_blog->save())
                throw new \Exception('Error saving Blogs');


            DB::commit();
            $request->session()->flash('success', 'Blogs store successfully');

            return redirect(route('blogs.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('blogs', $exception->getMessage());
        }
        return back()->withInput($request->all());
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>"required",
            'long_description'=>"required",
        ]);

        try {
            // Start Transaction
            DB::beginTransaction();

            $this->_blog = $this->_blog->findOrFail($id);
            $this->_blog->title = $request->title;
            $this->_blog->specialties = $request->specialties;
            $this->_blog->short_description = $request->short_description;
            $this->_blog->category = $request->category;
            $this->_blog->slug = Str::slug($request->title);

            if ($request->hasFile('banner')) {
                $banner = $this->_blog->modelFileUpload($request->file('banner'));
                $this->_blog->banner = Blog::$filepath.'/'.$banner;
            }

            if (!$this->_blog->save())
                throw new \Exception('Error saving Blogs');

            DB::commit();
            $request->session()->flash('success', 'Blogs update successfully');

            return redirect(route('blogs.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            dd( $exception->getMessage());
            $request->session()->flash('blogs', $exception->getMessage());
        }

        return back()->withInput($request->all());
    }

    public function show(Request $request,$id){
    }

    public function destroy(Request $request, $id)
    {
        try {

            $this->_blog = $this->_blog->findOrFail($id);
            if ($this->_blog->delete())
                $request->session()->flash('success', 'Blogs deleted successfully');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return back();

    }

}
