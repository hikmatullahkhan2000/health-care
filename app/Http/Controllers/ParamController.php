<?php

namespace App\Http\Controllers;

use App\Models\Params;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use PhpParser\Builder\Param;

class ParamController extends Controller
{

    protected $_param;

    public function __construct(Params $param)
    {
        $this->_param = $param;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = Params::all();
        return view('params.index',compact('params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->_param;
        return view('params.form',compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'key' => 'required|max:255',
            'value' => 'required',
            'type' => ['required',  Rule::in(['value', 'text'])],
        ]);

        Params::create($request->all());
        return redirect()->route("params.index")->with("success", "Param Created Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Params  $params
     * @return \Illuminate\Http\Response
     */
    public function show(Params $params)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Params  $params
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Params::findOrFail($id);
        return view('params.form',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Params  $params
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $params = Params::findOrFail($id);
        $params->update($request->all());
        return redirect()->route("params.index")->with("success", "Param Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Params  $params
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $params = Params::findOrFail($id);
        $params->delete();
        return redirect()->route("params.index")->with("success", "Param Deleted Successfully");
    }

    public function toggleStatus($id)
    {
        $param = Params::findOrFail($id);
        $status = (int) ! (bool) $param->active;
        $param->update(["active" => $status]);
        return redirect()->route("params.index");
    }
}
