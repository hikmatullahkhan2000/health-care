<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function edit(Request $request)
    {
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the user's profile information.
     *
     * @param  \App\Http\Requests\ProfileUpdateRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $user = $request->user();
        if(isset($request->first_name)) $user->first_name = $request->first_name;
        if(isset($request->last_name)) $user->last_name = $request->last_name;
        if(isset($request->email)) $user->email = $request->email;
        if(isset($request->address)) $user->address = $request->address;
        if(isset($request->phone)) $user->phone = $request->phone;
        if($request->file('photo')) $user->photo = (new User)->modelFileUpload($request->file("photo"));
        if ($request->user()->isDirty('email')) $request->user()->email_verified_at = null;

        $user->save();

        return Redirect::route('profile.edit')->with('success', 'profile-updated');
    }

    /**
     * Delete the user's account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current-password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => [
                'required', function ($attr, $value, $fail) {
                    if (!Hash::check($value, Auth::user()->password)) {
                        $fail('Old Password didn\'t match.');
                    }
                },
            ],
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
        ]);

        $user = $request->user();

        try {
            $user->password = Hash::make($request->password);
            $user->save();
            return Redirect::route('profile.change-password')->with('success', 'Password changed successfully');
        } catch (\Exception $exception) {
            return Redirect::route('profile.change-password')->with('error', 'Something Went Wrong.');
        }
    }
}
