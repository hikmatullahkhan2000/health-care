<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\CompleteJob;
use App\Models\Disease;
use App\Models\Drug;
use App\Models\File;
use App\Models\Job;
use App\Models\JobLog;
use App\Models\Procedure;
use App\Models\Specialty;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;


class ProcedureController extends Controller
{


    protected $mainViewFolder = 'procedure.';


    /**
     * The user repository instance.
     */
    protected $_procedure;

    public function __construct(Procedure $_procedure)
    {
        $this->_procedure = $_procedure;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $procedures = Procedure::orderBy('id', 'DESC')->get();
        return view($this->mainViewFolder . 'index', ['procedures' => $procedures]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->_procedure;
        $specialties = Specialty::get();
        return view($this->mainViewFolder . 'form', compact('model','specialties'));
    }


    public function edit($id)
    {
        $model = Procedure::findOrFail($id);
        $specialties = Specialty::get();
        return view($this->mainViewFolder . 'form', compact('model','specialties'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>"required",
            'specialty'=>"required",
        ]);

        try {
            // Start Transaction
            $data = $request->all();
            DB::beginTransaction();
            $this->_procedure = new Procedure($data);
            $firstLetter = substr($request->title, 0, 1);
            $this->_procedure->short_title = $firstLetter;
            $this->_procedure->slug = Str::slug($request->title);

            if ($request->hasFile('banner')) {
                $banner = (new Procedure())->modelFileUpload($request->file('banner'));
                $this->_procedure->banner = Procedure::$filepath.'/'.$banner;
            }
            if(!empty($request->file('video_link'))){
                $file = (new Procedure())->modelFileUpload($request->file('video_link'));
                $this->_procedure->video_link = Procedure::$filepath.'/'.$file;
            }

            if (!$this->_procedure->save())
                throw new \Exception('Error saving Procedure');


            DB::commit();
            $request->session()->flash('success', 'Procedure store successfully');

            return redirect(route('procedures.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('Procedure', $exception->getMessage());
        }
        return back()->withInput($request->all());
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>"required",
            'specialty'=>"required",
        ]);

        try {

            // Start Transaction
            DB::beginTransaction();

            $this->_procedure = $this->_procedure->findOrFail($id);
            $this->_procedure->title = $request->title;
            $this->_procedure->short_title = $request->short_title;
            $this->_procedure->specialties = $request->specialties;
            $this->_procedure->short_description = $request->short_description;
            $firstLetter = substr($request->title, 0, 1);
            $this->_procedure->short_title = $firstLetter;
            $this->_procedure->slug = Str::slug($request->title);

            if ($request->hasFile('banner')) {
                $banner = $this->_procedure->modelFileUpload($request->file('banner'));
                $this->_procedure->banner = Drug::$filepath.'/'.$banner;
            }

            if(!empty($request->file('video_link'))){
                $file = (new Procedure())->modelFileUpload($request->file('video_link'));
                $this->_procedure->video_link = Procedure::$filepath.'/'.$file;
            }

            if (!$this->_procedure->save())
                throw new \Exception('Error saving Procedure');

            DB::commit();
            $request->session()->flash('success', 'Procedure update successfully');

            return redirect(route('procedures.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('procedure', $exception->getMessage());
        }

        return back()->withInput($request->all());
    }

    public function show(Request $request,$id){
    }

    public function destroy(Request $request, $id)
    {
        try {

            $this->_procedure = $this->_procedure->findOrFail($id);
            if ($this->_procedure->delete())
                $request->session()->flash('success', 'Procedure deleted successfully');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return back();

    }

}
