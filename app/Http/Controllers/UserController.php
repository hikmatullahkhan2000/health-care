<?php

namespace App\Http\Controllers;

use App\Facades\FirebaseService;
use App\Http\Resources\UserResource;
use App\Models\Company;
use App\Models\ContactUs;
use App\Models\JobCategories;
use App\Models\Notifications;
use App\Models\RequestAppointment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
//use Kreait\Firebase\Contract\Database;
use Kreait\Firebase\Database;
use Kreait\Laravel\Firebase\Facades\Firebase;
use Kreait\Firebase\Factory;
use function Kreait\Firebase\RemoteConfig\equalsTo;

//use Kreait\Firebase\Auth;


class UserController extends Controller
{


    protected $mainViewFolder = 'users.';


    /**
     * The user repository instance.
     */
    protected $_user;

    public function __construct(User $user)
    {
        $this->_user = $user;

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::fetch();

        return view($this->mainViewFolder . 'index', ['users' => $users, 'type' => User::type]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $model = $this->_user;
        return view($this->mainViewFolder . 'form', compact('model'));
    }


    public function edit($id)
    {
        $model = User::findOrFail($id);
        $model->password = '';
        return view($this->mainViewFolder . 'form', compact('model'));
    }


//    public function listShow(Request $request)
//    {
//        $users = User::query();
//        $users = $users->orderBy("id",'desc');
//        $users = $users->paginate($request->get('per_page',10));
//        return  $this->sendResponse(UserResource::collection($users),'User data');
//    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'type' => 'required',
//            'job_category_id' => 'required',
        ]);

        try {
            // Start Transaction
            DB::beginTransaction();
            $this->_user = new User($request->except('password', 'password_confirmation'));
            $this->_user->password = bcrypt($request->password);

            if (!$this->_user->save())
                throw new \Exception('Error saving user');

            DB::commit();
            $request->session()->flash('success', 'User store successfully');

            return redirect(route('users.index'));

        } catch (\Exception $exception) {
            DB::rollBack();
            $request->session()->flash('User', $exception->getMessage());
        }
        return back()->withInput($request->all());
    }


    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError("Validation Error.",$validator->errors());
        }


        try {
            $this->_user = $this->_user->findOrFail($id);

            $this->_user->fill($request->except('password'));

            if ($request->password != null) {
                $this->_user->password = bcrypt($request->password);
            }
            // if the value exist in request filled
            if (!$this->_user->save())
                throw new \Exception('User not updated');

            $request->session()->flash('success', 'User Update successfully');

            return redirect(route('users.index'));

        } catch (\Exception $exception) {
            $request->session()->flash('error', $exception->getMessage());
        }

        return back()->withInput($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $this->_user = $this->_user->findOrFail($id);
            if ($this->_user->delete())
                $request->session()->flash('success', 'User deleted successfully');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return back();

    }

    public function VerifyDocument(int $user_id)
    {
        $user = User::findOrFail($user_id);
        if ($user->document) {
            $user->update(["doc_approved" => 1]);
            return redirect()->route('users.index')->with('success', 'Document Approved');
        }
        return redirect()->route('users.index')->with('warning', 'No Document Found');
    }


    public function Appointments(Request $request)
    {
        $appointments = RequestAppointment::query();
        if(Auth::user()->type != User::admin_user){
            $appointments  = $appointments->where('user_id',Auth::id());
        }
        $appointments  = $appointments->orderBy('id','desc')->paginate($request->get('per_page',10));
        return view($this->mainViewFolder . 'appointments', compact('appointments'));
    }





}
