<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Auth;


class FirebaseService extends Facade
{
    public static $credential = [
        "type"=> "",
        "project_id"=> "",
        "private_key_id"=> "",
        "private_key"=> "",
        "client_email"=> "",
        "client_id"=> "",
        "auth_uri"=> "",
        "token_uri"=> "",
        "auth_provider_x509_cert_url"=>"",
        "client_x509_cert_url"=> ""
    ];


    protected static function getFacadeAccessor()
    {
        return 'firebase-service';
    }

    public static function connection(){
        $credentials = json_encode(self::$credential);
//        $firebase = (new Factory())->withServiceAccount($credentials)->createDatabase();
        $firebase = (new Factory)
            ->withServiceAccount($credentials)
            ->withDatabaseUri('')
            ->createDatabase();
        return $firebase;
    }

    public static function auth(){
        $credentials = json_encode(self::$credential);
        $firebase = (new Factory)->withServiceAccount($credentials)->createAuth();
        return $firebase;
    }




}
