<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class MemberFcmToken extends Model
{
    protected $fillable = [
        "session_id",
        "customer_id",
        "shop_user_id",
        "fcm_token",
        "status",
    ];
}
