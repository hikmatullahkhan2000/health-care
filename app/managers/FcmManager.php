<?php


namespace App\managers;


use App\Models\Customer;
use App\Models\FcmHistory;
use App\Models\FcmMessage;
use App\Models\Member;
use App\Models\Params;
use Illuminate\Support\Facades\Auth;

class FcmManager
{

    static $API_KEY = '1:923413281002:android:57db448895347e4790bf93';
    static $SERVER_KEY = 'AAAAYY6_gsI:APA91bG1IM9u1mUI4zBUkTCA0L_QMuP3E55xldO4Zo4lgNuwmCBJ6KbXsDrgxBz3pl8CP2y4YkVd5iz5V6tH0owGaMqtIAa42lGR_SH0n2oJHEKN8Y_UUSyhJ_FjyLcVOdWj2OUccsEm';

    public static function sendAutoNotificationByStatus(Member $customer, $data = [])
    {
        if (env("APP_ENV") == "local")
            return false;

        $params = Params::where("key", "=", "allowNotification")->first();

        if (!empty($params)) {
            if (!$params->value)
                return false;
        }

        if (empty($customer) || empty($customer->fcmAppToken) || empty($customer->fcmAppToken->fcm_token))
            return false;

        $data["fcm_token"] = "c3GGOyicSDWLp9nkMMT1TD:APA91bH_czut2TpyQw1V0xscmLwkx9-SyF9uy6G6PUacFEB8ZaBsp1v2EWJqzpC-ERzIETi1Aw1rlp5vLPktDnGGwNE6GHzdi47XiybxICmqLejinS-SqxX1ml99mldAAR5cPFiAHfiH"; //$customer->fcmAppToken->fcm_token;
        $data["customer_id"] = $customer->id;
        $data["domain_url"] = (!empty($data["domain_url"])) ? $data["domain_url"] : "https://www.dietbox.pk";
        $data["image"] = (!empty($data["image"])) ? $data["image"] : "https://dietbox.pk/aron-assets/images/logo.png";

        $response = SELF::send($data);

        return true;
    }

    public static function sendToFcmMember(FcmMessage $messageModel, $fcmCustomer)
    {
        if (strtoupper(env('APP_ENV')) != 'PRODUCTION') {
            return;
        }


        if (empty($messageModel))
            return false;

        $response = SELF::send([
            "fcm_token" => $fcmCustomer->fcm_token,
            "title" => $messageModel->title,
            "image" => ($messageModel->image) ? env("IMAGE_URL") . "/" . $messageModel->image : '',
            "message" => $messageModel->message,
            "domain_url" => $messageModel->domain_url,
            "fcm_message_id" => $messageModel->id,
            "object" => $messageModel->OBJECT,
            "objectId" => $messageModel->OBJECT_ID,
            "customer_id" => $fcmCustomer->customer_id,
            "shop_user_id" => $fcmCustomer->shop_user_id,
        ]);
        if (!$response)
            throw new \Exception("something went wrong ....");

    }


    public static function sendByMemberIds(FcmMessage $messageModel, array $memberIds)
    {

        if (empty($messageModel))
            return false;

        foreach ($memberIds as $memberId) {
            $member = Member::findOrFail($memberId);
            if (!empty($member) && !empty($member->fcmAppToken)) {
                $response = SELF::send([
                    "fcm_token" => $member->fcmAppToken->fcm_token,
                    "title" => $messageModel->title,
                    "image" => ($messageModel->image) ? env("image_base_url") . "/" . $messageModel->image : '',
                    "message" => $messageModel->message,
                    "object" => $messageModel->object,
                    "objectId" => $messageModel->objectId,
                    "domain_url" => $messageModel->domain_url,
                    "fcm_message_id" => $messageModel->id,
                    "member_id" => $member->id
                ]);
                if (!$response)
                    throw new \Exception("something went wrong ....");
            }

        }
    }

    public static function sendCustomMsgBymemberIds($data, array $memberIds)
    {
        foreach ($memberIds as $memberId) {
            $member = Member::findOrFail($memberId);
            if (!empty($member) && !empty($member->fcmAppToken)) {
                $data['fcm_token'] = $member->fcmAppToken->fcm_token;
                $data['member_id'] = $member->id;
                $response = SELF::send($data);
                if (!$response)
                    throw new \Exception("something went wrong ....");
            }

        }
    }

    public static function sendMsgMember($msgData,$memberId)
    {

        if (empty($msgData))
            return false;

        $member = Member::findOrFail($memberId);

        if (!empty($member) && !empty($member->fcmAppToken)) {
            $response = SELF::send([
                "fcm_token" => $member->fcmAppToken->fcm_token,
                "title" => $msgData['title'],
                "image" => '',
                "message" => $msgData['msg'],
                "object" => '',
                "objectId" => '',
                "domain_url" => '',
                "fcm_message_id" => '',
                "member_id" => $memberId
            ]);
            if (!$response)
                throw new \Exception("something went wrong ....");
        }

    }

    public static function send(array $data)
    {
//        if(strtoupper(env('APP_ENV'))!='PRODUCTION'){
//            return;
//        }
        // data ...

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $to = !empty($data['fcm_token']) ? $data['fcm_token'] : "notfound";
        $title = $data['title'];
        $image_url = $data['image'];
        $message = $data['message'];
        $action_url = $data['domain_url'];
//        $api_key = Self::$API_KEY;
        $server_key = SELF::$SERVER_KEY;

        $msg = array(
            'body' => $message,
            'title' => $title,
            'click_action' => "FLUTTER_NOTIFICATION_CLICK",
            'object' => $data['object'] ?? '',
            'objectId' => !empty($data['objectId']) ? $data['objectId'] : null,
            'icon' => $image_url,/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/

        );
        $fields = array(
            'to' => $to,
            'data' => $msg,
            'notification' => $msg
        );


        $headers = array
        (
            'Authorization: key=' . $server_key,
            'Content-Type: application/json',
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err)
            throw new \Exception($err);

        $resdecoded = json_decode($response, true);
        $resp = (string)$response;
        $fcm_reponse = new FcmHistory([
            "user_token" => $data['fcm_token'],
            "fcm_message_id" => (!empty($data['fcm_message_id'])) ? $data['fcm_message_id'] : null,
            "multicast_id" => (!empty($resdecoded['multicast_id'])) ? $resdecoded['multicast_id'] : '',
            "status" => $resdecoded['success'],
            "fcm_response" => $resp,
            "member_id" => $data['member_id'] ?? null,
            "shop_user_id" => $data['shop_user_id'] ?? null
        ]);


        if (Auth::user())
            $fcm_reponse->user_id = Auth::user()->id;

        $fcm_reponse->title = $title;
        $fcm_reponse->message = $message;
        $fcm_reponse->image = $image_url;

        $fcm_reponse->save();

        return true;
    }



}
