<?php


namespace App\managers;


use App\User;

class UserManager
{
    /* @Enum
     */

    const AplhaRoles =  Array(
        'SUPER' => 'SUPER','USERS' => 'USERS'
    );
    const AlphaRoleSuper = "SUPER";
    const AlphaRoleUser = "USERS";

    static  function getUserRoleByName(User $user, $roleName){
        $roles=$user->roles->pluck('roleName')->join(',');
        if(strpos($roles,$roleName)!==false){
            return $user;
        }

        return 0;
    }

}
