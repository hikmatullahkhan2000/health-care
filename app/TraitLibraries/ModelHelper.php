<?php
/**
 * Created by PhpStorm.
 * User: warispopal
 * Date: 4/3/19
 * Time: 3:54 PM
 */

namespace App\TraitLibraries;

use App\Exports\QueryExport;
use App\helpers\DateTimeHelper;
use App\models\Disposition;
use App\Models\UserClickRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

trait ModelHelper
{

    public function existAttribute(string $attribute): bool
    {
        return in_array($attribute, $this->fillable, true);
    }

    protected static function boot()
    {
        parent::boot();
    }


    /**
     * @param array $attributes
     */
    public function loadModel(array $attributes): void
    {
        foreach ($attributes as $attribute => $value) {
            if (in_array($attribute, $this->fillable, true))
                $this->setAttribute($attribute, $value);
        }
    }

    public function exportIndex($request, $query, $columns = null, $file_name = null)
    {
        if ($request->has('export') && $request->get('export') == 1) {
            if ($columns) {
                $query->select($columns);
            }
            if (empty($file_name)) {
                $file_name = $this->getTable();
            }

            return Excel::download(new QueryExport($query), $file_name . '_' . date('d-m-Y') . '.xlsx');

        } else {
            return false;
        }
    }

    /**
     * @param string $value
     * @return array
     */
    protected function getDateRange(string $value): array
    {
        if (!empty($value) and strpos($value, '-') !== false) {
            $date_array = explode(' - ', $value);
            $createdStart = DateTimeHelper::convertFromLocalToUTC($date_array[0] . ' 00:00:00');
            $createdEnd = DateTimeHelper::convertFromLocalToUTC($date_array[1] . ' 23:59:59');
            return array($createdStart, $createdEnd);
        }
    }

    public function adDisposition($comment = "", $reason_id = 0)
    {

        $user_id = auth()->user()->id;
        $model = new Disposition([
            "object" => $this->getTable(),
            "objectId" => $this->id,
            "comment" => $comment,
            'reason_id' => $reason_id,
            'user_id' => $user_id
        ]);

        if ($this->existAttribute("status"))
            $model->status = $this->status;
        if ($this->existAttribute("type"))
            $model->type = $this->type;

        $model->save();
    }

    public function addUserClick($userType='user')
    {
        $user_id = request()->auth->id??null;
        $model = new UserClickRecord([
            "object"    => $this->getTable(),
            "object_id" => $this->id,
            'user_id'   => $user_id,
            'user_type' => $userType
        ]);

//        if ($this->existAttribute("status"))
//            $model->status = $this->status;
//        if ($this->existAttribute("type"))
//            $model->type = $this->type;

        $model->save();
    }

    /**
     * @param string $value
     * @return array
     */
    protected function getDateRangeTime(string $value): array
    {
        if (!empty($value) and strpos($value, '-') !== false) {
            $date_array = explode(' - ', $value);
            $createdStart = DateTimeHelper::convertFromLocalToUTC($date_array[0]);
            $createdEnd = DateTimeHelper::convertFromLocalToUTC($date_array[1]);
            return array($createdStart, $createdEnd);
        }
    }

    protected function getRangeTime(string $key, string $value): array
    {
        if (!empty($value) and strpos($value, '-') !== false) {
            $date_array = explode(' - ', $value);
            $key_array = explode('-', $key);

            $result_array = array_combine($key_array, $date_array);

//
//            $createdStart = DateTimeHelper::convertFromLocalToUTC($date_array[0]);
//            $createdEnd = DateTimeHelper::convertFromLocalToUTC($date_array[1]);

            return $result_array;
        }
        return [];
    }
    public static function str_slug($title, $separator = '-', $language = 'en')
    {
        return Str::slug($title, $separator, $language);
    }


}
