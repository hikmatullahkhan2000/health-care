<?php


namespace App\TraitLibraries;


use App\Models\Models\FcmHistory;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class FcmManager
{

    static $SERVER_KEY = 'AAAA03wmB9c:APA91bEbj_VLwn29uXmObYh5Wk34tBwNHpxknV5L0Swvehe7bltqmhQdDJeDZlTMWVxb_bpYqjzf8GZRQtEjTtydiEC67ggfu3geXjrIvG6Bbg4VqYLBtroBPUS0mhqRGQ5DFvQuCMpP';


    public static function send(User $user, $title, $message, $data = [])
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $to = $user->fcm_token;
        $server_key = SELF::$SERVER_KEY;
        $msg = array(
            'body' => $message,
            'title' => $title,
            'click_action' => "FLUTTER_NOTIFICATION_CLICK",
            'object' => !empty($data['object']) ? $data['object'] : null,
            'objectId' => !empty($data['objectId']) ? $data['objectId'] : null,
            'icon' => '',
            'sound' => 'mySound'

        );

        $fields = array(
            'to' => $to,
            'data' => $msg,
            'notification' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . $server_key,
            'Content-Type: application/json',
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err)
            throw new \Exception($err);

        $resdecoded = json_decode($response, true);
        $resp = (string)$response;

        $fcm_reponse = new FcmHistory([
            "user_token" => $user->fcm_token,
            "title" => $title,
            "message" => $message,
            "multicast_id" => (!empty($resdecoded['multicast_id'])) ? $resdecoded['multicast_id'] : '',
            "status" => $resdecoded['success'],
            "fcm_response" => $resp,
            "user_id" => $user->id,
        ]);
        $fcm_reponse->save();

        return true;
    }


}
