<?php
/**
 * Created by PhpStorm.
 * User: warispopal
 * Date: 1/25/19
 * Time: 3:14 PM
 */

namespace App\TraitLibraries;


trait AlertMessages
{
    protected $message = array(
        "error" => array(
            'none' => 'Something Went Wrong In {modelName}.',
            'saving' => 'An Error Occur While Saving {modelName}.',
            'update' => 'An Error Occur While Updating {modelName}.',
            'notFound' => '{modelName} Data Not found.',
        ),
        "success" => array(
            'none' => 'Successfully done {modelName}.',
            'saving' => '{modelName} Created Successfully .',
            'update' => '{modelName} Updated Successfully .',
            'deleted' => '{modelName} Deleted Successfully .',
        ),

    );


    public function setAlertError($modelName, $type = 'none')
    {
        return str_replace('{modelName}', $modelName, $this->message['error'][$type]);
    }

    public function setAlertSuccess($modelName, $type = 'none')
    {
        return str_replace('{modelName}', $modelName, $this->message['success'][$type]);
    }
}