<?php


namespace App\TraitLibraries;


use App\scopes\ActiveScope;

trait ActiveScopeFilter
{

    protected static function bootActiveScopeFilter()
    {
        static::addGlobalScope(new ActiveScope());
    }

}
