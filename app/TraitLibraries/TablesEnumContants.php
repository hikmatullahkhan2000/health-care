<?php
/**
 * Created by PhpStorm.
 * User: warispopal
 * Date: 2/9/19
 * Time: 4:54 AM
 */

namespace App\TraitLibraries;


trait TablesEnumContants
{
    protected $alphaRoles = Array(
        'SUPER' => 'SUPER',
        'USERS' => 'USERS'
    );
}