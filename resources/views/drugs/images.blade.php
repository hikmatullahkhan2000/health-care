<x-app-layout>
    <x-slot name="header">
        Images
    </x-slot>

    <div class="row">
        <!-- FormValidation -->
        <div class="col-2"></div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-12 mt-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title"
                                            id="basic-layout-form text-muted"> Jobs View</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="px-3">
                                            @foreach($files as $file)
                                                <form style="display: inline" action="{{ route('jobs.images', $file->id)}}"
                                                      id="delete_form_{{$file->id}}" method="post">
                                                    @method('DELETE')
                                                    @csrf
                                                    <div class="card" style="width: 18rem;">

                                                        @if($file->extension == 'pdf')
                                                            <a class="btn btn-sm btn-outline-info" href="{{URL::to('/storage').'/jobs/'.$file->title ?? '#'}}" download=""><i class="tf-icons bx bx-sm bx-cloud-download"></i> Download</a>
                                                        @else
                                                            <img
                                                                src="{{$file->title?URL::to('/storage')."/jobs/{$file->title}" :env('EMPTY_IMAGE_BASE_URL')."images/empty.jpeg"}}"
                                                                data-src="{{$file->title?URL::to('/storage')."/jobs/{$file->title}" :env('EMPTY_IMAGE_BASE_URL')."images/empty.jpeg"}}" width="50%" height="100px">
                                                        @endif

                                                            <p class="text-success">{{$file->type}}</p>
                                                            <a class="badge badge-light delete-confirm-id"
                                                               data-id="{{$file->id}}" title="Delete">
                                                                <i class="fa fa-trash text-danger mr-2"> Delete</i></a>

                                                    </div>
                                                </form>
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
            <!-- /FormValidation -->
        </div>
        <!-- /FormValidation -->
    </div>


</x-app-layout>

<script>
    {{--$(function () {--}}
    {{--    var text_value = <?php echo json_encode($model->long_description ?? ''); ?>;--}}

    {{--    // Replace the <textarea id="editor1"> with a CKEditor--}}
    {{--    // instance, using default configuration.--}}
    {{--    CKEDITOR.replace('editor1')--}}
    {{--    //bootstrap WYSIHTML5 - text editor--}}
    {{--    CKEDITOR.instances['editor1'].setData(text_value)--}}
    {{--    $('#text_box').show();--}}
    {{--})--}}

    $('.delete-confirm-id').on('click', function () {
        var id = $(this).attr('data-id');
        if (id != undefined) {
            swal.fire({
                title       : 'Are you sure?',
                text        : "You won't be able to revert this!",
                type        : 'warning',
                showCancelButton    : true,
                confirmButtonColor  : '#0CC27E',
                cancelButtonColor   : '#FF586B',
                confirmButtonText   : 'Yes!',
                cancelButtonText    : "No, cancel"
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    document.getElementById('delete_form_' + id).submit();
                }
            }).catch(swal.noop);
        }
    });

</script>








