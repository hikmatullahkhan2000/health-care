<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Drugs') }}
        <a href="{{ route('drugs.create') }}" class="btn btn-primary pull-right button-left">
            <i class="menu-icon tf-icons bx bx-plus"></i>Add New
        </a>
    </x-slot>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                                <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Short Title</th>
                                        <th>Slug</th>
{{--                                        <th>Banner</th>--}}
                                        <th>Video</th>
                                        <th>Type</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($drugs as $drug)
                                    <tr>
                                        <td>{{$drug->id}}</td>
                                        <td>{{$drug->title}}</td>
                                        <td>{{$drug->short_title}}</td>
                                        <td>{{$drug->slug}}</td>
{{--                                        <td>--}}
{{--                                            <div class="image-show" data-title="{!! $drug->banner !!}">--}}
{{--                                                <img src="{{$drug->banner?env('IMAGE_BASE_URL')."/{$drug->banner}" :'/images/member.png'}}" class="img-circle" width="80%" height="50px">--}}
{{--                                            </div>--}}
{{--                                        </td>--}}
                                        <td>
                                            <div class="image-show" data-title="{!! $drug->video_link !!}">
                                                @if($drug->video_link)
                                                    <video width="80%" height="auto" controls>
                                                        <source src="{{$drug->video_link?env('IMAGE_BASE_URL')."/{$drug->video_link}" :'/images/member.png'}}" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                @endif

                                            </div>
                                        </td>
                                        <td>{{\App\Models\Drug::types[$drug->type] ?? ''}}</td>
                                        <td>
                                            <a href="{{ route('drugs.edit', $drug->id) }}" ><i class="tf-icons bx bx-edit"></i></a>
                                            <form style="display: inline" action="{{ route('drugs.destroy', $drug->id)}}"
                                                  id="delete_form_{{$drug->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$drug->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-slot name="scripts">
        <script>
            $(function () {
                $("#usersTable").DataTable({
                    order: [[0, 'desc']],
                });
            });
        </script>
    </x-slot>
</x-app-layout>
