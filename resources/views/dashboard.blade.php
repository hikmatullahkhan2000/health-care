<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="row">
        <div class="col-lg-12 mb-4 order-0">
            <div class="card">
                <div class="d-flex align-items-end row">
                    <div class="col-sm-7">
                        <div class="card-body">
                            <h5 class="card-title text-primary">Hello, {{ Str::headline(Auth::user()->name) }}! 👋</h5>
                            <p class="mb-4">You're doing great, Have a nice day.</p>

                            <!-- <a href="javascript:;" class="btn btn-sm btn-outline-primary">View Badges</a> -->
                        </div>
                    </div>
                    <div class="col-sm-5 text-center text-sm-left">
                        <div class="card-body pb-0 px-0 px-md-4">
                            <img src="../assets/img/illustrations/man-with-laptop-light.png" height="140" alt="View Badge User" data-app-dark-img="illustrations/man-with-laptop-dark.png" data-app-light-img="illustrations/man-with-laptop-light.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    <div class="row">--}}
{{--    <div class="col-lg-12 col-md-4 order-1">--}}
{{--    <div class="row">--}}
{{--      <div class="col-lg-3 col-md-12 col-6 mb-4">--}}
{{--        <div class="card">--}}
{{--          <div class="card-body">--}}
{{--            <span>Total Jobs</span>--}}
{{--            <h3 class="card-title text-nowrap mb-1">{{ array_sum($data['shifts']) ?? "N/A" }}</h3>--}}
{{--            <a class="d-block" href="{{ route('jobs.index',['shift' => 'schedule']) }}">view Details &RightArrow;</a>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--      <div class="col-lg-3 col-md-12 col-6 mb-4">--}}
{{--        <div class="card">--}}
{{--          <div class="card-body">--}}
{{--            <span>Scheduled Shifts</span>--}}
{{--            <h3 class="card-title text-nowrap mb-1">{{ $data['shifts'][0] ?? "N/A" }}</h3>--}}
{{--            <a class="d-block" href="{{ route('jobs.index',['shift' => 'schedule']) }}">view Details &RightArrow;</a>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--      <div class="col-lg-3 col-md-12 col-6 mb-4">--}}
{{--        <div class="card">--}}
{{--          <div class="card-body">--}}
{{--            <span>Booked Shifts</span>--}}
{{--            <h3 class="card-title text-nowrap mb-1">{{ $data['shifts'][1] ?? "N/A" }}</h3>--}}
{{--            <a class="d-block" href="{{ route('jobs.index',['shift' => 'booked']) }}">view Details &RightArrow;</a>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--      <div class="col-lg-3 col-md-12 col-6 mb-4">--}}
{{--        <div class="card">--}}
{{--          <div class="card-body">--}}
{{--            <span>Completed Shifts</span>--}}
{{--            <h3 class="card-title text-nowrap mb-1">{{ $data['shifts'][2] ?? "N/A" }}</h3>--}}
{{--            <a class="d-block" href="{{ route('jobs.index',['shift' => 'completed']) }}">view Details &RightArrow;</a>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--      </div>--}}
    </div>
  </div>
    </div>

</x-app-layout>
