<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Diseases') }}
        <a href="{{ route('diseases.create') }}" class="btn btn-primary pull-right button-left">
            <i class="menu-icon tf-icons bx bx-plus"></i>Add New
        </a>
    </x-slot>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Short Title</th>
                                    <th>Slug</th>
{{--                                    <th>Banner</th>--}}
                                    <th>Video</th>
                                    <th>Specialties</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($diseases as $disease)
                                    <tr>
                                        <td>{{$disease->id}}</td>
                                        <td>{{$disease->title}}</td>
                                        <td>{{$disease->short_title}}</td>
                                        <td>{{$disease->slug}}</td>
{{--                                        <td>--}}
{{--                                            <div class="image-show" data-title="{!! $disease->banner !!}">--}}
{{--                                                <img src="{{$disease->banner?env('IMAGE_BASE_URL')."/{$disease->banner}" :'/images/member.png'}}" class="img" width="80%" height="50px">--}}
{{--                                            </div>--}}
{{--                                        </td>--}}
                                        <td>
                                            <div class="image-show" data-title="{!! $disease->video_link !!}">
                                                @if($disease->video_link)
                                                    <video width="80%" height="auto" controls>
                                                        <source src="{{$disease->video_link?env('IMAGE_BASE_URL')."/{$disease->video_link}" :'/images/member.png'}}" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                @endif

                                            </div>
                                        </td>
                                        <td>{{$disease->specialtie->name ?? ''}}</td>
                                        <td>
                                            <a href="{{ route('diseases.edit', $disease->id) }}" ><i class="tf-icons bx bx-edit"></i></a>
                                            <form style="display: inline" action="{{ route('diseases.destroy', $disease->id)}}"
                                                  id="delete_form_{{$disease->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$disease->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-slot name="scripts">
        <script>
            $(function () {
                $("#usersTable").DataTable({
                    order: [[0, 'desc']],
                });
            });
        </script>
    </x-slot>
</x-app-layout>
