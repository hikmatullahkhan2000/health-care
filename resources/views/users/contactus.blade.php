<x-app-layout>
    <x-slot name="header">
        {{ __('ContactUs') }}
{{--        @if(\Illuminate\Support\Facades\Auth::user()->type != \App\Models\User::nurse && \Illuminate\Support\Facades\Auth::user()->type != \App\Models\User::company_user)--}}

{{--            <a href="{{ route('users.create') }}" class="btn btn-primary pull-right button-left">Add New</a>--}}
{{--        @endif--}}
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>City</th>
                                    <th>Specialty</th>
                                    <th>Year Of Experience</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contacts as $contact)
                                    <tr>
                                        <td>{{$contact->id}}</td>
                                        <td>{{$contact->first_name}}</td>
                                        <td>{{$contact->last_name}}</td>
                                        <td>{{$contact->email}}</td>
                                        <td>{{$contact->phone}}</td>
                                        <td>{{$contact->city}}</td>
                                        <td>{{$contact->specialty}}</td>
                                        <td>{{$contact->year_of_ex}}</td>
{{--                                        <td>{{$user->company->name ?? '-'}}</td>--}}
{{--                                        <td class="text-nowrap">--}}
{{--                                            @if (!empty($user->document))--}}
{{--                                                    <a class="btn btn-sm btn-outline-info" href="{{$user->document ?? '#'}}" download=""><i class="tf-icons bx bx-sm bx-cloud-download"></i> Download</a>--}}
{{--                                            @else--}}
{{--                                                <span class="text-muted">N/A</span>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @if ($user->doc_approved)--}}
{{--                                                <i class="tf-icons bx bx-sm bx-check-double text-success"></i>--}}
{{--                                            @elseif (empty($user->document))--}}
{{--                                                <i class="tf-icons bx bx-sm bx-x text-danger"></i>--}}
{{--                                            @else--}}
{{--                                                <a class="btn btn-sm btn-primary" href="{{ route('users.verify.doc', $user->id)}}">Verify</a>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
                                        <td class="text-nowrap" title="{{ $contact->created_at }}">{{ date("d M, Y", strtotime($contact->created_at)) }}</td>


                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Send Message</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
{{--                    <form method="post">--}}
                        @csrf
                        <div class="mb-3">
{{--                            <label for="assigned_to">Assigned To</label>--}}
                            <input type="text" name="message"  id="message" class="form-control">
                        </div>
                        <button type="submit" id="sent" class="btn btn-primary">Submit</button>
{{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>

{{--    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--        <div class="modal-dialog modal-dialog-centered">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="exampleModalLabel1">Send Notification</h5>--}}
{{--                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                <form method="post" action="{{route("sendNotification")}}">--}}
{{--                    @csrf--}}
{{--                    <div class="mb-3">--}}
{{--                        <input type="text" name="message"  id="message" class="form-control">--}}
{{--                        <input type="hidden" name="uid"  id="uid" class="form-control">--}}
{{--                    </div>--}}
{{--                    <button type="submit" id="sent" class="btn btn-primary">Submit</button>--}}
{{--                </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <x-slot name="scripts">
    </x-slot>
</x-app-layout>
