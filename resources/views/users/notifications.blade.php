<x-app-layout>
    <x-slot name="header">
        {{ __('Notifications') }}
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Sender</th>
                                    <th>Title</th>
                                    <th>Body</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($notifications as $notification)
                                    <tr>
                                        <td>{{$notification->id}}</td>
                                        <td>{{$notification->sender->name ?? ''}}</td>
                                        <td>{{$notification->title}}</td>
                                        <td>{{$notification->body}}</td>
                                        <td>
                                            @if($notification->type == \App\Models\Notifications::CHAT)
                                                <a href="{{ route('realTimeChat', ['phone'=>$notification->sender->phone]) }}"><i class="tf-icons bx bx-mail-send"></i></a>
                                            @endif
                                        </td>
                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
