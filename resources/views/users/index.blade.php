<x-app-layout>
    <x-slot name="header">
        {{ __('Manage User') }}
{{--            <a href="{{ route('users.create') }}" class="btn btn-primary pull-right button-left">Add New</a>--}}
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Gender</th>
                                    <th>Job Category</th>
                                    <th>Type</th>
{{--                                    <th>Company</th>--}}
{{--                                    <th>Document</th>--}}
{{--                                    <th>Verify</th>--}}
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{\App\Models\User::genders[$user->gender] ?? ''}}</td>
                                        <td>{{$user->jobCategory->title ?? ""}}</td>
                                        <td>{{@$type[$user->type]}}
{{--                                            <span class="text-muted">{{$user->nurse_type ? '(' . strtolower(\App\Models\User::nurseType[$user->nurse_type]) . ')'  : '' }}</span>--}}
                                        </td>
{{--                                        <td>{{$user->company->name ?? '-'}}</td>--}}
{{--                                        <td class="text-nowrap">--}}
{{--                                            @if (!empty($user->document))--}}
{{--                                                    <a class="btn btn-sm btn-outline-info" href="{{$user->document ?? '#'}}" download=""><i class="tf-icons bx bx-sm bx-cloud-download"></i> Download</a>--}}
{{--                                            @else--}}
{{--                                                <span class="text-muted">N/A</span>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @if ($user->doc_approved)--}}
{{--                                                <i class="tf-icons bx bx-sm bx-check-double text-success"></i>--}}
{{--                                            @elseif (empty($user->document))--}}
{{--                                                <i class="tf-icons bx bx-sm bx-x text-danger"></i>--}}
{{--                                            @else--}}
{{--                                                <a class="btn btn-sm btn-primary" href="{{ route('users.verify.doc', $user->id)}}">Verify</a>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
                                        <td class="text-nowrap" title="{{ $user->created_at }}">{{ date("d M, Y", strtotime($user->created_at)) }}</td>

                                        <td>
{{--                                            <a href="{{ route('users.edit', $user->id)}}"><i class="tf-icons bx bx-edit"></i></a>--}}
{{--                                            <form action="{{ route('users.destroy', $user->id)}}" method="post" id="user_delete">--}}
{{--                                                @csrf--}}
{{--                                                @method('DELETE')--}}
{{--                                                <i class="tf-icons bx bx-trash text-danger listing_delete_icon" form_id="user_delete"></i>--}}

{{--                                            </form>--}}


{{--                                            @if (Auth::user()->type == \App\Models\User::company_user)--}}
{{--                                                <a type="button" class="mb-2 text-info" data-bs-toggle="modal"--}}
{{--                                                   data-bs-target="#exampleModal" data-id="{{ $user->id }}" fid="{{\Illuminate\Support\Facades\Auth::user()->uid}}" toid="{{$user->uid}}" title="send message">--}}
{{--                                                    <i class="tf-icons bx bx-chat"></i>--}}
{{--                                                </a>--}}
{{--                                            @endif--}}
{{--                                            @if (Auth::user()->type == \App\Models\User::admin_user)--}}
{{--                                                <a type="button" class="mb-2 text-info send_notification" data-bs-toggle="modal"--}}
{{--                                                   data-bs-target="#exampleModal1" uid="{{ $user->uid }}" title="send notification">--}}
{{--                                                    <i class="tf-icons bx bx-notification"></i>--}}
{{--                                                </a>--}}
{{--                                            @endif--}}
{{--                                            <a href="{{ route('realTimeChat', ['phone'=>$user->phone]) }}"><i class="tf-icons bx bx-chat"></i></a>--}}
                                            <a href="{{ route('users.edit', $user->id) }}"><i class="tf-icons bx bx-edit"></i></a>
{{--                                            <form action="{{ route('users.destroy', $user->id)}}" method="post" id="company_delete"--}}
{{--                                                  style="display: inline-block">--}}
{{--                                                @csrf--}}
{{--                                                @method('DELETE')--}}
{{--                                                <i class="tf-icons bx bx-trash text-danger listing_delete_icon" form_id="company_delete{{$user->id}}"></i>--}}
{{--                                            </form>--}}

                                            <form style="display: inline" action="{{ route('users.destroy', $user->id)}}"
                                                  id="delete_form_{{$user->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$user->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>

                                        </td>

                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Send Message</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
{{--                    <form method="post">--}}
                        @csrf
                        <div class="mb-3">
{{--                            <label for="assigned_to">Assigned To</label>--}}
                            <input type="text" name="message"  id="message" class="form-control">
                        </div>
                        <button type="submit" id="sent" class="btn btn-primary">Submit</button>
{{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>

{{--    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--        <div class="modal-dialog modal-dialog-centered">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="exampleModalLabel1">Send Notification</h5>--}}
{{--                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                <form method="post" action="{{route("sendNotification")}}">--}}
{{--                    @csrf--}}
{{--                    <div class="mb-3">--}}
{{--                        <input type="text" name="message"  id="message" class="form-control">--}}
{{--                        <input type="hidden" name="uid"  id="uid" class="form-control">--}}
{{--                    </div>--}}
{{--                    <button type="submit" id="sent" class="btn btn-primary">Submit</button>--}}
{{--                </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <x-slot name="scripts">
    </x-slot>
</x-app-layout>
