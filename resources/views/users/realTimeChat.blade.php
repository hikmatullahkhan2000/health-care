<x-app-layout>
    <x-slot name="header">
        {{ __('Real Time Chat') }}
    </x-slot>
    <style>
            .container{max-width:1170px; margin:auto;}
            img{ max-width:100%;}
            .inbox_people {
                background: #f8f8f8 none repeat scroll 0 0;
                float: left;
                overflow: hidden;
                width: 40%; border-right:1px solid #c4c4c4;
            }
            .inbox_msg {
                border: 1px solid #c4c4c4;
                clear: both;
                overflow: hidden;
            }
            .top_spac{ margin: 20px 0 0;}


            .recent_heading {float: left; width:40%;}
            .srch_bar {
                display: inline-block;
                text-align: right;
                width: 60%;
            }
            .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

            .recent_heading h4 {
                color: #05728f;
                font-size: 21px;
                margin: auto;
            }
            .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
            .srch_bar .input-group-addon button {
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                border: medium none;
                padding: 0;
                color: #707070;
                font-size: 18px;
            }
            .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

            .chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
            .chat_ib h5 span{ font-size:13px; float:right;}
            .chat_ib p{ font-size:14px; color:#989898; margin:auto}
            .chat_img {
                float: left;
                width: 11%;
            }
            .chat_ib {
                float: left;
                padding: 0 0 0 15px;
                width: 88%;
            }

            .chat_people{ overflow:hidden; clear:both;}
            .chat_list {
                border-bottom: 1px solid #c4c4c4;
                margin: 0;
                padding: 18px 16px 10px;
            }
            .inbox_chat { height: 550px; overflow-y: scroll;}

            .active_chat{ background:#ebebeb;}

            .incoming_msg_img {
                display: inline-block;
                width: 6%;
            }
            .received_msg {
                display: inline-block;
                padding: 0 0 0 10px;
                vertical-align: top;
                width: 92%;
            }
            .received_withd_msg p {
                background: #ebebeb none repeat scroll 0 0;
                border-radius: 3px;
                color: #646464;
                font-size: 14px;
                margin: 0;
                padding: 5px 10px 5px 12px;
                width: 100%;
            }
            .time_date {
                color: #747474;
                display: block;
                font-size: 12px;
                margin: 8px 0 0;
            }
            .received_withd_msg { width: 57%;}
            .mesgs {
                float: left;
                padding: 30px 15px 0 25px;
                width: 60%;
            }

            .sent_msg p {
                background: #05728f none repeat scroll 0 0;
                border-radius: 3px;
                font-size: 14px;
                margin: 0; color:#fff;
                padding: 5px 10px 5px 12px;
                width:100%;
            }
            .outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
            .sent_msg {
                float: right;
                width: 46%;
            }
            .input_msg_write input {
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                border: medium none;
                color: #4c4c4c;
                font-size: 15px;
                min-height: 48px;
                width: 100%;
            }

            .type_msg {border-top: 1px solid #c4c4c4;position: relative;}
            .msg_send_btn {
                background: #05728f none repeat scroll 0 0;
                border: medium none;
                border-radius: 50%;
                color: #fff;
                cursor: pointer;
                font-size: 17px;
                height: 33px;
                position: absolute;
                right: 0;
                top: 11px;
                width: 33px;
            }
            .messaging { padding: 0 0 50px 0;}
            .msg_history {
                height: 516px;
                overflow-y: auto;
            }
        </style>
    <div class="container">
        <h3 class=" text-center">Messaging</h3>
        <div class="messaging">
            <div class="inbox_msg">
                <div class="inbox_people">
                    <div class="headind_srch">
                        <div class="recent_heading">
                            <h4>Recent</h4>
                        </div>
{{--                        <div class="srch_bar">--}}
{{--                            <div class="stylish-input-group">--}}
{{--                                <input type="text" class="search-bar"  placeholder="Search" >--}}
{{--                                <span class="input-group-addon">--}}
{{--                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>--}}
{{--                </span> </div>--}}
{{--                        </div>--}}
                    </div>
                    <div class="inbox_chat">
                            <?php
                                $toPhone = $chatToUser->phone ?? '';
                            ?>

                            @foreach($chatUsers as $user)
                            <div class="chat_list {{($user['phone'] == $toPhone)?'active_chat':''}} ">
                                <a href="{{ route('realTimeChat', ['phone'=>$user['phone'],'chat_id'=>$user['chatId']]) }}">
                                    <div class="chat_people">
                                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                        <div class="chat_ib">
                                            <h5>{{$user['name']}}
{{--                                                <span class="chat_date">{{$user['createdAt']}}</span>--}}
                                            </h5>
                                            {{--                                    <p>Test, which is a new approach to have all solutions--}}
                                            {{--                                        astrology under one roof.</p>--}}
                                        </div>
                                    </div>
                                </a>
                             </div>
                            @endforeach

                    </div>
                </div>
                <div class="mesgs">
                    <div class="msg_history" id="msg_history">
{{--                        @foreach($fromUserChat as $chat)--}}
{{--                            @if($chat['idFrom'] == "5cAXdoz77DRlsxt7wWJsjNZCzXx1-+9203313543211")--}}
{{--                                <div class="outgoing_msg">--}}
{{--                                    <div class="sent_msg">--}}
{{--                                        <p>{{$chat['content']}}</p>--}}
{{--                                        <span class="time_date">{{date("Y-m-d H:i:s", $chat['timestamp']/1000)}}</span> </div>--}}
{{--                                </div>--}}
{{--                                @else--}}
{{--                                <div class="incoming_msg">--}}
{{--                                    <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>--}}
{{--                                    <div class="received_msg">--}}
{{--                                        <div class="received_withd_msg">--}}
{{--                                            <p>{{$chat['content']}}</p>--}}
{{--                                            <span class="time_date">{{date("Y-m-d H:i:s", $chat['timestamp']/1000)}}</span> </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                        @endforeach--}}
                    </div>
                    <div class="type_msg">
                        <div class="input_msg_write">
{{--                            <input type="text" class="write_msg" placeholder="Type a message" />--}}
{{--                            <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>--}}
                            <input type="hidden" name="data" chatUserPhone="{{$chatToUser->phone ?? ''}}" userType="{{$loginUser->type}}" id="data" fid="{{$loginUser->uid ?? ''}}" toid="{{$chatToUser->uid ?? ''}}">
                            <form onsubmit="return sendMessage();">
{{--                                <input id="message" placeholder="Enter message testing" autocomplete="off">--}}
                                <input type="text" id="message" class="write_msg" placeholder="Type a message" />

                                <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>


{{--            <p class="text-center top_spac"> Design by <a target="_blank" href="https://www.linkedin.com/in/sunil-rajput-nattho-singh/">Sunil Rajput</a></p>--}}

        </div></div>

{{--    <div class="row">--}}
{{--        <!-- FormValidation -->--}}
{{--        <div class="col-2"></div>--}}
{{--        <div class="row">--}}
{{--            <ul id="messages"></ul>--}}

{{--            <!-- create a form to send message -->--}}
{{--            <form onsubmit="return sendMessage();">--}}
{{--                <input id="message" placeholder="Enter message testing" autocomplete="off">--}}

{{--                <input type="submit">--}}
{{--            </form>--}}
{{--        </div>--}}
{{--        <!-- /FormValidation -->--}}
{{--    </div>--}}
    <x-slot name="scripts">

        <!-- The core Firebase JS SDK is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-app.js"></script>

        <!-- include firebase database -->
        <script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-database.js"></script>

        <script type="text/javascript">

            const  myName  = "{{$loginUser->name}}"
            const uniqueMsgId = ""

            var fid  =  $('#data').attr('fid');
            var toid  =  $('#data').attr('toid');
            var currentUserType  =  $('#data').attr('userType');

            var chatToPhone = $('#data').attr('chatUserPhone');


            // Your web app's Firebase configuration
            var firebaseConfig = {
                // apiKey:'AIzaSyAGxaRSfUXXscCLS2O-rKxOgfsfREZ6rjY',
                // authDomain:'reliablehands-cc670.firebaseapp.com',
                // databaseURL: 'https://reliablehandscompany-default-rtdb.firebaseio.com/',
                // projectId: 'reliablehands-cc670',
                // storageBucket: 'reliablehands-cc670.appspot.com',
                // messagingSenderId:'66911498519',
                // appId: '1:66911498519:web:f16ffd0ed76cc0cb0d4752'

                apiKey: "",
                authDomain: "",
                databaseURL: "",
                projectId: "",
                storageBucket: "",
                messagingSenderId: "",
                appId: "",
                measurementId: ""


            };
            // Initialize Firebase
            firebase.initializeApp(firebaseConfig);

            // var myName = prompt("Enter your name");

            function sendMessage() {
                var currenDateTime = new Date();
                // var parseDate = Date.parse(currenDateTime)/1000;
                var parseDate = Date.parse(currenDateTime);
                //get message
                var message = document.getElementById("message").value;
                $('#message').val('');
                // save in database

                var gId = '';
                if(currentUserType == 2){
                    gId = toid+"-"+fid;
                }else if(currentUserType == 3){
                    gId = fid+"-"+toid;
                }
                firebase.database().ref('messages')
                    .child(gId)
                    .child(gId)
                    .child(parseDate)
                    .set({
                    'idFrom':fid,
                    'idTo': toid,
                    'timestamp': parseDate.toString(),
                    'content': message,
                });

                $("#msg_history").scrollTop($("#msg_history")[0].scrollHeight);


                //prevent form from submitting
                return false;

            }

            function timeConverter(UNIX_timestamp){
                let time_stamp = parseInt(UNIX_timestamp);
                let datae = new Date(time_stamp);
                // console.log(date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds());
                // var a = new Date(UNIX_timestamp * 1000);
                // var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                // var year = a.getFullYear();
                // var month = months[a.getMonth()];
                // var date = a.getDate();
                // var hour = a.getHours();
                // var min = a.getMinutes();
                // var sec = a.getSeconds();
                // var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;

                var time = datae.getFullYear() + '-' + (datae.getMonth() + 1) + '-' + datae.getDate() + ' ' + datae.getHours() + ':' + datae.getMinutes() + ':' + datae.getSeconds();
                return time;
            }


            if(chatToPhone != ""){
                // listen for incoming messages

                var gId = '';
                if(currentUserType == 2){
                    gId = toid+"-"+fid;
                }else if(currentUserType == 3){
                    gId = fid+"-"+toid;
                }

                firebase.database().ref('messages')
                    .child(gId)
                    .child(gId)
                    .on("child_added", function (snapshot) {
                        let formattedDate = timeConverter(snapshot.val().timestamp);
                        var htmll = "";
                        if(snapshot.val().idFrom == fid){
                            htmll += "<div class='outgoing_msg'>"+
                                "<div class='sent_msg'>"+
                                "<p>"+''+snapshot.val().content+''+"</p>"+
                                "<span class='time_date'>"+'"'+formattedDate+'"'+"</span>"+

                                "</div>"+
                                "</div>";
                        }else{
                            htmll +=  "<div class='incoming_msg'>"+
                                "<div class='incoming_msg_img'>"+"<img src='https://ptetutorials.com/images/user-profile.png' alt='sunil'>"+"</div>"+
                                "<div class='received_msg'>"+
                                "<div class='received_withd_msg'>"+
                                "<p>"+snapshot.val().content+"</p>"+
                                "<span class='time_date'>"+formattedDate+"</span>"+"</div>"+
                                "</div>"+
                                "</div>";
                        }



                        // var html = "";
                        // // give each message a unique ID
                        // html += "<li id='message-" + snapshot.key + "'>";
                        // // show delete button if message is sent by me
                        // if (snapshot.val().sender == myName) {
                        //     html += "<button data-id='" + snapshot.key + "' onclick='deleteMessage(this);'>";
                        //     html += "Delete";
                        //     html += "</button>";
                        // }
                        // html += snapshot.val().content;
                        // html += "</li>";

                        document.getElementById("msg_history").innerHTML += htmll;
                    });


            }

            function deleteMessage(self) {
                // get message ID
                var messageId = self.getAttribute("data-id");

                // delete message
                firebase.database().ref(uniqueMsgId).child(messageId).remove();
            }

            // attach listener for delete message
            firebase.database().ref(uniqueMsgId).on("child_removed", function (snapshot) {
                // remove message node
                document.getElementById("message-" + snapshot.key).innerHTML = "This message has been removed";
            });

        </script>

    </x-slot>

</x-app-layout>


