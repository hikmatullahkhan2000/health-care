<x-app-layout>
    <x-slot name="header">
        {{  $model->exists ? __('Edit User') : __('Create User')  }}
    </x-slot>

    <div class="row">
        <!-- FormValidation -->
        <div class="col-2"></div>
        <div class="row">
            <!-- FormValidation -->
            <div class="col-2"></div>
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                        <form method="post" class="form" action="{{ ($model->exists)? route('users.update', [$model->id]): route('users.store') }}">
                            @if ($model->exists)
                                @method('PUT')
                            @endif
                            @csrf

                            <div class="col-12">
                                <h6 class="fw-semibold">
                                    {{  $model->exists ? __('Edit User') : __('Create User')  }}
                                </h6>
                                <hr class="mt-0">
                            </div>
                            <div class="fv-plugins-icon-container">
                                <label class="form-label" for="first_name">First Name</label>
                                <input type="text" id="first_name" value="{!! old('first_name')??$model->first_name !!}" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="Type User First Name" name="first_name">
                                @if ($errors->has('first_name'))
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('first_name') }}</div>
                                </div>
                                @endif
                            </div>
                            <div class="fv-plugins-icon-container">
                                <label class="form-label" for="last_name">Last Name</label>
                                <input type="text" id="last_name" value="{!! old('last_name')??$model->last_name !!}" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="Type User last_name" name="last_name">
                                @if ($errors->has('last_name'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('last_name') }}</div>
                                    </div>
                                @endif
                            </div>

                            <div class="fv-plugins-icon-container">
                                <label for="gender">Gender</label>
                                <select id="gender" name="gender" class="form-control">
                                    <option value="">Select</option>
                                    @foreach(\App\Models\User::genders as $key => $type)
                                        <option value="{{$key}}" {{ (old('gender') == $key) || ($type == $key) || (isset($model->gender) && $model->gender == $key) ? 'selected=""':'' }}>
                                            {{ strtolower(str_replace('_',' ',$type)) }}
                                        </option>
                                    @endForeach
                                </select>

                                @if ($errors->has('gender'))
                                    <div class="help-block">
                                <span class="" role="alert">
                                    <strong class="text-danger">{{ $errors->first('gender') }}</strong>
                                </span>
                                    </div>
                                @endif
                            </div>
                            <div class="fv-plugins-icon-container">
                                <label for="type">Type</label>
                                <select id="type" name="type" class="form-control">
                                    <option value="">Select</option>
                                    @foreach(\App\Models\User::type as $key => $type)
                                    <option value="{{$key}}" {{ (old('type') == $key) || ($type == $key) || (isset($model->type) && $model->type == $key) ? 'selected=""':'' }}>
                                        {{ strtolower(str_replace('_',' ',$type)) }}
                                    </option>
                                    @endForeach
                                </select>

                                @if ($errors->has('type'))
                                <div class="help-block">
                                    <span class="" role="alert">
                                        <strong class="text-danger">{{ $errors->first('type') }}</strong>
                                    </span>
                                </div>
                                @endif
                            </div>
{{--                            <div class="fv-plugins-icon-container" id="companyDiv" {{ $errors->has('company_id') ? 'style="display:block;' : 'style=display:none;' }}>--}}
{{--                                <label for="type">Company</label>--}}
{{--                                <select id="company" disabled name="company_id" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}">--}}
{{--                                    <option value="">Select</option>--}}
{{--                                    @foreach($companies as $company)--}}
{{--                                    <option value="{{$company->id}}" {{ (old('company_id') == $company->id) || (isset($model->company_id) && $model->company_id == $company->id) ? 'selected=""':'' }}>--}}
{{--                                        {{ strtolower($company->name) }}--}}
{{--                                    </option>--}}
{{--                                    @endForeach--}}
{{--                                </select>--}}
{{--                                @if ($errors->has('company_id'))--}}
{{--                                <div class="help-block">--}}
{{--                                    <small class="text-danger">{{ $errors->first('company_id') }}</small>--}}
{{--                                </div>--}}
{{--                                @endif--}}
{{--                            </div>--}}
                            <div class="fv-plugins-icon-container">
                                <label class="form-label" for="phone">Phone</label>
                                <input type="text" id="phone" value="{!! old('phone')??$model->phone !!}" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="Type User phone" name="phone">
                                @if ($errors->has('phone'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('phone') }}</div>
                                    </div>
                                @endif
                            </div>
                            <div class="fv-plugins-icon-container">
                                <label class="form-label" for="form_email">Email</label>
                                <input type="email" id="form_email" value="{!! old('email')??$model->email !!}" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Type User Email" name="email">
                                @if ($errors->has('email'))
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('email') }}</div>
                                </div>
                                @endif
                            </div>
                            <div class="fv-plugins-icon-container">
                                <label class="form-label" for="form_password">Password</label>
                                <input type="password" id="form_password" value="{!! old('password')??$model->password !!}" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Type User Password" name="password">
                                @if ($errors->has('password'))
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('password') }}</div>
                                </div>
                                @endif
                            </div>
                            <div class="fv-plugins-icon-container">
                                <label class="form-label" for="form_password_confirmation">Password Confirmation</label>
                                <input type="password" id="form_password_confirmation" value="{!! old('password_confirmation')??$model->password_confirmation !!}" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="Type User Password Confirmation" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('password_confirmation') }}</div>
                                </div>
                                @endif
                            </div>
                            <div class="col-12">
                                <button type="submit" name="submitButton" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
            <!-- /FormValidation -->
        </div>
        <!-- /FormValidation -->
    </div>
    <x-slot name="scripts">
        <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>

        <script>
            $(document).ready(function() {
                if (document.querySelector("#type").value == 2) {
                    document.querySelector("#companyDiv").style.display = "block";
                    document.querySelector("#company").setAttribute("name", "company_id");
                }
            });

            document.querySelector("#type").addEventListener("change", (e) => {
                if (e.target.value == 2) {
                    document.querySelector("#companyDiv").style.display = "block";
                    document.querySelector("#company").setAttribute("name", "company_id");
                } else {
                    document.querySelector("#companyDiv").style.display = "none";
                    document.querySelector("#company").removeAttribute("name");
                }
            });
            // console.log(
            //     document.querySelector("#type").value
            // );
        </script>
    </x-slot>

</x-app-layout>

<script>
{{--$(document).ready(function() {--}}
{{-- $("form").submit(function (event) {--}}
{{-- var formData = {--}}
{{-- name: $(this).find('input[name=name]').val(),--}}
{{-- email:$(this).find('input[name=email]').val(),--}}
{{-- password : $(this).find('input[name=password]').val(),--}}
{{-- password_confirmation: $(this).find('input[name=password_confirmation]').val(),--}}
{{-- _token: "{{ csrf_token() }}"--}}
{{-- };--}}

{{-- var url = '{{env("APP_URL")}}/users-store';--}}
{{-- var type = 'POST'--}}
{{-- if({{$model->exists}}){--}}
{{-- url = '{{env("APP_URL")}}/users-update/'+{{$model->id}};--}}
{{-- type = 'PUT'--}}
{{-- }--}}
{{-- console.log(url,type)--}}

{{-- $.ajax({--}}
{{-- type: type,--}}
{{-- url: url,--}}
{{-- data: formData,--}}
{{-- dataType: "json",--}}
{{-- encode: true,--}}
{{-- }).done(function (data) {--}}
{{-- // $("#successBox").html(--}}
{{-- //     '<div class="alert alert-success">' + data.message + "</div>"--}}
{{-- // );--}}
{{-- // $('form').trigger("reset");--}}
{{-- location.reload();--}}

{{-- }).error(function (result){--}}
{{-- if (!result.responseJSON.success) {--}}
{{-- if (result.responseJSON.data.name != undefined) {--}}
{{-- $("#nameError").text(result.responseJSON.data.name[0])--}}
{{-- }--}}
{{-- if (result.responseJSON.data.email != undefined) {--}}
{{-- $("#emailError").text(result.responseJSON.data.email[0])--}}
{{-- }--}}
{{-- if (result.responseJSON.data.password != undefined) {--}}
{{-- $("#passwordError").text(result.responseJSON.data.password[0])--}}
{{-- }--}}

{{-- }--}}
{{-- });--}}

{{-- event.preventDefault();--}}
{{-- });--}}

{{--});--}}
</script>
