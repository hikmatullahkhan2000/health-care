<?php
/**
 * Created by PhpStorm.
 * User: warispopal
 * Date: 1/12/19
 * Time: 10:04 PM
 */

// user view

?>
<x-app-layout>
    <x-slot name="header">
        {!! $model->name !!}
    </x-slot>
    <div class="row">
        <div class="col-2"></div>
        <!-- Users detail page-->
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">User Details</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$model->id}}</td>
                                    <td>{{$model->name}}</td>
                                    <td>{{$model->email}}</td>
                                    <td>{{$model->created_at}}</td>
                                    <td>{{$model->updated_at}}</td>
                                    <td>
                                        <a href="{{ route('users.edit', $model->id) }}"
                                           class="btn btn-primary">Edit</a>
                                        <form action="{{ route('users.destroy', $model->id)}}" method="post"
                                              style="display: inline-block">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('users.index') }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</x-app-layout>

