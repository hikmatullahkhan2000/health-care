<x-app-layout>
    <x-slot name="header">
        {{ __('Manage User') }}
            <a href="{{ route('users.create') }}" class="btn btn-primary pull-right button-left">Add New</a>
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Full Name</th>
                                    <th>Phone</th>
                                    <th>Concerns</th>
                                    <th>Doctor</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($appointments as $appointment)
                                    <tr>
                                        <td>{{ $appointment->id }}</td>
                                            <td>{{ $appointment->full_name }}</td>
                                            <td>{{ $appointment->phone }}</td>
                                            <td>{{ $appointment->concerns }}</td>
                                            <td>
                                                {{ optional($appointment->user)->first_name }} {{ optional($appointment->user)->last_name }}
                                            </td>
                                            <td>{{ $appointment->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Send Message</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
{{--                    <form method="post">--}}
                        @csrf
                        <div class="mb-3">
{{--                            <label for="assigned_to">Assigned To</label>--}}
                            <input type="text" name="message"  id="message" class="form-control">
                        </div>
                        <button type="submit" id="sent" class="btn btn-primary">Submit</button>
{{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>

{{--    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--        <div class="modal-dialog modal-dialog-centered">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="exampleModalLabel1">Send Notification</h5>--}}
{{--                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                <form method="post" action="{{route("sendNotification")}}">--}}
{{--                    @csrf--}}
{{--                    <div class="mb-3">--}}
{{--                        <input type="text" name="message"  id="message" class="form-control">--}}
{{--                        <input type="hidden" name="uid"  id="uid" class="form-control">--}}
{{--                    </div>--}}
{{--                    <button type="submit" id="sent" class="btn btn-primary">Submit</button>--}}
{{--                </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <x-slot name="scripts">
    </x-slot>
</x-app-layout>
