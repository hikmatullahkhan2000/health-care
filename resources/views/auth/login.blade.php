<x-guest-layout>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <div class="row">
            <div class="col-3"></div>
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Logo -->
                        <div class="app-brand justify-content-center mb-5">
                                <a>
                                    <img class="img-fluid d-block mx-auto" src="{{asset("front_assets/img/site-identify/logo.png")}}" alt="" width="100%">
                                </a>
                        </div>
                        <!-- /Logo -->
                        <!-- <h4 class="mb-2">Welcome to Task Management System! 👋</h4> -->
                        <!-- <p class="mb-4">Please sign-in to your account and start the adventure</p> -->

                        <form id="formAuthentication" method="POST" class="mb-3" action="{{ route('login') }}">
                            @csrf

                            <div class="mb-3">
                                <x-input-label for="email" :value="__('Email')" />

                                <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" placeholder="Email" :value="old('email')" required autofocus />

                                <x-input-error :messages="$errors->get('email')" class="mt-2" />
                            </div>
                            <div class="mb-3">
                                <x-input-label for="password" :value="__('Password')" />

                                <x-text-input id="password" class="block mt-1 w-full" type="password" placeholder="Password" name="password" required autocomplete="current-password" />

                                <x-input-error :messages="$errors->get('password')" class="mt-2" />
                            </div>

                            <div class="mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="remember-me" name="remember_me" />
                                    <label class="form-check-label" for="remember-me">{{ __('Remember me') }}</label>
                                </div>
                            </div>
                            <div class="mb-3">
                                <button class="btn btn-primary d-grid w-100" type="submit">Sign in</button>
                            </div>
                        </form>

                        <p class="text-center">
                            @if(Route::has('password.request'))
                            <span> {{ __('Forgot your password?') }}</span>
                            <a href="{{ route('password.request') }}">
                            </a>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-3"></div>
        </div>
</x-guest-layout>
