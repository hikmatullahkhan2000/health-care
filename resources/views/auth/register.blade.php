@include('front.layouts.header')
<?php
$states = [
    'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];
?>
<section class="banner diseases-banner about-us-banner">
    <div class="container">
        <div class="text-cont">
            <h1>Register</h1>
        </div>
    </div>
</section>

<section class="contact-us-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="img-cont">
                    <img src="{{asset("front_assets/img/contact-us/1.png")}}" alt="contact us" class="img-fluid">
                </div>
            </div>
            <div class="col-md-7">
                <div class="text-cont">
                    <h2>Join <span>Now</span></h2>
                </div>
                <form method="POST" action="{{ route('register') }}">
                @csrf

                <!-- Name -->
                    <div>
                        <x-input-label for="first_name" :value="__('First Name')" />

                        <x-text-input id="first_name" class="block mt-1 w-full" type="text" name="first_name" :value="old('first_name')" required autofocus />

                        <x-input-error :messages="$errors->get('first_name')" class="mt-2" />
                    </div>
                    <div>
                        <x-input-label for="last_name" :value="__('Last Name')" />

                        <x-text-input id="last_name" class="block mt-1 w-full" type="text" name="last_name" :value="old('last_name')" required autofocus />

                        <x-input-error :messages="$errors->get('last_name')" class="mt-2" />
                    </div>

                    <div>
                        <x-input-label for="phone" :value="__('Phone')" />

                        <x-text-input id="phone" class="block mt-1 w-full" type="text" name="phone" :value="old('phone')" required autofocus />

                        <x-input-error :messages="$errors->get('phone')" class="mt-2" />
                    </div>


                    <!-- Email Address -->
                    <div class="mt-4">
                        <x-input-label for="email" :value="__('Email')" />

                        <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />

                        <x-input-error :messages="$errors->get('email')" class="mt-2" />
                    </div>

                    <!-- Password -->
                    <div class="mt-4">
                        <x-input-label for="password" :value="__('Password')" />

                        <x-text-input id="password" class="block mt-1 w-full"
                                      type="password"
                                      name="password"
                                      required autocomplete="new-password" />

                        <x-input-error :messages="$errors->get('password')" class="mt-2" />
                    </div>

                    <!-- Confirm Password -->
                    <div class="mt-4">
                        <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

                        <x-text-input id="password_confirmation" class="block mt-1 w-full"
                                      type="password"
                                      name="password_confirmation" required />

                        <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
                    </div>

                    <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="specialty">Specialties</label>
                                <select id="specialty" name="specialty"
                                        class="block mt-1 w-full">
                                    <option value="">Select Specialties</option>

                                    @foreach(\App\Models\Specialty::all() as $key =>  $data)
                                        <option value="{{$data->id}}" >
                                            {{$data->name}}</option>
                                    @endForeach
                                </select>

                                @if ($errors->has('specialty'))
                                    <div class="help-block">
                                        <span class="" role="alert">
                                                    <strong class="text-danger">{{ $errors->first('specialty') }}</strong>
                                        </span></div>
                                @endif

                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="service_id">Services</label>
                                <select id="service_id" name="service_id"
                                        class="block mt-1 w-full">
                                    <option value="">Select Services</option>

                                    @foreach(\App\Models\Services::all() as $key =>  $data)
                                        <option value="{{$data->id}}" >
                                            {{$data->name}}</option>
                                    @endForeach
                                </select>

                                @if ($errors->has('service_id'))
                                    <div class="help-block">
                                        <span class="" role="alert">
                                                    <strong class="text-danger">{{ $errors->first('service_id') }}</strong>
                                        </span></div>
                                @endif

                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="state">States</label>
                                <select id="state" name="state" class="block mt-1 w-full">
                                    <option value="">Select State</option>
                                    @foreach($states as $state)
                                        <option value="{{ $state }}">{{ $state }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('state'))
                                    <div class="help-block">
                <span class="" role="alert">
                    <strong class="text-danger">{{ $errors->first('state') }}</strong>
                </span>
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>



                    <div class="flex items-center justify-end mt-4">
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                            {{ __('Already registered?') }}
                        </a>

                        <x-primary-button class="ml-4">
                            {{ __('Register') }}
                        </x-primary-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@include('front.layouts.footer')
