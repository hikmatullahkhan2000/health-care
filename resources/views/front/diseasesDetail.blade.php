@include('front.layouts.header')

<section class="banner diseases-banner">
    <div class="container">
        <div class="text-cont">
            <h1>{{$diseases->title}}</h1>
            <p>
                <?php
                echo $diseases->short_description;
                ?>
            </p>
        </div>
    </div>
</section>


<section class="doctors-cont">
    <div class="container">
       <?php
        echo $diseases->long_description;
       ?>
       <div class="image-show" data-title="{!! $diseases->video_link !!}">
           @if($diseases->video_link)
               <video width="80%" height="auto" controls>
                   <source src="{{$diseases->video_link?env('IMAGE_BASE_URL')."/{$diseases->video_link}" :'/images/member.png'}}" type="video/mp4">
                   Your browser does not support the video tag.
               </video>
           @endif

       </div>
    </div>
</section>


@include('front.layouts.footer')
