@include('front.layouts.header')

<section class="banner diseases-banner">
    <div class="container">
        <div class="text-cont">
            <h1>Drug</h1>
            <p>No two patients are alike. The topics covered on 5MinuteConsult will support your clinical decisions and improve patient care. Browse more than 2,000 diseases and conditions you encounter in your practice. Sample the site by taking a look at the free open topics!</p>
        </div>
    </div>
</section>


<section class="diseases-cont">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="left-text">
                    <div class="top-cont top-drug">
                        <div class="radio-cont">
                            <form action="#">
                               <span>
                                   <input type="radio" id="test1" name="radio-group" {{ !$type ? 'checked' : '' }}>
                                  <label for="test1">
                                    <a href="{{ route("drug", array_merge(request()->query(), ['type' => 0])) }}" class="text-gray" onclick="selectRadio('test1')">By Drug Name</a>
                                  </label>
                                </span>
                                <span>
                                  <input type="radio" id="test2" name="radio-group" {{ $type ? 'checked' : '' }}>
                                  <label for="test2">
                                    <a href="{{ route("drug", array_merge(request()->query(), ['type' => 1])) }}" class="text-gray" onclick="selectRadio('test2')">By Therapeutic Classification</a>
                                  </label>
                                </span>
                            </form>
                        </div>
                        <div class="search-cont">
                            <div class="input-group">
                                <input placeholder="Search Diseases" type="text">
                                <button>
                                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M18.5 18.5L24.5 24.5" stroke="#B8B8B8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path d="M11 20.5C16.2467 20.5 20.5 16.2467 20.5 11C20.5 5.75329 16.2467 1.5 11 1.5C5.75329 1.5 1.5 5.75329 1.5 11C1.5 16.2467 5.75329 20.5 11 20.5Z" stroke="#B8B8B8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div class="pagination">
                            <span>Jump to:</span>
                            <ul>
                                @foreach(range('a', 'z') as $letter)
                                    <li>
                                        <a class="{{ $shortTitle == $letter ? 'active' : '' }}" href="{{ URL::current() }}?{{ http_build_query(array_merge(request()->query(), ['short_title' => $letter])) }}">{{ strtoupper($letter) }}</a>
                                    </li>
                                @endforeach
                           </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="inner-list">
                                <ul>
                                    @foreach($drugs as $key => $drug)
                                        <li><a href="{{route("drugDetail",[$drug->slug])}}">{{$drug->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function selectRadio(id) {
        document.getElementById(id).checked = true;
    }
</script>

@include('front.layouts.footer')
