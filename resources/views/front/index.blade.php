@include('front.layouts.header')
<section class="banner">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="text-cont">
                        <h3>We're</h3>
                        <h1>Physican Healthcare</b></h1>
                            <h2>Worldwide System</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    </div>
                    <div class="input-group" style="flex-direction: column; height: 60px;">
                        <form method="get" action="{{route('diseases_condition')}}">
                            <input type="text" class="search-diesases" name="title" placeholder="Search Diesases">
                            <button type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="healthcare-services">
    <div class="inner-cont">
        <div class="container">
            <div class="top-text-cont">
                <h2>Managed Your<br><span>Healthcare Services</span></h2>
            </div>
            <div class="row">
                <div class="col-md-4" data-aos="zoom-in-up">
                    <div class="card">
                        <div class="img-cont">
                            <img src="{{asset("new_assets/img/home/7.png")}}" alt="">
                        </div>
                        <div class="text-cont">
                            <h3>Diseases & Condition</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <a href="{{route('diseases_condition')}}">
                                See more
                                <svg width="32" height="16" viewBox="0 0 32 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 8L31 8" stroke="#0797FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M24 0.999999L31 8L24 15" stroke="#0797FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" data-aos="zoom-in-up">
                    <div class="card">
                        <div class="img-cont">
                            <img src="{{asset("new_assets/img/home/8.png")}}" alt="">
                        </div>
                        <div class="text-cont">
                            <h3>Find Doctors</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <a href="{{route('doctor')}}">
                                See more
                                <svg width="32" height="16" viewBox="0 0 32 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 8L31 8" stroke="#0797FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M24 0.999999L31 8L24 15" stroke="#0797FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" data-aos="zoom-in-up">
                    <div class="card">
                        <div class="img-cont">
                            <img src="{{asset("new_assets/img/home/9.png")}}" alt="">
                        </div>
                        <div class="text-cont">
                            <h3>Book Appointments</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <a href="{{route('doctor')}}">
                                See more
                                <svg width="32" height="16" viewBox="0 0 32 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 8L31 8" stroke="#0797FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M24 0.999999L31 8L24 15" stroke="#0797FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-us-home">
    <div class="container">
        <div class="row">
            <div class="col-md-5" data-aos="zoom-out-right">
                <div class="img-cont">
                    <img class="img-fluid" src="{{asset("new_assets/img/home/13.png")}}" alt="about us">
                </div>
            </div>
            <div class="col-md-7" data-aos="zoom-out-left">
                <div class="text-cont">
                    <h4>ABOUT US</h4>
                    <h2>Physician Healthcare Worldwide System</h2>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                </div>
                <div class="ceo-cont">
                    <div class="profile-cont">
                        <img class="img-fluid" src="{{asset("new_assets/img/home/14.png")}}" alt="Ceo">
                    </div>
                    <div class="v-center">
                        <div class="text-content">
                            <h5>Dr. Emily Watson</h5>
                            <h6>CEO</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-specialists">
    <div class="container">
        <div class="text-cont">
            <h5>MEET</h5>
            <h2>Our <span>Specialists</span></h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
        </div>
        <div class="row">
            @foreach($doctors as $doctor)
                <div class="col-md-4 col-sm-6" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card">
                        <div class="img-cont" style=" overflow: hidden; height: 350px">
                            <img class="img-fluid" src="{{asset("storage/users/".$doctor->photo)}}" alt="" style="height: 350px;">
                        </div>
                        <div class="inner-text">
                            <h6>{{$doctor->first_name}} {{$doctor->last_name}}</h6>
                                <a href="#">{{$doctor->specialtie->name ?? ''}}</a>
                        </div>
                    </div>
                </div>
            @endforeach


            <div class="col-12 text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                <a href="{{route("doctor")}}" class="arrow-btn">
                    View <more></more>
                    <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 8L21 8" stroke="#FCFCFC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M14 0.999999L21 8L14 15" stroke="#FCFCFC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="trending-topics">
    <div class="inner-cont">
        <div class="container">
            <div class="top-text-cont">
                <h5>SOME OF OUR</h5>
                <h2>Trending <span>Topics</span></h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
            </div>
            <div class="row">
                <div class="col-md-4" data-aos="zoom-in-up">
                    <div class="card">
                        <div class="img-cont">
                            <a href="#">
                                <svg width="11" height="14" viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 0L11 6.63354L0 13.2671V0Z" fill="#FCFCFC"/>
                                </svg>
                            </a>
                        </div>
                        <div class="text-cont">
                            <h6>Cureus provides a smarter</h6>
                            <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <a href="#">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" data-aos="zoom-in-up">
                    <div class="card">
                        <div class="img-cont">
                            <a href="#">
                                <svg width="11" height="14" viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 0L11 6.63354L0 13.2671V0Z" fill="#FCFCFC"/>
                                </svg>
                            </a>
                        </div>
                        <div class="text-cont">
                            <h6>Cureus provides a smarter</h6>
                            <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <a href="#">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" data-aos="zoom-in-up">
                    <div class="card">
                        <div class="img-cont">
                            <a href="#">
                                <svg width="11" height="14" viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 0L11 6.63354L0 13.2671V0Z" fill="#FCFCFC"/>
                                </svg>
                            </a>
                        </div>
                        <div class="text-cont">
                            <h6>Cureus provides a smarter</h6>
                            <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <a href="#">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-us-home">
    <div class="container">
        <div class="row">
            <div class="col-md-5" data-aos="zoom-in-right">
                <div class="text-cont">
                    <h3>Contact Information</h3>
                    <p>Fill up the form and our Team will get back to you within 24 Hours.</p>
                    <ul>
                        <li>
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M29.9991 22.4608V26.977C30.0008 27.3962 29.9147 27.8112 29.7464 28.1954C29.5781 28.5795 29.3313 28.9243 29.0217 29.2078C28.7122 29.4912 28.3467 29.707 27.9488 29.8413C27.5508 29.9756 27.1292 30.0255 26.7108 29.9878C22.0692 29.4844 17.6106 27.9015 13.6934 25.3662C10.0488 23.0549 6.95889 19.9711 4.64301 16.3338C2.09379 12.4065 0.507362 7.93512 0.0122432 3.28192C-0.0254508 2.86563 0.0241216 2.44607 0.157804 2.04994C0.291486 1.65381 0.506348 1.28981 0.788712 0.981098C1.07108 0.672388 1.41475 0.425738 1.79786 0.25685C2.18097 0.087963 2.59512 0.000539584 3.01394 0.000145952H7.53912C8.27115 -0.00704452 8.98083 0.251667 9.53587 0.728058C10.0909 1.20445 10.4534 1.86601 10.5559 2.58944C10.7469 4.03473 11.1011 5.45381 11.6118 6.81962C11.8147 7.35844 11.8586 7.94403 11.7383 8.507C11.618 9.06997 11.3386 9.58672 10.933 9.99602L9.01734 11.9079C11.1646 15.6767 14.2914 18.7973 18.0677 20.9403L19.9833 19.0284C20.3935 18.6237 20.9112 18.3448 21.4753 18.2247C22.0394 18.1046 22.6262 18.1485 23.1661 18.351C24.5346 18.8607 25.9565 19.2142 27.4046 19.4048C28.1374 19.508 28.8065 19.8763 29.2849 20.4397C29.7633 21.0032 30.0174 21.7225 29.9991 22.4608Z" fill="#FCFCFC"/>
                            </svg>
                            <a href="#">+1 555 555 555</a>
                        </li>
                        <li>
                            <svg width="32" height="24" viewBox="0 0 32 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4 0H28C29.65 0 31 1.35 31 3V21C31 22.65 29.65 24 28 24H4C2.35 24 1 22.65 1 21V3C1 1.35 2.35 0 4 0Z" fill="#FCFCFC"/>
                                <path d="M31 3L16 13.5L1 3" stroke="#0797FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            <a href="#">info@example.com</a>
                        </li>
                        <li>
                            <svg width="30" height="38" viewBox="0 0 30 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M15 37.4445C15 37.4445 30 27.4445 30 15.7778C30 11.7996 28.4196 7.98427 25.6066 5.17123C22.7936 2.35818 18.9782 0.777832 15 0.777832C11.0218 0.777832 7.20644 2.35818 4.3934 5.17123C1.58035 7.98427 0 11.7996 0 15.7778C0 27.4445 15 37.4445 15 37.4445ZM20 15.7777C20 18.5392 17.7614 20.7777 15 20.7777C12.2386 20.7777 9.99998 18.5392 9.99998 15.7777C9.99998 13.0163 12.2386 10.7777 15 10.7777C17.7614 10.7777 20 13.0163 20 15.7777Z" fill="#FCFCFC"/>
                            </svg>
                            <a href="#">12345 S 44th St, Phoenix</a>
                        </li>
                    </ul>
                    <div class="social-text">
                        <h4>Our Social Links</h4>
                        <ul>
                            <li>
                                <a href="#">
                                    <svg width="12" height="24" viewBox="0 0 12 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.7778 0H8.56566C7.14581 0 5.78412 0.620435 4.78014 1.72482C3.77615 2.8292 3.21212 4.32706 3.21212 5.88889V9.42222H0V14.1333H3.21212V23.5556H7.49495V14.1333H10.7071L11.7778 9.42222H7.49495V5.88889C7.49495 5.57652 7.60776 5.27695 7.80855 5.05607C8.00935 4.8352 8.28169 4.71111 8.56566 4.71111H11.7778V0Z" fill="#FCFCFC"/>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect x="0.965271" width="23.5556" height="23.5556" rx="6" fill="#FCFCFC"/>
                                        <circle cx="12.7431" cy="12.1458" r="4.52083" stroke="#0797FF" stroke-width="2"/>
                                        <circle cx="12.743" cy="11.7777" r="2.86508" stroke="#FCFCFC"/>
                                        <circle cx="19.4732" cy="5.04777" r="1.18254" stroke="#0797FF"/>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <svg width="24" height="21" viewBox="0 0 24 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M24 0.691802C22.9747 1.43998 21.8394 2.01222 20.638 2.38648C19.9932 1.61946 19.1361 1.07581 18.1829 0.829067C17.2296 0.582324 16.2261 0.644391 15.3081 1.00687C14.3901 1.36935 13.6018 2.01476 13.0499 2.8558C12.498 3.69684 12.2091 4.69294 12.2222 5.70938V6.81701C10.3406 6.86748 8.47613 6.43578 6.79483 5.56034C5.11354 4.6849 3.66763 3.39291 2.58587 1.79943C2.58587 1.79943 -1.69696 11.7681 7.93941 16.1987C5.73432 17.7471 3.10747 18.5235 0.444458 18.4139C10.0808 23.9521 21.8586 18.4139 21.8586 5.67615C21.8576 5.36762 21.8289 5.05985 21.7729 4.75681C22.8657 3.64197 23.6369 2.23442 24 0.691802Z" fill="#FCFCFC"/>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-7" data-aos="zoom-in-left">
                <div class="inner-text">
                    <h4>Contact Us</h4>
                    <h2>Any Question or remark? Just write us a Message!</h2>
                </div>
                <div class="form-cont">
                    <form method="post" action="{{route('contactStore')}} ">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" name="first_name" placeholder="First Name" type="text">
                                @if ($errors->has('name'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('name') }}</div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" name="last_name" placeholder="Last Name" type="text">
                                @if ($errors->has('last_name'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('last_name') }}</div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" name="email" placeholder="Email Address" type="email">
                                @if ($errors->has('email'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('email') }}</div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" name="phone" placeholder="Phone" type="text">
                                @if ($errors->has('phone'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('phone') }}</div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-12">
                                <textarea name="concerns" placeholder="Message" id="" ></textarea>
                            </div>
                            <div class="col-12">
                                <button class="btn" type="submit">
                                    Submit
                                    <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1 8L21 8" stroke="#FCFCFC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path d="M14 0.999999L21 8L14 15" stroke="#FCFCFC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.layouts.footer')
