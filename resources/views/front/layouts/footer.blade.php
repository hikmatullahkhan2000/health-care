<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-4">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
                <div class="col-lg-4 col-md-4">
                    <h3>Quick Links</h3>
                    <ul>
                        <li><a href="#">Diseases & Condition</a></li>
                        <li><a href="#">Drug</a></li>
                        <li><a href="#">Procedure</a></li>
                        <li><a href="#">Doctors</a></li>
                        <li><a href="#">News feeds</a></li>
                        <li><a href="#">About us</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="inner-text">
                        <h3>Contact Us</h3>
                        <input type="text" placeholder="Enter your email">
                        <h3>Social Links</h3>
                        <ul class="social-links">
                            <li>
                                <a href="#">
                                    <svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g filter="url(#filter0_d_14_228)">
                                            <rect x="12" y="8" width="30" height="30" rx="6" fill="#064F85"/>
                                        </g>
                                        <path d="M30 16H28.0909C27.247 16 26.4377 16.3687 25.841 17.0251C25.2443 17.6815 24.9091 18.5717 24.9091 19.5V21.6H23V24.4H24.9091V30H27.4545V24.4H29.3636L30 21.6H27.4545V19.5C27.4545 19.3143 27.5216 19.1363 27.6409 19.005C27.7603 18.8737 27.9221 18.8 28.0909 18.8H30V16Z" fill="#FCFCFC"/>
                                        <defs>
                                            <filter id="filter0_d_14_228" x="0" y="0" width="54" height="54" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                                <feOffset dy="4"/>
                                                <feGaussianBlur stdDeviation="6"/>
                                                <feComposite in2="hardAlpha" operator="out"/>
                                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/>
                                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_14_228"/>
                                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_14_228" result="shape"/>
                                            </filter>
                                        </defs>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g filter="url(#filter0_d_14_231)">
                                            <rect x="12" y="8" width="30" height="30" rx="6" fill="#064F85"/>
                                        </g>
                                        <path d="M34 17.0067C33.3906 17.4585 32.7159 17.804 32.0018 18.0299C31.6186 17.5668 31.1092 17.2386 30.5427 17.0896C29.9761 16.9406 29.3797 16.9781 28.8341 17.197C28.2884 17.4158 27.8199 17.8055 27.4919 18.3133C27.1639 18.8211 26.9922 19.4225 27 20.0362V20.705C25.8817 20.7354 24.7735 20.4748 23.7743 19.9462C22.775 19.4177 21.9157 18.6376 21.2727 17.6755C21.2727 17.6755 18.7273 23.6943 24.4545 26.3694C23.144 27.3043 21.5827 27.773 20 27.7069C25.7273 31.0507 32.7273 27.7069 32.7273 20.0161C32.7267 19.8299 32.7096 19.644 32.6764 19.4611C33.3258 18.788 33.7842 17.9381 34 17.0067Z" fill="#FCFCFC"/>
                                        <defs>
                                            <filter id="filter0_d_14_231" x="0" y="0" width="54" height="54" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                                <feOffset dy="4"/>
                                                <feGaussianBlur stdDeviation="6"/>
                                                <feComposite in2="hardAlpha" operator="out"/>
                                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/>
                                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_14_231"/>
                                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_14_231" result="shape"/>
                                            </filter>
                                        </defs>
                                    </svg>

                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g filter="url(#filter0_d_14_239)">
                                            <rect x="12" y="8" width="30" height="30" rx="6" fill="#064F85"/>
                                        </g>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M23 16C21.3431 16 20 17.3431 20 19V27C20 28.6569 21.3431 30 23 30H31C32.6569 30 34 28.6569 34 27V19C34 17.3431 32.6569 16 31 16H23ZM30 23C30 24.6569 28.6569 26 27 26C25.3431 26 24 24.6569 24 23C24 21.3431 25.3431 20 27 20C28.6569 20 30 21.3431 30 23ZM27 24C27.5523 24 28 23.5523 28 23C28 22.4477 27.5523 22 27 22C26.4477 22 26 22.4477 26 23C26 23.5523 26.4477 24 27 24ZM32 19C32 19.5523 31.5523 20 31 20C30.4477 20 30 19.5523 30 19C30 18.4477 30.4477 18 31 18C31.5523 18 32 18.4477 32 19Z" fill="#FCFCFC"/>
                                        <defs>
                                            <filter id="filter0_d_14_239" x="0" y="0" width="54" height="54" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                                <feOffset dy="4"/>
                                                <feGaussianBlur stdDeviation="6"/>
                                                <feComposite in2="hardAlpha" operator="out"/>
                                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/>
                                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_14_239"/>
                                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_14_239" result="shape"/>
                                            </filter>
                                        </defs>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g filter="url(#filter0_d_14_234)">
                                            <rect x="12" y="8" width="30" height="30" rx="6" fill="#064F85"/>
                                        </g>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M34.2073 18.7904C34.4396 19.0298 34.6045 19.3263 34.6855 19.6499C34.9018 20.8489 35.0067 22.0653 34.9991 23.2836C35.0034 24.4838 34.8984 25.6818 34.6855 26.8629C34.6045 27.1864 34.4396 27.4829 34.2073 27.7223C33.9751 27.9617 33.6838 28.1356 33.3629 28.2264C32.1902 28.54 27.4997 28.54 27.4997 28.54C27.4997 28.54 22.8092 28.54 21.6366 28.2264C21.3222 28.1404 21.0353 27.9747 20.8036 27.7454C20.5719 27.5161 20.4033 27.2309 20.314 26.9174C20.0977 25.7184 19.9927 24.502 20.0004 23.2836C19.9944 22.0744 20.0994 20.8672 20.314 19.6771C20.395 19.3536 20.5599 19.0571 20.7921 18.8177C21.0244 18.5783 21.3157 18.4044 21.6366 18.3136C22.8092 18 27.4997 18 27.4997 18C27.4997 18 32.1902 18 33.3629 18.2863C33.6838 18.3771 33.9751 18.551 34.2073 18.7904ZM29.8859 23.2836L25.9658 25.513V21.0543L29.8859 23.2836Z" fill="#FCFCFC"/>
                                        <defs>
                                            <filter id="filter0_d_14_234" x="0" y="0" width="54" height="54" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                                <feOffset dy="4"/>
                                                <feGaussianBlur stdDeviation="6"/>
                                                <feComposite in2="hardAlpha" operator="out"/>
                                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/>
                                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_14_234"/>
                                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_14_234" result="shape"/>
                                            </filter>
                                        </defs>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="inner-cont">
                <p>Copyright by Physician Healthcare Worldwide System. All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>

<script src="{{asset("new_assets/js/jquery-3.6.1.min.js")}}"></script>
<script src="{{asset("new_assets/js/bootstrap.min.js")}}"></script>
<script src="{{asset("new_assets/js/owl.carousel.min.js")}}"></script>
<script src="{{asset("new_assets/js/aos.js")}}"></script>
<script src="{{asset("new_assets/js/script.js")}}"></script>

</body>
</html>
