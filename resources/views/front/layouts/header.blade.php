<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <META NAME="robots" CONTENT="noindex,nofollow">
    <title>Physican Worldwide | Home</title>
    <link rel="icon" type="image/x-icon" href="{{asset("new_assets/img/favicon.png")}}">
    <link rel="stylesheet" href="{{asset("new_assets/scss/style.css")}}" />
    ِِ
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

</head>
<body>
<header>
    <nav id="navbar_top" class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="{{asset("new_assets/img/logo.png")}}" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" aria-current="page" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('page/diseases-condition') ? 'active' : '' }}" href="{{route('diseases_condition')}}">Diseases & Condition</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('page/drugs') ? 'active' : '' }}" href="{{route("drug")}}">Drug</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('page/procedures') ? 'active' : '' }}" href="{{route("procedure")}}">Procedure</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('page/doctors') ? 'active' : '' }}" href="{{route("doctor")}}">Doctors</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('page/news-feeds') ? 'active' : '' }}" href="{{route("newsfeeds")}}">News feeds</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('page/about-us') ? 'active' : '' }}" href="{{route("aboutUs")}}">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('page/contact-us') ? 'active' : '' }}" href="{{route("contactUs")}}">Contact us</a>
                    </li>



                </ul>
                <ul class="navbar-nav login">
                @guest <!-- Check if the user is a guest (not logged in) -->
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{ route('login') }}">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">Sign Up</a>
                    </li>
                @else <!-- If the user is logged in -->
                    <li class="nav-item">
                        <span class="nav-link">{{ Auth::user()->first_name }}</span> <!-- Display the user's name -->
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    @endguest
                </ul>


            </div>
        </div>
    </nav>
</header>

<div class="container-fluid flex-grow-1 container-p-y">

    @if(session()->has('success'))
        <div class="row">
            <div class="d-flex align-items-center justify-content-center">
                <div class="col-md-12 p-2 m-2 my-auto justify-content-center">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        {!! session()->get('success') !!}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(session()->has('error'))
        <div class="row">
            <div class="d-flex align-items-center justify-content-center">
                <div class="col-md-12 p-2 m-2 my-auto justify-content-center">
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        {!! session()->get('error') !!}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

