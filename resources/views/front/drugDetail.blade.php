@include('front.layouts.header')

<section class="banner diseases-banner">
    <div class="container">
        <div class="text-cont">
            <h1>{{$drug->title}}</h1>
            <p>
                <?php
                echo $drug->short_description;
                ?>
            </p>
        </div>
    </div>
</section>


<section class="doctors-cont">
    <div class="container">
       <?php
        echo $drug->long_description;
       ?>
           <div class="image-show" data-title="{!! $drug->video_link !!}">
               @if($drug->video_link)
                   <video width="80%" height="auto" controls>
                       <source src="{{$drug->video_link?env('IMAGE_BASE_URL')."/{$drug->video_link}" :'/images/member.png'}}" type="video/mp4">
                       Your browser does not support the video tag.
                   </video>
               @endif

           </div>
    </div>

</section>


@include('front.layouts.footer')
