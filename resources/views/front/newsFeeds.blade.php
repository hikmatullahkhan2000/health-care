@include('front.layouts.header')
<section class="banner diseases-banner about-us-banner">
    <div class="container">
        <div class="text-cont">
            <h1>News Feeds</h1>
            <p>No two patients are alike. The topics covered on 5MinuteConsult will support your clinical decisions and improve patient care. Browse more than 2,000 diseases and conditions you encounter in your practice. Sample the site by taking a look at the free open topics!</p>
        </div>
    </div>
</section>


<section class="news-feeds-cont">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card-cont" >
                    @foreach($blogs as $blog)
                        <a href="{{route('newsFeedDetail',[$blog->slug])}}">
                            <div class="card">
                                <div class="card-top">
                                    {{--                                <h5>{{$blog->created_at}}</h5>--}}
                                    <h2>{{$blog->title}}</h2>
                                    <h6>{{$blog->short_description}}</h6>
                                </div>
                                <div class="card-bottom">
                                    <div class="imd-cont">
                                        <img src="{{asset("storage/".$blog->banner)}}" alt="News Feeds" class="img-fluid" style="height: 160px">
                                    </div>
{{--                                    <div class="bottom-text">--}}
{{--                                        <?php--}}
{{--                                        echo $blog->short_description;--}}
{{--                                        ?>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                        </a>

                    @endforeach

                </div>
            </div>
            <div class="col-lg-4">
                <div class="search-cont">
                    <form method="get" action="{{route('newsfeeds')}}" >
                        <input type="text" name="title" placeholder="Looking for...">
                        <button type="submit">
                            <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.3889 16.7778C13.4697 16.7778 16.7778 13.4697 16.7778 9.3889C16.7778 5.30812 13.4697 2 9.3889 2C5.30812 2 2 5.30812 2 9.3889C2 13.4697 5.30812 16.7778 9.3889 16.7778Z" stroke="black" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M20.9996 21L14.6069 14.6073" stroke="black" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.layouts.footer')
