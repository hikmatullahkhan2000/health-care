@include('front.layouts.header')
<section class="banner diseases-banner about-us-banner">
    <div class="container">
        <div class="text-cont">
            <h1>{{$blog->title}}</h1>
            <p>{{$blog->short_description}}</p>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="card-cont">
                <div class="card">
                    <div class="card-top">

                    </div>
                    <div class="card-bottom">
                        <div class="imd-cont">
                            <img src="{{asset("storage/".$blog->banner)}}" alt="News Feeds" class="img-fluid">
                        </div>
                        <div class="bottom-text">
                            <?php
                            echo $blog->long_description;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.layouts.footer')
