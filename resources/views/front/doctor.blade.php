@include('front.layouts.header')
<?php
$states = [
    'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];
?>
<section class="banner diseases-banner">
    <div class="container">
        <div class="text-cont">
            <h1>Doctors</h1>
            <p>No two patients are alike. The topics covered on 5MinuteConsult will support your clinical decisions and improve patient care. Browse more than 2,000 diseases and conditions you encounter in your practice. Sample the site by taking a look at the free open topics!</p>
        </div>
    </div>
</section>


<section class="doctors-cont">
    <div class="container">
            <div class="inner-cont" style="padding: 4rem 0;">
                <div class="row">
                    <div class="filter-cont">
                        <form  method="get" class="row g-3" action="{{route("doctor")}}">
                            <div class="col-md-4">
                                <select id="state" name="state" class="block mt-1 w-full " style="padding: 20px 10px;border-radius: 12px;background: #FCFCFC;border: 1px solid #E9E9E9;box-shadow: 0px 4px 24px rgba(0, 0, 0, 0.06);color: #323232; outline: none;  width: 100%;">
                                    <option value="">Select State</option>
                                    @foreach($states as $state)
                                        <option value="{{ $state }}">{{ $state }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select id="specialty" name="specialty"
                                        class="block mt-1 w-full " style="padding: 20px 10px;border-radius: 12px;background: #FCFCFC;border: 1px solid #E9E9E9;box-shadow: 0px 4px 24px rgba(0, 0, 0, 0.06);color: #323232; outline: none;  width: 100%;">
                                    <option value="">Select Specialties</option>
                                    @foreach(\App\Models\Specialty::all() as $key =>  $data)
                                        <option value="{{$data->id}}" >
                                            {{$data->name}}</option>
                                    @endForeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary" style="background: #0797FF;box-shadow: 0px 4px 24px rgba(3, 104, 176, 0.37);color: #fff; border-radius: 12px;width: 100%;padding: 20px 25px;font-size: 18px;line-height: 21px;font-weight: 600;">
                                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M18 18L24 24" stroke="#FCFCFC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path d="M10.5 20C15.7467 20 20 15.7467 20 10.5C20 5.25329 15.7467 1 10.5 1C5.25329 1 1 5.25329 1 10.5C1 15.7467 5.25329 20 10.5 20Z" stroke="#FCFCFC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                    Search
                                </button>
                            </div>
                        </form>
                    </div>
                    @foreach($doctors as $doctor)
                        <div class="col-md-4 col-sm-6">
                            <div class="card">
                                <div class="img-cont" style="width: 366px; height: 250px; overflow: hidden;">
                                    <img src="{{asset("storage/users/".$doctor->photo)}}" class="img-fluid" style="width: 100%; height: auto; display: block;" alt="Doctor">
                                </div>

                                <div class="text-cont">
                                    <h3>{{$doctor->first_name}} {{$doctor->last_name}}</h3>
                                    <h5>{{$doctor->specialtie->name ?? ''}} </h5>
                                    <ul class="doctor-info" data-email="{{$doctor->email}}" data-phone="{{$doctor->phone}}" data-address="{{$doctor->address}}">
                                        <li>
                                            <svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="23" cy="23" r="23" fill="#ECECEC"/>
                                                <path d="M31.538 27.7278V30.287C31.539 30.5245 31.4874 30.7597 31.3864 30.9774C31.2855 31.1951 31.1374 31.3905 30.9516 31.5511C30.7659 31.7117 30.5466 31.834 30.3078 31.9101C30.0691 31.9862 29.8161 32.0145 29.565 31.9931C26.7801 31.7078 24.105 30.8109 21.7546 29.3742C19.5679 28.0644 17.7139 26.3169 16.3244 24.2558C14.7948 22.0303 13.843 19.4966 13.5459 16.8598C13.5233 16.6239 13.553 16.3861 13.6333 16.1616C13.7135 15.9372 13.8424 15.7309 14.0118 15.556C14.1812 15.381 14.3874 15.2413 14.6173 15.1455C14.8472 15.0498 15.0956 15.0003 15.3469 15.0001H18.062C18.5013 14.996 18.9271 15.1426 19.2601 15.4126C19.5931 15.6825 19.8106 16.0574 19.8721 16.4673C19.9867 17.2863 20.1992 18.0905 20.5056 18.8645C20.6274 19.1698 20.6538 19.5016 20.5816 19.8206C20.5094 20.1396 20.3417 20.4325 20.0984 20.6644L18.949 21.7478C20.2373 23.8835 22.1134 25.6518 24.3792 26.8662L25.5286 25.7828C25.7747 25.5534 26.0853 25.3954 26.4238 25.3273C26.7622 25.2593 27.1143 25.2841 27.4382 25.3989C28.2593 25.6877 29.1125 25.888 29.9814 25.9961C30.421 26.0545 30.8225 26.2632 31.1095 26.5825C31.3965 26.9018 31.549 27.3094 31.538 27.7278Z" fill="#0797FF"/>
                                                <!-- Phone number to display on hover -->
                                                <title>{{$doctor->phone}}</title>
                                            </svg>
                                        </li>

                                        <li>
                                            <svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="23" cy="23" r="23" fill="#ECECEC"/>
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M22.7692 34.4359C22.7692 34.4359 31.5385 28.5897 31.5385 21.7692C31.5385 19.4435 30.6146 17.213 28.97 15.5684C27.3255 13.9239 25.095 13 22.7692 13C20.4435 13 18.213 13.9239 16.5684 15.5684C14.9239 17.213 14 19.4435 14 21.7692C14 28.5897 22.7692 34.4359 22.7692 34.4359ZM25.6923 20.7359C25.6923 22.3502 24.3836 23.659 22.7692 23.659C21.1549 23.659 19.8462 22.3502 19.8462 20.7359C19.8462 19.1215 21.1549 17.8128 22.7692 17.8128C24.3836 17.8128 25.6923 19.1215 25.6923 20.7359Z" fill="#0797FF"/>
                                                <title>{{$doctor->address}}</title>
                                            </svg>
                                        </li>

                                        <li>
                                            <svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="23" cy="23" r="23" fill="#ECECEC"/>
                                                <path d="M15.4386 16H30.6386C31.6836 16 32.5386 16.8221 32.5386 17.8269V28.7885C32.5386 29.7933 31.6836 30.6154 30.6386 30.6154H15.4386C14.3936 30.6154 13.5386 29.7933 13.5386 28.7885V17.8269C13.5386 16.8221 14.3936 16 15.4386 16Z" fill="#0797FF"/>
                                                <path d="M32.5386 17.8269L23.0386 24.2212L13.5386 17.8269" fill="#0797FF"/>
                                                <path d="M32.5386 17.8269L23.0386 24.2212L13.5386 17.8269" stroke="#FCFCFC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                <title>{{$doctor->email}}</title>
                                            </svg>
                                        </li>
                                    </ul>
                                    <a href="{{route("requestAppointment",[$doctor->id])}}">Request Appointment</a>
                                    <div class="dropdown">
                                        @php
                                            $service = \App\Models\Services::find($doctor->service_id);
                                        @endphp
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                                            @if($service)
                                                {{ $service->name }}
                                            @endif
                                        </button>
{{--                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">--}}

{{--                                            @if($service)--}}
{{--                                                <li>--}}
{{--                                                    <a class="dropdown-item" href="#">{{ $service->name }}</a>--}}
{{--                                                </li>--}}
{{--                                            @endif--}}
{{--                                        </ul>--}}
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function(){
        $('.doctor-info li').click(function(){
            var email = $(this).parent().data('email');
            var phone = $(this).parent().data('phone');
            var address = $(this).parent().data('address');

            var message = "Email: " + email + "\nPhone: " + phone + "\nAddress: " + address;
            alert(message);
        });
    });
</script>

@include('front.layouts.footer')
