@include('front.layouts.header')

<section class="banner diseases-banner">
    <div class="container">
        <div class="text-cont">
            <h1>{{$procedure->title}}</h1>
            <p>
                <?php
                echo $procedure->short_description;
                ?>
            </p>
        </div>
    </div>
</section>


<section class="doctors-cont">
    <div class="container">
       <?php
        echo $procedure->long_description;
       ?>
           <div class="image-show" data-title="{!! $procedure->video_link !!}">
               @if($procedure->video_link)
                   <video width="80%" height="auto" controls>
                       <source src="{{$procedure->video_link?env('IMAGE_BASE_URL')."/{$procedure->video_link}" :'/images/member.png'}}" type="video/mp4">
                       Your browser does not support the video tag.
                   </video>
               @endif

           </div>
    </div>

</section>


@include('front.layouts.footer')
