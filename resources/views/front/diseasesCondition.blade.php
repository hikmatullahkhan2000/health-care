@include('front.layouts.header')
<section class="banner diseases-banner">
    <div class="container">
        <div class="text-cont">
            <h1>Diseases & Condition</h1>
            <p>No two patients are alike. The topics covered on 5MinuteConsult will support your clinical decisions and improve patient care. Browse more than 2,000 diseases and conditions you encounter in your practice. Sample the site by taking a look at the free open topics!</p>
        </div>
    </div>
</section>


<section class="diseases-cont">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="right-text">
                    <h4>All Specialties</h4>
                    <ul>
                        @foreach(\App\Models\Specialty::all() as $key => $data)
                            <li><a href="{{ route("diseases_condition", array_merge(request()->query(), ['specialty' => $data->id])) }}">{{ $data->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="left-text">
                    <div class="top-cont">
                        <div class="search-cont">
                            <div class="input-group">
                                <form method="get" action="{{route('diseases_condition')}}">
                                    <input placeholder="Search Diseases" type="text" name="title">
                                    <button>
                                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M18.5 18.5L24.5 24.5" stroke="#B8B8B8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M11 20.5C16.2467 20.5 20.5 16.2467 20.5 11C20.5 5.75329 16.2467 1.5 11 1.5C5.75329 1.5 1.5 5.75329 1.5 11C1.5 16.2467 5.75329 20.5 11 20.5Z" stroke="#B8B8B8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </button>
                                </form>

                            </div>
                        </div>
                        <div class="pagination">
                            <span>Jump to:</span>
                            <ul>
                                    @foreach(range('a', 'z') as $letter)
                                        <li>
                                            <a class="{{ $shortTitle == $letter ? 'active' : '' }}" href="{{ URL::current() }}?{{ http_build_query(array_merge(request()->query(), ['short_title' => $letter])) }}">{{ strtoupper($letter) }}</a>
                                        </li>
                                    @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="inner-list">
                                <ul>
                                    @foreach($diseases as $key => $disease)
                                        <li><a href="{{route("diseasesConditionDetail",[$disease->slug])}}">{{$disease->title}}</a></li>

                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
{{--                            <div class="news-cont">--}}
{{--                                <h5>Try These Free</h5>--}}
{{--                                <ul>--}}
{{--                                    <li>--}}
{{--                                        <div class="img-cont">--}}
{{--                                            <img src="{{asset('new_assets/img/common/1.png')}}" class="img-fluid" alt="Breast Cancer">--}}
{{--                                        </div>--}}
{{--                                        <div class="text-cont">--}}
{{--                                            <h6>Breast Cancer</h6>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <div class="img-cont">--}}
{{--                                            <img src="{{asset('new_assets/img/common/2.png')}}" class="img-fluid" alt="Coronavirus Disease 2019 (COVID-19)">--}}
{{--                                        </div>--}}
{{--                                        <div class="text-cont">--}}
{{--                                            <h6>Coronavirus Disease 2019 (COVID-19)</h6>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <div class="img-cont">--}}
{{--                                            <img src="{{asset('new_assets/img/common/3.png')}}" class="img-fluid" alt="Endometrial Cancer and Uterine Sarcoma">--}}
{{--                                        </div>--}}
{{--                                        <div class="text-cont">--}}
{{--                                            <h6>Endometrial Cancer and Uterine Sarcoma</h6>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                                <h5>Recent News</h5>--}}
{{--                                <ul>--}}
{{--                                    <li>--}}
{{--                                        <div class="img-cont">--}}
{{--                                            <img src="{{asset('new_assets/img/common/1.png')}}" class="img-fluid" alt="Breast Cancer">--}}
{{--                                        </div>--}}
{{--                                        <div class="text-cont">--}}
{{--                                            <h6>NOVEMBER 12, 2022</h6>--}}
{{--                                            <p>Thoughts on Procrastination</p>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <div class="img-cont">--}}
{{--                                            <img src="{{asset('new_assets/img/common/2.png')}}" class="img-fluid" alt="Coronavirus Disease 2019 (COVID-19)">--}}
{{--                                        </div>--}}
{{--                                        <div class="text-cont">--}}
{{--                                            <h6>NOVEMBER 10, 2022</h6>--}}
{{--                                            <p>People who made inspiring life changes</p>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <div class="img-cont">--}}
{{--                                            <img src="{{asset('new_assets/img/common/3.png')}}" class="img-fluid" alt="Endometrial Cancer and Uterine Sarcoma">--}}
{{--                                        </div>--}}
{{--                                        <div class="text-cont">--}}
{{--                                            <h6>NOVEMBER 08, 2022</h6>--}}
{{--                                            <p>The incredible power of small changes</p>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.layouts.footer')
