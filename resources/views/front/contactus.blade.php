@include('front.layouts.header')
<section class="banner diseases-banner about-us-banner">
    <div class="container">
        <div class="text-cont">
            <h1>Contact Us</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500</p>
        </div>
    </div>
</section>

<section class="contact-us-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="img-cont">
                    <img src="{{asset("front_assets/img/contact-us/1.png")}}" alt="contact us" class="img-fluid">
                </div>
            </div>
            <div class="col-md-7">
                <div class="text-cont">
                    <h2>Have Some <span>Questions?</span></h2>
                    <h6>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text.</h6>
                    <h3>Contact Us</h3>
                    <p>Our friendly team would love to hear from you!</p>
                </div>
                <form method="post" action="{{route('contactStore')}} ">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <label class="form-label">First name <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="first_name" placeholder="First Name" type="text">
                                @if ($errors->has('name'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('name') }}</div>
                                    </div>
                                @endif                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label class="form-label">Last name <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="last_name" placeholder="Last Name" type="text">
                                @if ($errors->has('last_name'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('last_name') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label  class="form-label">Email <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="email" placeholder="Email Address" type="email">
                                @if ($errors->has('email'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('email') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label  class="form-label">Area Code <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="area_code" placeholder="area_code" type="text">
                                @if ($errors->has('area_code'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('area_code') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label  class="form-label">Telephone <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="phone" placeholder="phone" type="text">
                                @if ($errors->has('phone'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('phone') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label  class="form-label">State <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="state" placeholder="state" type="text">
                                @if ($errors->has('state'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('state') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6">
                            <label  class="form-label">Address <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="address" placeholder="address" type="text">
                                @if ($errors->has('address'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('address') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label  class="form-label">City <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="city" placeholder="city" type="text">
                                @if ($errors->has('city'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('city') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6">
                            <label  class="form-label">Please Tell us about your concerns <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="concerns" placeholder="Please Tell us about your concerns" type="text">
                                @if ($errors->has('concerns'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('concerns') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label  class="form-label">Zip Code <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="zip_code" placeholder="zip_code" type="text">
                                @if ($errors->has('zip_code'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('zip_code') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary w-100">Submit</button>
                        </div>
                        <div class="col-12">
                            <button type="reset" class="btn btn-primary reset w-100">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@include('front.layouts.footer')
