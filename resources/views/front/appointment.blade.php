@include('front.layouts.header')
<section class="banner diseases-banner about-us-banner">
    <div class="container">
        <div class="text-cont">
            <h1>Request Appointment</h1>
        </div>
    </div>
</section>

<section class="contact-us-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="img-cont">
                    <img src="{{asset("front_assets/img/contact-us/1.png")}}" alt="contact us" class="img-fluid">
                </div>
            </div>
            <div class="col-md-7">
                <div class="text-cont">
                    <h2><span>Appointment</span></h2>
                </div>
                <form method="post" action="{{route('appointmentStore')}} ">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <label class="form-label">Full name <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="full_name" placeholder="Full Name" type="text">
                                @if ($errors->has('full_name'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('full_name') }}</div>
                                    </div>
                                @endif
                                <input name="user_id" type="hidden" value="{{$id}}">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label  class="form-label">Email <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="email" placeholder="Email Address" type="email">
                                @if ($errors->has('email'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('email') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label  class="form-label">Telephone <span>*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="phone" placeholder="phone" type="text">
                                @if ($errors->has('phone'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('phone') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6">
                            <label  class="form-label">Please Tell us about your concerns </label>
                            <div class="input-group">
                                <input class="form-control" name="concerns" placeholder="Please Tell us about your concerns" type="text">
                                @if ($errors->has('concerns'))
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('concerns') }}</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary w-100">Submit</button>
                        </div>
                        <div class="col-12">
                            <button type="reset" class="btn btn-primary reset w-100">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@include('front.layouts.footer')
