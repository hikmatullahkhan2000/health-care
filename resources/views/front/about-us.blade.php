@include('front.layouts.header')
<section class="banner diseases-banner about-us-banner">
    <div class="container">
        <div class="text-cont">
            <h1>About Us</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500</p>
        </div>
    </div>
</section>

<section class="about-us-home">
    <div class="container">
        <div class="row">
            <div class="col-md-5" data-aos="zoom-out-right">
                <div class="img-cont">
                    <img class="img-fluid" src="{{asset("front_assets/img/home/13.png")}}" alt="about us">
                </div>
            </div>
            <div class="col-md-7" data-aos="zoom-out-left">
                <div class="text-cont">
                    <h4>ABOUT US</h4>
                    <h2>Physician Healthcare Worldwide System</h2>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                </div>
                <div class="ceo-cont">
                    <div class="profile-cont">
                        <img class="img-fluid" src="{{asset("front_assets/img/home/14.png")}}" alt="Ceo">
                    </div>
                    <div class="v-center">
                        <div class="text-content">
                            <h5>Dr. Emily Watson</h5>
                            <h6>CEO</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="last-sec">
            <div class="row">
                <div class="col-md-7" data-aos="zoom-out-right">
                    <div class="text-cont">
                        <h2>Physician Healthcare Worldwide System</h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                    </div>
                </div>
                <div class="col-md-5" data-aos="zoom-out-left">
                    <div class="img-cont">
                        <img class="img-fluid" src="{{asset("front_assets/img/about-us/1.png")}}" alt="about us">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('front.layouts.footer')
