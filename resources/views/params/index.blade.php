<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Params') }}
         <a href="{{ route('params.create') }}" class="btn btn-primary pull-right button-left"><i class="menu-icon tf-icons bx bx-plus"></i> Add New</a>
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Key</th>
                                    <th>Value</th>
                                    <th>type</th>
                                    <th>role</th>
                                    <th>active</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($params as $param)
                                    <tr>
                                        <td>{{$param->id ?? '-'}}</td>
                                        <td>{{$param->key ?? '-'}}</td>
                                        <td style="max-width: 250px;">{{ Str::limit($param->value, 110) ?? '-'}}</td>
                                        <td>{{$param->type ?? '-'}}</td>
                                        <td>{{ App\Models\Params::role[$param->role] ?? '-' }}</td>
                                        <td>
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" type="checkbox" {{$param->active ? 'checked' : ''}} onclick="window.location.href ='{{ route("params.toggle-status",["id" => $param->id]) }}';">
                                            </div>
                                        </td>
                                        <td>{{$param->created_at ?? '-'}}</td>
                                        <td>
                                            <a href="{{ route('params.edit', $param->id) }}"><i class="tf-icons bx bx-edit"></i></a>


                                            <form style="display: inline" action="{{ route('params.destroy', $param->id)}}"
                                                  id="delete_form_{{$param->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$param->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="scripts">
        <script>
            $(function () {
            $("#usersTable").DataTable({
                order: [[0, 'desc']],
            });

        });
        </script>
    </x-slot>
</x-app-layout>
