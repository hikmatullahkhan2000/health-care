<x-app-layout>
    <x-slot name="header">
        {{  $model->exists ? __('Edit Param') : __('Create Param')  }}
    </x-slot>

    <div class="row">
        <!-- FormValidation -->
        <div class="col-2"></div>
        <div class="row">
            <!-- FormValidation -->
            <div class="col-2"></div>
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                        <form id="paramForm" method="post" class="form" action="{{ ($model->exists)? route('params.update', [$model->id]): route('params.store') }}" enctype="multipart/form-data">
                            @if ($model->exists)
                            @method('PUT')
                            @endif
                            @csrf

                             <div class="col-12">
                                <h6 class="fw-semibold">
                                    {{  $model->exists ? __('Edit Param') : __('Create Param')  }}

                                </h6>
                                <hr class="mt-0">
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="fv-plugins-icon-container mb-2">
                                        <label class="form-label" for="form_user_name">Key</label>
                                        <input type="text" id="form_key" value="{{ old('key') ? old('key') : $model->key }}" class="form-control {{ $errors->has('key') ? ' is-invalid' : '' }}" placeholder="Type User Name" name="key">
                                        @if ($errors->has('key'))
                                        <div class="fv-plugins-message-container invalid-feedback">
                                            <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('key') }}</div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="fv-plugins-icon-container mb-2">
                                        <label class="form-label" for="form_type">Type</label>
                                        <select id="form_type" class="form-control {{ $errors->has('type') ? ' is-invalid' : '' }}" name="type">
                                            <option value="value" {{ (old('type') == 'value' ? 'selected' : $model->type == 'value') ? 'selected' : ''  }}>value</option>
                                            <option value="text" {{ (old('type')  == 'text' ? 'selected' : $model->type == 'text') ? 'selected' : ''  }}>text</option>
                                        </select>
                                        @if ($errors->has('type'))
                                        <div class="fv-plugins-message-container invalid-feedback">
                                            <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('type') }}</div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
{{--                                <div class="col-6">--}}
{{--                                    <div class="fv-plugins-icon-container mb-2">--}}
{{--                                        <label class="form-label" for="form_role">Role</label>--}}
{{--                                        <select id="form_role" class="form-control {{ $errors->has('role') ? ' is-invalid' : '' }}" name="role">--}}
{{--                                            @foreach (App\Models\Params::role as $key => $value)--}}
{{--                                                <option value="{{ $key }}" {{ (old('role') ? 'selected' : $model->role == $key) ? 'selected' : ''  }}>{{ $value }}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                        @if ($errors->has('role'))--}}
{{--                                        <div class="fv-plugins-message-container invalid-feedback">--}}
{{--                                            <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('role') }}</div>--}}
{{--                                        </div>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-12">
                                    <div id="richTextEditorDiv" class="fv-plugins-icon-container mb-2">
                                        <label class="form-label" for="form_user_email">Value</label>
                                        <div id="richTextEditor" style="min-height: 200px;"></div>
                                        @if ($errors->has('value'))
                                        <div class="fv-plugins-message-container invalid-feedback">
                                            <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('value') }}</div>
                                        </div>
                                        @endif
                                    </div>
                                    <div id="textAreaDiv" class="fv-plugins-icon-container mb-2">
                                        <label class="form-label" for="form_user_email">Value</label>
                                        <textarea class="form-control {{ $errors->has('key') ? ' is-invalid' : '' }}" name="value" rows="10">@if (old('value')) {{ old('value') }} @elseif($model->value) {{ $model->value }} @endif</textarea>
                                        @if ($errors->has('value'))
                                        <div class="fv-plugins-message-container invalid-feedback">
                                            <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('value') }}</div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mt-3">
                                <button type="submit" name="submitButton" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
            <!-- /FormValidation -->
        </div>
        <!-- /FormValidation -->
    </div>
    <x-slot name="scripts">
        <script src="https://cdn.quilljs.com/1.3.6/quill.min.js"></script>
        <script>
            var container = document.querySelector('#richTextEditor');
            var quill = new Quill(container, {
                modules: {
                    toolbar: [
                        [{
                            header: [1, 2, false]
                        }],
                        ['bold', 'italic', 'underline'],
                        ['image', 'code-block']
                    ]
                },
                placeholder: 'param value...',
                theme: 'snow' // or 'bubble'
            });

            document.querySelector('#paramForm').addEventListener('submit', (e) => {
                if (document.querySelector('#form_type').value == "text") {
                    document.querySelector('textarea').value = document.querySelector('#richTextEditor > div').innerHTML
                }
            });

            if (document.querySelector('#form_type').value == "value") {
                document.querySelector('#textAreaDiv').style.display = "block"
                document.querySelector('#richTextEditorDiv').style.display = "none"
            } else{
                document.querySelector('#textAreaDiv').style.display = "none"
                document.querySelector('#richTextEditorDiv').style.display = "block"
            }

            document.querySelector('#form_type').addEventListener('change', (e) => {
                if (e.target.value == "value") {
                    document.querySelector('textarea').value = "";
                    document.querySelector('#textAreaDiv').style.display = "block"
                    document.querySelector('#richTextEditorDiv').style.display = "none"
                } else {
                    document.querySelector('#textAreaDiv').style.display = "none"
                    document.querySelector('#richTextEditorDiv').style.display = "block"
                }
            });

        </script>
    </x-slot>

</x-app-layout>
