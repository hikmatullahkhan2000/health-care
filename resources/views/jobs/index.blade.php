<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Jobs') }}
        <a href="{{ route('jobs.create') }}" class="btn btn-primary pull-right button-left">
            <i class="menu-icon tf-icons bx bx-plus"></i>Add New
        </a>
    </x-slot>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                                <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
{{--                                        <th>Title</th>--}}
                                        <th>Price</th>
                                        <th>Date</th>
                                        <th>Address</th>
                                        <th>Square</th>
                                        <th>Price Doc</th>
{{--                                        <th>Job Doc</th>--}}
                                        <th>Job Requirement Doc</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jobs as $job)
                                    <tr>
                                        <td>{{$job->id}}</td>
{{--                                        <td>{{$job->title}}</td>--}}
                                        <td>{{$job->price}}</td>
                                        <td>{{$job->date}}</td>
                                        <td>{{$job->address}}</td>
                                        <td>{{$job->square}}</td>
                                        <td class="text-nowrap">
                                            @if (!empty($job->price_doc))
                                                <a class="btn btn-sm btn-outline-info" href="{{URL::to('/storage').'/'.$job->price_doc ?? '#'}}" download=""><i class="tf-icons bx bx-sm bx-cloud-download"></i> Download</a>
                                            @else
                                                <span class="text-muted">N/A</span>
                                            @endif
                                        </td>
{{--                                        <td class="text-nowrap">--}}
{{--                                            @if (!empty($job->job_doc))--}}
{{--                                                <a class="btn btn-sm btn-outline-info" href="{{URL::to('/storage').'/'.$job->job_doc ?? '#'}}" download=""><i class="tf-icons bx bx-sm bx-cloud-download"></i> Download</a>--}}
{{--                                            @else--}}
{{--                                                <span class="text-muted">N/A</span>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
                                        <td class="text-nowrap">
                                            @if (!empty($job->job_requirement_doc))
                                                <a class="btn btn-sm btn-outline-info" href="{{URL::to('/storage').'/'.$job->job_requirement_doc ?? '#'}}" download=""><i class="tf-icons bx bx-sm bx-cloud-download"></i> Download</a>
                                            @else
                                                <span class="text-muted">N/A</span>
                                            @endif
                                        </td>
                                        <td>{{\App\Models\Job::statuses[$job->status] ?? '-'}}</td>
                                        <td>
                                            <a href="{{ route('jobs.edit', $job->id) }}" ><i class="tf-icons bx bx-edit"></i></a>
                                            <a href="{{ route('jobs.show', $job->id) }}" ><i class="tf-icons bx bx-image"></i></a>
{{--                                            <a href="{{ route('jobAssignMember', $job->id) }}" title="Assign job" ><i class="tf-icons bx bx-street-view"></i></a>--}}

                                            <form style="display: inline" action="{{ route('jobs.destroy', $job->id)}}"
                                                  id="delete_form_{{$job->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$job->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-slot name="scripts">
        <script>
            $(function () {
                $("#usersTable").DataTable({
                    order: [[0, 'desc']],
                });
            });
        </script>
    </x-slot>
</x-app-layout>
