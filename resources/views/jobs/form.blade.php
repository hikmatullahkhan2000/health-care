<x-app-layout>
    <x-slot name="header">
        {{  $model->exists ? __('Edit Jobs') : __('Create Jobs')  }}

    </x-slot>


    <div class="row">
        <div class="col-md-12">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1default" data-toggle="tab">Custom Form</a></li>

                        @if(!$model->exists)
                            <li><a href="#tab2default" data-toggle="tab">Airtable Form</a></li>
                        @endif
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form method="post" class="form" enctype="multipart/form-data"
                                              action="{{ ($model->exists)? route('jobs.update', [$model->id]): route('jobs.store') }}">
                                            @if ($model->exists)
                                                @method('PUT')
                                            @endif
                                            @csrf

                                            <div class="col-12">
                                                <h6 class="fw-semibold">
                                                    {{  $model->exists ? __('Edit Jobs') : __('Create Jobs')  }}
                                                </h6>
                                                <hr class="mt-0">
                                            </div>

                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="fv-plugins-icon-container">
                                                        <label class="form-label" for="title">Title</label>
                                                        <input type="text" id="title"
                                                               value="{!! old('title')??$model->title !!}"
                                                               class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                               placeholder="title"
                                                               name="title">
                                                        @if ($errors->has('title'))
                                                            <div class="fv-plugins-message-container invalid-feedback">
                                                                <div data-field="formValidationBio"
                                                                     data-validator="stringLength">{{ $errors->first('title') }}</div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="fv-plugins-icon-container">
                                                        <label class="form-label" for="date">Date</label>
                                                        <input type="date" id="date"
                                                               value="{!! old('date')??$model->date !!}"
                                                               class="form-control {{ $errors->has('date') ? ' is-invalid' : '' }}"
                                                               placeholder="date"
                                                               name="date">
                                                        @if ($errors->has('date'))
                                                            <div class="fv-plugins-message-container invalid-feedback">
                                                                <div data-field="formValidationBio"
                                                                     data-validator="stringLength">{{ $errors->first('date') }}</div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="fv-plugins-icon-container">
                                                        <label for="status">Status</label>
                                                        <select id="status" name="status"
                                                                class="form-control">
                                                            <option value="">Select</option>
                                                            @foreach(\App\Models\Job::statuses as $key => $status)
                                                                <option value="{{ $key }}" {{ (old('status') == $key) ||(isset($model->status) && $model->status == $key) ? 'selected' : '' }} >
                                                                    {{ strtolower($status) }}
                                                                </option>
                                                            @endForeach
                                                        </select>

                                                        @if ($errors->has('status'))
                                                            <div class="help-block">
                                        <span class="" role="alert">
                                        <strong class="text-danger">{{ $errors->first('status') }}</strong>
                                    </span></div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="fv-plugins-icon-container">
                                                        <label class="form-label" for="estimated_day">Estimated Day</label>
                                                        <input type="text" id="estimated_day"
                                                               value="{!! old('estimated_day')??$model->estimated_day !!}"
                                                               class="form-control {{ $errors->has('estimated_day') ? ' is-invalid' : '' }}"
                                                               placeholder="estimated_day"
                                                               name="estimated_day">
                                                        @if ($errors->has('estimated_day'))
                                                            <div class="fv-plugins-message-container invalid-feedback">
                                                                <div data-field="formValidationBio"
                                                                     data-validator="stringLength">{{ $errors->first('estimated_day') }}</div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="fv-plugins-icon-container">
                                                        <label class="form-label" for="price">Price</label>
                                                        <input type="text" id="price"
                                                               value="{!! old('price')??$model->price !!}"
                                                               class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}"
                                                               placeholder="price"
                                                               name="price">
                                                        @if ($errors->has('price'))
                                                            <div class="fv-plugins-message-container invalid-feedback">
                                                                <div data-field="formValidationBio"
                                                                     data-validator="stringLength">{{ $errors->first('price') }}</div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="fv-plugins-icon-container">
                                                        <label class="form-label" for="square">Square</label>
                                                        <input type="text" id="square"
                                                               value="{!! old('square')??$model->square !!}"
                                                               class="form-control {{ $errors->has('square') ? ' is-invalid' : '' }}"
                                                               placeholder="square"
                                                               name="square">
                                                        @if ($errors->has('square'))
                                                            <div class="fv-plugins-message-container invalid-feedback">
                                                                <div data-field="formValidationBio"
                                                                     data-validator="stringLength">{{ $errors->first('square') }}</div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-3">
                                                    <div class="fv-plugins-icon-container mb-2">
                                                        <label class="form-label" for="price_doc">Price Doc</label>
                                                        <input type="file" id="price_doc" class="form-control" value="{!! old('price_doc')??$model->price_doc !!}"
                                                               placeholder="price_doc" name="price_doc">
                                                        @if ($errors->has('price_doc'))
                                                            <div class="fv-plugins-message-container invalid-feedback">
                                                                <div data-field="formValidationBio"
                                                                     data-validator="stringLength">{{ $errors->first('price_doc') }}</div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
{{--                                                <div class="col-3">--}}
{{--                                                    <div class="fv-plugins-icon-container mb-2">--}}
{{--                                                        <label class="form-label" for="job_doc">Job Doc</label>--}}
{{--                                                        <input type="file" multiple id="job_doc" class="form-control" value="{!! old('job_doc')??$model->job_doc !!}"--}}
{{--                                                               placeholder="job_doc" name="job_doc[]">--}}
{{--                                                        @if ($errors->has('job_doc'))--}}
{{--                                                            <div class="fv-plugins-message-container invalid-feedback">--}}
{{--                                                                <div data-field="formValidationBio"--}}
{{--                                                                     data-validator="stringLength">{{ $errors->first('job_doc') }}</div>--}}
{{--                                                            </div>--}}
{{--                                                        @endif--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

                                                <div class="col-3">
                                                    <div class="fv-plugins-icon-container mb-2">
                                                        <label class="form-label" for="job_doc">Job Doc</label>
                                                        <input type="file" id="job_doc" multiple class="form-control" value="{!! old('job_doc')??$model->job_doc !!}"
                                                               placeholder="job_doc" name="job_doc[]">
                                                        @if ($errors->has('job_doc'))
                                                            <div class="fv-plugins-message-container text-danger">
                                                                {{ $errors->first('job_doc') }}
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
{{--                                                <div class="col-3">--}}
{{--                                                    <div class="fv-plugins-icon-container mb-2">--}}
{{--                                                        <label class="form-label" for="job_requirement_doc">Job Requirement Doc</label>--}}
{{--                                                        <input type="file" id="job_requirement_doc" class="form-control" value="{!! old('job_requirement_doc')??$model->job_requirement_doc !!}"--}}
{{--                                                               placeholder="job_requirement_doc" name="job_requirement_doc">--}}
{{--                                                        @if ($errors->has('job_requirement_doc'))--}}
{{--                                                            <div class="fv-plugins-message-container invalid-feedback">--}}
{{--                                                                <div data-field="formValidationBio"--}}
{{--                                                                     data-validator="stringLength">{{ $errors->first('job_requirement_doc') }}</div>--}}
{{--                                                            </div>--}}
{{--                                                        @endif--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class="col-3">
                                                    <div class="fv-plugins-icon-container mb-2">
                                                        <label class="form-label" for="images">Banner Images</label>
                                                        <input type="file" id="images" multiple class="form-control" value="{!! old('images')??$model->images !!}"
                                                               placeholder="images" name="images[]">
                                                        @if ($errors->has('images'))
                                                            <div class="fv-plugins-message-container text-danger">
                                                                {{ $errors->first('images') }}
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="contract_detail">Job Description</label>
                                                    <textarea type="text" id="description"
                                                              class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                                              placeholder="Job Description"
                                                              name="description">{{ (old('description '))? old('description '): $model->description }}</textarea>
                                                    @if ($errors->has('description'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="address">Address</label>
                                                    <textarea type="text" id="address"
                                                              class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                                              placeholder="address"
                                                              name="address">{{ (old('address '))? old('address '): $model->address }}</textarea>
                                                    @if ($errors->has('address'))
                                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <?php
                                                    $pickup_doc = ($model->pickup_doc == 1)?$model->pickup_doc:$model->pickup_doc;
                                                    ?>
                                                    <label for="pickup_doc" class="en">Pickup</label>
                                                    <textarea cols="80" id="pickup_doc" name="pickup_doc"  rows="10" class="en">
                                                        {!! Request::old('pickup_doc', $pickup_doc) !!}
                                                    </textarea>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="tab2default">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form method="post" class="form" enctype="multipart/form-data"
                                              action="{{ route('airtableStore') }}">

                                            @csrf
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="fv-plugins-icon-container">
                                                        <label class="form-label" for="title">Address</label>
                                                        <input type="text" id="title"
                                                               value="{!! old('title')??$model->title !!}"
                                                               class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                               placeholder="Address"
                                                               name="title">
                                                        @if ($errors->has('title'))
                                                            <div class="fv-plugins-message-container invalid-feedback">
                                                                <div data-field="formValidationBio"
                                                                     data-validator="stringLength">{{ $errors->first('title') }}</div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-slot name="scripts">
{{--        <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>--}}
{{--        <script src="{{ asset('assets/js/mapInput.js')}}"></script>--}}
    </x-slot>

</x-app-layout>
<script src="https://cdn.ckeditor.com/4.19.0/standard-all/ckeditor.js"></script>
<script>
    $(document).ready(function() {
        CKEDITOR.replace('pickup_doc', {
{{--            filebrowserUploadUrl: "{{route('article.upload', ['_token' => csrf_token() ])}}",--}}
            filebrowserUploadMethod: 'form',
            fullPage: false,
            extraPlugins: 'docprops',
            // Disable content filtering because if you use full page mode, you probably
            // want to  freely enter any HTML content in source mode without any limitations.
            allowedContent: true,
            height: 320,
            removeButtons: 'PasteFromWord',

        });
    });
</script>


