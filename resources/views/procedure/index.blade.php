<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Procedure') }}
        <a href="{{ route('procedures.create') }}" class="btn btn-primary pull-right button-left">
            <i class="menu-icon tf-icons bx bx-plus"></i>Add New
        </a>
    </x-slot>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Short Title</th>
                                    <th>Slug</th>
{{--                                    <th>Banner</th>--}}
                                    <th>Video</th>
                                    <th>Specialties</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($procedures as $procedure)
                                    <tr>
                                        <td>{{$procedure->id}}</td>
                                        <td>{{$procedure->title}}</td>
                                        <td>{{$procedure->short_title}}</td>
                                        <td>{{$procedure->slug}}</td>
{{--                                        <td>--}}
{{--                                            <div class="image-show" data-title="{!! $procedure->banner !!}">--}}
{{--                                                <img src="{{$procedure->banner?env('IMAGE_BASE_URL')."/{$procedure->banner}" :'/images/member.png'}}" class="img" width="80%" height="50px">--}}
{{--                                            </div>--}}
{{--                                        </td>--}}
                                        <td>
                                            <div class="image-show" data-title="{!! $procedure->video_link !!}">
                                                @if($procedure->video_link)
                                                    <video width="80%" height="auto" controls>
                                                        <source src="{{$procedure->video_link?env('IMAGE_BASE_URL')."/{$procedure->video_link}" :'/images/member.png'}}" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                @endif

                                            </div>
                                        </td>
                                        <td>{{$procedure->specialtie->name ?? ''}}</td>
                                        <td>
                                            <a href="{{ route('procedures.edit', $procedure->id) }}" ><i class="tf-icons bx bx-edit"></i></a>
                                            <form style="display: inline" action="{{ route('procedures.destroy', $procedure->id)}}"
                                                  id="delete_form_{{$procedure->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$procedure->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-slot name="scripts">
        <script>
            $(function () {
                $("#usersTable").DataTable({
                    order: [[0, 'desc']],
                });
            });
        </script>
    </x-slot>
</x-app-layout>
