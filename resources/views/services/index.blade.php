<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Services') }}
         <a href="{{ route('services.create') }}" class="btn btn-primary pull-right button-left"><i class="menu-icon tf-icons bx bx-plus"></i> Add New</a>
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($services as $service)
                                    <tr>
                                        <td>{{$service->id ?? '-'}}</td>
                                        <td>{{$service->name}}</td>
                                        <td>{{$service->created_at ?? '-'}}</td>
                                        <td>
                                            <a href="{{ route('services.edit', $service->id) }}"><i class="tf-icons bx bx-edit"></i></a>

                                            <form style="display: inline" action="{{ route('services.destroy', $service->id)}}"
                                                  id="delete_form_{{$service->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$service->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="scripts">
        <script>
            $(function () {
            $("#usersTable").DataTable({
                order: [[0, 'desc']],
            });

        });
        </script>
    </x-slot>
</x-app-layout>
