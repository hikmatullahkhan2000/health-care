<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Profile') }}
        </h2>
    </x-slot>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills flex-column flex-md-row mb-3">
                <li class="nav-item"><a class="nav-link" href="{{ route('profile.edit') }}"><i class="bx bx-user me-1"></i> Account</a></li>
                <li class="nav-item"><a class="nav-link active"><i class="bx bx-key me-1"></i> Change Password</a></li>
            </ul>
            <form method="post" action="{{ route('profile.change-password') }}">
                @csrf
                <div class="card mb-4">
                    <h5 class="card-header">Change Password</h5>
                    <div class="card-body">
                        <p class="card-title">Ensure your account is using a long, random password to stay secure.</p>
                        <hr class="mb-4">
                        <div class="row">
                            <div class="mb-3 col-md-4">
                                <label for="firstName" class="form-label">Current Password</label>
                                <input class="form-control @error('current_password') invalid @enderror" type="password" name="current_password" value="" autofocus="">
                                @error('current_password')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3 col-md-4">
                                <label for="firstName" class="form-label">New Password</label>
                                <input class="form-control @error('password') invalid @enderror" type="password" name="password" value="" autofocus="">
                                @error('password')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3 col-md-4">
                                <label for="firstName" class="form-label">Confirm Password</label>
                                <input class="form-control @error('password_confirmation') invalid @enderror" type="password" name="password_confirmation" value="" autofocus="">
                                @error('password_confirmation')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12 mt-2">
                                <button type="submit" class="btn btn-primary me-2">Save changes</button>
                            </div>
                        </div>
                    </div>
                    <!-- /Account -->
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
