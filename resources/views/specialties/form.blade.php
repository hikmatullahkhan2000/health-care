<x-app-layout>
    <x-slot name="header">
        {{  $model->exists ? __('Edit Specialties') : __('Create Specialties')  }}
    </x-slot>

    <div class="row">
        <!-- FormValidation -->
        <div class="col-2"></div>
        <div class="row">
            <!-- FormValidation -->
            <div class="col-2"></div>
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                        <form id="paramForm" method="post" class="form" action="{{ ($model->exists)? route('specialties.update', [$model->id]): route('specialties.store') }}" enctype="multipart/form-data">
                            @if ($model->exists)
                            @method('PUT')
                            @endif
                            @csrf

                             <div class="col-12">
                                <h6 class="fw-semibold">
                                    {{  $model->exists ? __('Edit specialties') : __('Create specialties')  }}

                                </h6>
                                <hr class="mt-0">
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="fv-plugins-icon-container mb-2">
                                        <label class="form-label" for="name">Name</label>
                                        <input type="text" id="name" value="{{ old('name') ? old('name') : $model->name }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="name" name="name">
                                        @if ($errors->has('name'))
                                        <div class="fv-plugins-message-container invalid-feedback">
                                            <div data-field="formValidationBio" data-validator="stringLength">{{ $errors->first('name') }}</div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mt-3">
                                <button type="submit" name="submitButton" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
            <!-- /FormValidation -->
        </div>
        <!-- /FormValidation -->
    </div>

</x-app-layout>
