<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Specialties') }}
         <a href="{{ route('specialties.create') }}" class="btn btn-primary pull-right button-left"><i class="menu-icon tf-icons bx bx-plus"></i> Add New</a>
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($specialties as $specialty)
                                    <tr>
                                        <td>{{$specialty->id ?? '-'}}</td>
                                        <td>{{$specialty->name}}</td>
                                        <td>{{$specialty->created_at ?? '-'}}</td>
                                        <td>
                                            <a href="{{ route('specialties.edit', $specialty->id) }}"><i class="tf-icons bx bx-edit"></i></a>

                                            <form style="display: inline" action="{{ route('specialties.destroy', $specialty->id)}}"
                                                  id="delete_form_{{$specialty->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$specialty->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="scripts">
        <script>
            $(function () {
            $("#usersTable").DataTable({
                order: [[0, 'desc']],
            });

        });
        </script>
    </x-slot>
</x-app-layout>
