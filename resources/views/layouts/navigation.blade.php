<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="my-4">
        <a href="/">
                <img class="img-fluid d-block mx-auto" src="{{asset("front_assets/img/site-identify/logo.png")}}" alt="" width="50%">
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item {{request()->is('/')  ? 'active' : ''}}">
            <a href="{{url('/dashboard')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>

        @if (Auth::user()->type == \App\Models\User::admin_user)
            <li class="menu-item {{request()->segment(1) == 'users'  ? 'active' : ''}}">
                <a href="{{route("users.index")}}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-group"></i>
                    <div data-i18n="create">Users</div>
                </a>
            </li>
            <li class="menu-item {{request()->segment(1) == 'blogs'  ? 'active' : ''}}">
                <a href="{{route("blogs.index")}}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-briefcase"></i>
                    <div data-i18n="create">Blogs</div>
                </a>
            </li>
            <li class="menu-item {{request()->segment(1) == 'procedures'  ? 'active' : ''}}">
                <a href="{{route("procedures.index")}}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-briefcase"></i>
                    <div data-i18n="create">Procedures</div>
                </a>
            </li>
            <li class="menu-item {{request()->segment(1) == 'drugs'  ? 'active' : ''}}">
                <a href="{{route("drugs.index")}}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-briefcase"></i>
                    <div data-i18n="create">Drugs</div>
                </a>
            </li>
            <li class="menu-item {{request()->segment(1) == 'diseases'  ? 'active' : ''}}">
                <a href="{{route("diseases.index")}}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-briefcase"></i>
                    <div data-i18n="create">Diseases & Condition</div>
                </a>
            </li>

            <li class="menu-item {{request()->segment(1) == 'specialties'  ? 'active' : ''}}">
                <a href="{{route("specialties.index")}}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-briefcase"></i>
                    <div data-i18n="create">Specialties</div>
                </a>
            </li>

            <li class="menu-item {{request()->segment(1) == 'services'  ? 'active' : ''}}">
                <a href="{{route("services.index")}}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-briefcase"></i>
                    <div data-i18n="create">Services</div>
                </a>
            </li>

        @endif
        <li class="menu-item {{request()->segment(1) == 'services'  ? 'active' : ''}}">
            <a href="{{route("services.index")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-briefcase"></i>
                <div data-i18n="create">Services</div>
            </a>
        </li>
        <li class="menu-item {{request()->segment(1) == 'Appointments'  ? 'active' : ''}}">
            <a href="{{route("appointments.index")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-briefcase"></i>
                <div data-i18n="create">Appointments</div>
            </a>
        </li>
        {{--        <li class="menu-item {{request()->segment(1) == 'job-logs'  ? 'active' : ''}}">--}}
{{--            <a href="{{route("jobLogs")}}" class="menu-link">--}}
{{--                <i class="menu-icon tf-icons bx bx-briefcase"></i>--}}
{{--                <div data-i18n="create">JobLogs</div>--}}
{{--            </a>--}}
{{--        </li>--}}


        <li class="menu-item {{request()->segment(1) == 'profile'  ? 'active' : ''}}">
            <a href="{{route("profile.edit")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div data-i18n="create">Profile</div>
            </a>
        </li>
{{--        @if (Auth::user()->type == \App\Models\User::admin_user)--}}
{{--            <li class="menu-item {{request()->segment(1) == 'params'  ? 'active' : ''}}">--}}
{{--                <a href="{{route("params.index")}}" class="menu-link">--}}
{{--                    <i class="menu-icon tf-icons bx bx-slider"></i>--}}
{{--                    <div data-i18n="create">Params</div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}

{{--        @if (Auth::user()->type == \App\Models\User::company_user)--}}
{{--            <li class="menu-item {{request()->segment(1) == 'messages'  ? 'active' : ''}}">--}}
{{--                <a href="{{route("realTimeChat")}}" class="menu-link">--}}
{{--                    <i class="menu-icon tf-me bx-message"></i>--}}
{{--                    <div data-i18n="create">Messages</div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}

    </ul>
</aside>
