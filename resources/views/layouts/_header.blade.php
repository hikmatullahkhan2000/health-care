<nav
    class="layout-navbar container-fluid navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
    id="layout-navbar">
    <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
        <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
            <i class="bx bx-menu bx-sm"></i>
        </a>
    </div>

    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
        <!-- Search -->
        <div class="navbar-nav align-items-center">
            <div class="nav-item d-flex align-items-center">
                @if (isset($header))
                    <h4 class="m-auto">{{ $header }}
                @endif
            </div>
        </div>
        <!-- /Search -->

        <ul class="navbar-nav flex-row align-items-center ms-auto">
            <!-- Place this tag where you want the button to render. -->
            <li class="nav-item lh-1 me-3">

                            <span
                                class="github-button">Hello {!! \Illuminate\Support\Facades\Auth::user()->name !!}</span>
            </li>

            <li class="nav-item dropdown-notifications navbar-dropdown dropdown me-3 me-xl-1">
                <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="false">
{{--                    <i class="bx bx-bell bx-sm"></i>--}}
                    <span class="badge bg-danger rounded-pill badge-notifications" id="notificationCount"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-end py-0">
                    <li class="dropdown-menu-header border-bottom">
                        <div class="dropdown-header d-flex align-items-center py-3">
                            <h5 class="text-body mb-0 me-auto">Notification</h5>
                            <a href="#" class="dropdown-notifications-all text-body" data-bs-toggle="tooltip" data-bs-placement="top" aria-label="Mark all as read" data-bs-original-title="Mark all as read"><i class="bx fs-4 bx-envelope-open"></i></a>
                        </div>
                    </li>
                    <li class="dropdown-notifications-list scrollable-container ps">
                        <ul class="list-group list-group-flush" id="notificationNew">

                       </ul>
                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></li>
                    <li class="dropdown-menu-footer border-top">
                        <a href="#" class="dropdown-item d-flex justify-content-center p-3">
                            View all notifications
                        </a>
                    </li>
                </ul>
            </li>


            <li class="nav-item lh-1 me-3">

                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <x-responsive-nav-link :href="route('logout')"
                                           onclick="event.preventDefault();
                                                                                this.closest('form').submit();">
                        <i class="bx bx-power-off"></i>
                    </x-responsive-nav-link>
                </form>
            </li>

            <!-- User -->
        {{--            <li class="nav-item navbar-dropdown dropdown-user dropdown">--}}
        {{--                <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);"--}}
        {{--                   data-bs-toggle="dropdown">--}}
        {{--                    <div class="avatar avatar-online">--}}
        {{--                        <img src="../assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle"/>--}}
        {{--                    </div>--}}
        {{--                </a>--}}
        {{--                <ul class="dropdown-menu dropdown-menu-end">--}}
        {{--                    <li>--}}
        {{--                        <a class="dropdown-item" href="#">--}}
        {{--                            <div class="d-flex">--}}
        {{--                                <div class="flex-shrink-0 me-3">--}}
        {{--                                    <div class="avatar avatar-online">--}}
        {{--                                        <img src="../assets/img/avatars/1.png" alt--}}
        {{--                                             class="w-px-40 h-auto rounded-circle"/>--}}
        {{--                                    </div>--}}
        {{--                                </div>--}}
        {{--                                <div class="flex-grow-1">--}}
        {{--                                    <span class="fw-semibold d-block">John Doe</span>--}}
        {{--                                    <small class="text-muted">Admin</small>--}}
        {{--                                </div>--}}
        {{--                            </div>--}}
        {{--                        </a>--}}
        {{--                    </li>--}}
        {{--                    <li>--}}
        {{--                        <div class="dropdown-divider"></div>--}}
        {{--                    </li>--}}
        {{--                    <li>--}}
        {{--                        <a class="dropdown-item" href="#">--}}
        {{--                            <i class="bx bx-user me-2"></i>--}}
        {{--                            <span class="align-middle">My Profile</span>--}}
        {{--                        </a>--}}
        {{--                    </li>--}}
        {{--                    <li>--}}
        {{--                        <a class="dropdown-item" href="#">--}}
        {{--                            <i class="bx bx-cog me-2"></i>--}}
        {{--                            <span class="align-middle">Settings</span>--}}
        {{--                        </a>--}}
        {{--                    </li>--}}
        {{--                    <li>--}}
        {{--                        <a class="dropdown-item" href="#">--}}
        {{--                        <span class="d-flex align-items-center align-middle">--}}
        {{--                          <i class="flex-shrink-0 bx bx-credit-card me-2"></i>--}}
        {{--                          <span class="flex-grow-1 align-middle">Billing</span>--}}
        {{--                          <span class="flex-shrink-0 badge badge-center rounded-pill bg-danger w-px-20 h-px-20">4</span>--}}
        {{--                        </span>--}}
        {{--                        </a>--}}
        {{--                    </li>--}}
        {{--                    <li>--}}
        {{--                        <div class="dropdown-divider"></div>--}}
        {{--                    </li>--}}
        {{--                    <li>--}}


        {{--                            <form method="POST" action="{{ route('logout') }}">--}}
        {{--                                @csrf--}}
        {{--                                <x-responsive-nav-link :href="route('logout')"--}}
        {{--                                                       onclick="event.preventDefault();--}}
        {{--                                                                                this.closest('form').submit();">--}}
        {{--                                    <span class="align-middle">Log Out</span>--}}
        {{--                                </x-responsive-nav-link>--}}
        {{--                            </form>--}}
        {{--                    </li>--}}
        {{--                </ul>--}}
        {{--            </li>--}}
        <!--/ User -->
        </ul>
    </div>
</nav>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        {{--$(document).ready(function() {--}}
        {{--    setInterval(function() {--}}
        {{--        $.ajax({--}}
        {{--            url: "{{route("getNotification")}}",--}}
        {{--            type: "GET",--}}
        {{--            success: function(data) {--}}
        {{--                $("#notificationCount").text(data['count']);--}}
        {{--                $("#notificationCount2").text(data['count']);--}}

        {{--                $('#notificationNew').empty();--}}
        {{--                for (var i = 0; i < data['data'].length; i++) {--}}
        {{--                    var minus = data['data'][i]['diff']+' minus .....';--}}
        {{--                    var title =  data['data'][i]['title'];--}}
        {{--                    var body =  data['data'][i]['body'];--}}
        {{--                    var type =  data['data'][i]['type'];--}}
        {{--                    var uid =  data['data'][i]['id'];--}}
        {{--                    var url = "{{url('/real-time-chat')}}?phone=" + encodeURIComponent(data['data'][i]['phone']);--}}
        {{--                    var fullUrl = "";--}}
        {{--                    if(type == 0){--}}
        {{--                        fullUrl ="<a href='"+ url +"' class='dropdown-notifications-read'>"+"<span class='badge badge-dot'>"+'view'+"</span>"+"</a>";--}}
        {{--                    }--}}

        {{--                    var html = "<li class='list-group-item list-group-item-action dropdown-notifications-item marked-as-read'>"+--}}
        {{--                        "<div class='d-flex'>"+--}}
        {{--                            "<div class='flex-shrink-0 me-3'>"+--}}
        {{--                                "<div class='avatar'>"+--}}
        {{--                                    "<h6 class='w-px-40 h-auto rounded-circle'>"+'✉️'+"</h6>"+--}}
        {{--                                "</div>"+--}}
        {{--                            "</div>"+--}}
        {{--                            "<div class='flex-grow-1'>"+--}}
        {{--                                "<h6 class='mb-1'>"+title+"</h6>"+--}}
        {{--                                "<p class='mb-0'>"+body+"</p>"+--}}
        {{--                                "<small class='text-muted'>"+minus+"</small>"+--}}
        {{--                            "</div>"+--}}
        {{--                            "<div class='flex-shrink-0 dropdown-notifications-actions'>"+--}}
        {{--                                fullUrl--}}
        {{--                            "</div>"+--}}
        {{--                        "</div>"+--}}
        {{--                    "</li>"--}}

        {{--                    $('#notificationNew').append(html);--}}
        {{--                }--}}

        {{--            }--}}
        {{--        });--}}
        {{--    }, 1000); // 1000 ms = 1 second--}}
        {{--});--}}


    </script>

