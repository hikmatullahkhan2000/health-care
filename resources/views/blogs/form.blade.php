<x-app-layout>
    <x-slot name="header">
        {{  $model->exists ? __('Edit Blogs') : __('Create Blogs')  }}

    </x-slot>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" class="form" enctype="multipart/form-data"
                          action="{{ ($model->exists)? route('blogs.update', [$model->id]): route('blogs.store') }}">
                        @if ($model->exists)
                            @method('PUT')
                        @endif
                        @csrf

                        <div class="col-12">
                            <h6 class="fw-semibold">
                                {{  $model->exists ? __('Edit Blogs') : __('Create Blogs')  }}
                            </h6>
                            <hr class="mt-0">
                        </div>

                        <div class="row">
                            <div class="col-4">
                                <div class="fv-plugins-icon-container">
                                    <label class="form-label" for="title">Title</label>
                                    <input type="text" id="title"
                                           value="{!! old('title')??$model->title !!}"
                                           class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                           placeholder="title"
                                           name="title">
                                    @if ($errors->has('title'))
                                        <div class="fv-plugins-message-container invalid-feedback">
                                            <div data-field="formValidationBio"
                                                 data-validator="stringLength">{{ $errors->first('title') }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>
{{--                            <div class="col-4">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="type">Types</label>--}}
{{--                                    <select id="type" name="type"--}}
{{--                                            class="form-control">--}}
{{--                                        <option value="">Select Type</option>--}}

{{--                                        @foreach(\App\Models\Drug::types as $key =>  $status)--}}
{{--                                            <option value="{{$key}}" {{ (old('type') == $key) ? 'selected=""':'' }} {{ ($model->type == $key) ? 'selected=""':''}} >--}}
{{--                                                {{$status}}</option>--}}
{{--                                        @endForeach--}}
{{--                                    </select>--}}

{{--                                    @if ($errors->has('type'))--}}
{{--                                        <div class="help-block">--}}
{{--                                                    <span class="" role="alert">--}}
{{--                                                                <strong class="text-danger">{{ $errors->first('type') }}</strong>--}}
{{--                                                    </span></div>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-4">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="specialties">Specialties</label>--}}
{{--                                    <select id="specialties" name="specialties"--}}
{{--                                            class="form-control">--}}
{{--                                        <option value="">Select Specialties</option>--}}

{{--                                        @foreach(\App\Models\Blog::specialties as $key =>  $status)--}}
{{--                                            <option value="{{$key}}" {{ (old('specialties') == $key) ? 'selected=""':'' }} {{ ($model->specialties == $key) ? 'selected=""':''}} >--}}
{{--                                                {{$status}}</option>--}}
{{--                                        @endForeach--}}
{{--                                    </select>--}}

{{--                                    @if ($errors->has('specialties'))--}}
{{--                                        <div class="help-block">--}}
{{--                                        <span class="" role="alert">--}}
{{--                                                    <strong class="text-danger">{{ $errors->first('specialties') }}</strong>--}}
{{--                                        </span></div>--}}
{{--                                    @endif--}}

{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-4">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="category">Categories</label>--}}
{{--                                    <select id="category" name="category"--}}
{{--                                            class="form-control">--}}
{{--                                        <option value="">Select Categories</option>--}}

{{--                                        @foreach(\App\Models\Blog::categories as $key =>  $status)--}}
{{--                                            <option value="{{$key}}" {{ (old('category') == $key) ? 'selected=""':'' }} {{ ($model->category == $key) ? 'selected=""':''}} >--}}
{{--                                                {{$status}}</option>--}}
{{--                                        @endForeach--}}
{{--                                    </select>--}}

{{--                                    @if ($errors->has('category'))--}}
{{--                                        <div class="help-block">--}}
{{--                                        <span class="" role="alert">--}}
{{--                                                    <strong class="text-danger">{{ $errors->first('category') }}</strong>--}}
{{--                                        </span></div>--}}
{{--                                    @endif--}}

{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="fv-plugins-icon-container mb-2">
                                    <label class="form-label" for="banner">Banner</label>
                                    <input type="file" id="banner" class="form-control" value="{!! old('banner')??$model->banner !!}"
                                           placeholder="banner" name="banner">
                                    @if ($errors->has('banner'))
                                        <div class="fv-plugins-message-container text-danger">
                                            {{ $errors->first('banner') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="short_description">Short Description</label>
                                    <textarea type="text"
                                              class="form-control{{ $errors->has('short_description') ? ' is-invalid' : '' }}"
                                              placeholder="short_description"
                                              name="short_description">{{ (old('short_description'))? old('short_description'): $model->short_description }}</textarea>
                                    @if ($errors->has('short_description'))
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('short_description') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="contract_detail">Long Description</label>
                                <textarea type="text" id="long_description"
                                          class="form-control{{ $errors->has('long_description') ? ' is-invalid' : '' }}"
                                          placeholder="Long Description"
                                          name="long_description">{{ (old('long_description '))? old('long_description '): $model->long_description }}</textarea>
                                @if ($errors->has('long_description'))
                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('long_description') }}</strong>
                                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


    <x-slot name="scripts">
        {{--        <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>--}}
        {{--        <script src="{{ asset('assets/js/mapInput.js')}}"></script>--}}
    </x-slot>

</x-app-layout>
<script src="https://cdn.ckeditor.com/4.19.0/standard-all/ckeditor.js"></script>
<script>
    $(document).ready(function() {
        CKEDITOR.replace('long_description', {
            {{--            filebrowserUploadUrl: "{{route('article.upload', ['_token' => csrf_token() ])}}",--}}
            filebrowserUploadMethod: 'form',
            fullPage: false,
            extraPlugins: 'docprops',
            // Disable content filtering because if you use full page mode, you probably
            // want to  freely enter any HTML content in source mode without any limitations.
            allowedContent: true,
            height: 320,
            removeButtons: 'PasteFromWord',

        });
    });
</script>


