<?php
/**
 * Created by PhpStorm.
 * User: warispopal
 * Date: 2/2/19
 * Time: 4:47 PM
 */

?>


    <form class="form validate-form" method="GET" novalidate>
        <div class="form-body">
            <h4 class="form-section"><i class=""></i>Search:</h4>
            <div class="row">
                <div class="col-lg-4 col-md-8">
                    <div class="form-group">
                        <label for="basicInputFile">User Name</label>
                        <div class="position-relative has-icon-left">
                            <input type="text" class="form-control"
                                   name="name"
                                   placeholder="Name"
                                   value="{{ request('name') }}">

                            <div class="form-control-position">
                                <i class="ft-user-check info"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-8">
                    <div class="form-group">
                        <label for="basicInputFile">User Email</label>
                        <div class="position-relative has-icon-left">
                            <input type="text" class="form-control"
                                   name="email"
                                   placeholder="Email Address"
                                   value="{{ request('email') }}">
                            <div class="form-control-position">
                                <i class="ft-mail info"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-8">
                    <fieldset class="form-group">
                        <label for="basicSelect">Alpha Roles</label>
                        <select class="form-control" name="alphaRole">
                            <option value="">Select</option>
                            @foreach($alphaRoles as $key => $title)
                                <option value="{{$key}}" {{ ($key == request('alphaRole'))?'selected=""':'' }}>{{$title}}</option>
                            @endForeach
                        </select>
                    </fieldset>
                </div>
                <div class="col-lg-4 col-md-8">
                    <div class="form-group">
                        <label for="basicInputFile">Created</label>
                        <div class="position-relative has-icon-left " id="defaultrange">
                            <input type="text" class="form-control"
                                   name="created_at"
                                   placeholder="Created Date Range"
                                   value="{{ request('created_at') }}">

                            <div class="form-control-position">
                                <i class="ft-clock info"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions right">

            <a href="{{ route('users.index') }}"
               class="btn btn-raised btn-outline-secondary round btn-min-width mr-1 mb-1">Reset</a>
            <button type="submit"
                    class="btn btn-raised btn-outline-success round btn-min-width mr-1 mb-1">
                <i class="fa fa-check-square-o"></i> Search
            </button>
        </div>
    </form>
