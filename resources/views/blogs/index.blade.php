<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Blogs') }}
        <a href="{{ route('blogs.create') }}" class="btn btn-primary pull-right button-left">
            <i class="menu-icon tf-icons bx bx-plus"></i>Add New
        </a>
    </x-slot>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Banner</th>
{{--                                    <th>Specialties</th>--}}
{{--                                    <th>Category</th>--}}
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($blogs as $blog)
                                    <tr>
                                        <td>{{$blog->id}}</td>
                                        <td>{{$blog->title}}</td>
                                        <td>{{$blog->slug}}</td>
                                        <td>
                                            <div class="image-show" data-title="{!! $blog->banner !!}">
                                                <img src="{{asset("storage/".$blog->banner)}}" alt="News Feeds" class="img" width="50%" height="50px">

                                            </div>
                                        </td>
{{--                                        <td>{{\App\Models\Blog::specialties[$blog->specialties] ?? ''}}</td>--}}
{{--                                        <td>{{\App\Models\Blog::categories[$blog->category] ?? ''}}</td>--}}
                                        <td>
                                            <a href="{{ route('blogs.edit', $blog->id) }}" ><i class="tf-icons bx bx-edit"></i></a>
                                            <form style="display: inline" action="{{ route('blogs.destroy', $blog->id)}}"
                                                  id="delete_form_{{$blog->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a class="badge badge-light delete-confirm-id"
                                                   data-id="{{$blog->id}}" title="Delete">
                                                    <i class="tf-icons bx bx-trash text-danger listing_delete_icon"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-slot name="scripts">
        <script>
            $(function () {
                $("#usersTable").DataTable({
                    order: [[0, 'desc']],
                });
            });
        </script>
    </x-slot>
</x-app-layout>
